﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPCEditor
{
    public class NPCCTRLTIME
    {
        public int iYear;
        public int iMouth;
        public int iWeek;
        public int iDay;
        public int iHours;
        public int iMinutes;
    };

    public class NPCGENFILECTRL
    {
        public uint id = 0;
        public int iControllerID = 0;
        public byte[] szName;
        public bool bActived = false;
        public int iWaitTime = 0;
        public int iStopTime = 0;
        public bool bActiveTimeInvalid = false;
        public bool bStopTimeInvalid = false;
        public NPCCTRLTIME ActiveTime;
        public NPCCTRLTIME StopTime;
        public uint iActiveTimeRange = 0;
    };

    public class NPCGENFILEDYNOBJ
    {
        public uint dwDynObjID = 0;
        public float[] vPos;
        public byte[] dir;
        public byte rad = 0;
        public uint idController = 0;
        public byte scale = 0;
    };

    public class NPCGENFILERES
    {
        public int iResType = 0;
        public int idTemplate = 0;
        public uint dwRefreshTime = 0;
        public uint dwNumber = 0;
        public float fHeiOff = 0;
    };

    public class NPCGENFILERESAREA
    {
        public float[] vPos;
        public float fExtX = 0;
        public float fExtZ = 0;
        public int iNumRes = 0;
        public bool bInitGen = false;
        public bool bAutoRevive = false;
        public bool bValidOnce = false;
        public uint dwGenID = 0;
        public byte[] dir;
        public byte rad = 0;
        public uint idCtrl = 0;
        public int iMaxNum = 0;
        public List<NPCGENFILERES> m_aRes;
    };

    public class NPCGENFILEAIGEN
    {
        public uint dwID = 0;
        public uint dwNum = 0;
        public int iRefresh = 0;
        public uint dwDiedTimes = 0;
        public uint dwAggressive = 0;
        public float fOffsetWater = 0;
        public float fOffsetTrn = 0;
        public uint dwFaction = 0;
        public uint dwFacHelper = 0;
        public uint dwFacAccept = 0;
        public bool bNeedHelp = false;
        public bool bDefFaction = false;
        public bool bDefFacHelper = false;
        public bool bDefFacAccept = false;
        public int iPathID = 0;
        public int iLoopType = 0;
        public int iSpeedFlag = 0;
        public int iDeadTime = 0;
        public int iRefreshLower = 0;
    };

    public class AREA
    {
        public int iType = 0;
        public int iNumGen = 0;
        public float[] vPos;
        public float[] vDir;
        public float[] vExts;
        public int iNPCType = 0;
        public int iGroupType = 0;
        public bool bInitGen = false;
        public bool bAutoRevive = false;
        public bool bValidOnce = false;
        public uint dwGenID = 0;
        public uint idCtrl = 0;
        public int iLifeTime = 0;
        public int iMaxNum = 0;
        public List<NPCGENFILEAIGEN> m_aNPCGens;
    };

    public class NPCGENFILEHEADER
    {
        public int iNumAIGen;
        public int iNumResArea;
        public int iNumDynObj;
        public int iNumNPCCtrl;
    };

    public class NPCDATA
    {
        public int version;
        public NPCGENFILEHEADER Header;
        public List<AREA> m_aAreas;
        public List<NPCGENFILERESAREA> m_aResAreas;
        public List<NPCGENFILEDYNOBJ> m_aDynObjs;
        public List<NPCGENFILECTRL> m_aControllers;
    }

    public class DYNAMICOBJECT
    {
        public uint id;
        public int length;
        public byte[] path;
    };

    public class DYNAMICOBJECTSDATA
    {
        public int version;
        public int count;
        public List<DYNAMICOBJECT> objectsinfo;
    };
}
