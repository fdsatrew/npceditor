﻿namespace NPCEditor
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.npcList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.npcListMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createNpc = new System.Windows.Forms.ToolStripMenuItem();
            this.removeNpc = new System.Windows.Forms.ToolStripMenuItem();
            this.copyNpc = new System.Windows.Forms.ToolStripMenuItem();
            this.exportNpc = new System.Windows.Forms.ToolStripMenuItem();
            this.importNpc = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.npcList1 = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.npcList1Menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createNpc1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeNpc1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyNpc1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportNpc1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importNpc1 = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resList1Menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createResource1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeResource1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyResource1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportRes1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importRes1 = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resListMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createResource = new System.Windows.Forms.ToolStripMenuItem();
            this.removeResource = new System.Windows.Forms.ToolStripMenuItem();
            this.copyResource = new System.Windows.Forms.ToolStripMenuItem();
            this.exportRes = new System.Windows.Forms.ToolStripMenuItem();
            this.importRes = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dynObjMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.copyDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.exportDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.importDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.triggerMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createTrigger = new System.Windows.Forms.ToolStripMenuItem();
            this.removeTrigger = new System.Windows.Forms.ToolStripMenuItem();
            this.copyTrigger = new System.Windows.Forms.ToolStripMenuItem();
            this.exportTrigger = new System.Windows.Forms.ToolStripMenuItem();
            this.importTrigger = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label68 = new System.Windows.Forms.Label();
            this.searchText = new System.Windows.Forms.TextBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.searchType = new System.Windows.Forms.ComboBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.searchCategory = new System.Windows.Forms.ComboBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openNPCDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openElementsdataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveNPCDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsNPCDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientProcessNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.russianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishPWIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pWIEUFrenchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pWIEUGermanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pWChinaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pWBrazilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pWJapanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pWPhiliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.malaysianEnglishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsMenu = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dynamicobjectsdataEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.converterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v6ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v7ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v9ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.v11ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToCoordsdatatxtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.npcList)).BeginInit();
            this.npcListMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.npcList1)).BeginInit();
            this.npcList1Menu.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.resList1Menu.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.resListMenu.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.dynObjMenu.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.triggerMenu.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.settingsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.progressBar1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 96F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1044, 572);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1038, 543);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1030, 517);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "NPC";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Controls.Add(this.npcList, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 511F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1024, 511);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // npcList
            // 
            this.npcList.AllowUserToAddRows = false;
            this.npcList.AllowUserToDeleteRows = false;
            this.npcList.AllowUserToResizeColumns = false;
            this.npcList.AllowUserToResizeRows = false;
            this.npcList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.npcList.ColumnHeadersVisible = false;
            this.npcList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.npcList.ContextMenuStrip = this.npcListMenu;
            this.npcList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.npcList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.npcList.Location = new System.Drawing.Point(3, 3);
            this.npcList.Name = "npcList";
            this.npcList.ReadOnly = true;
            this.npcList.RowHeadersVisible = false;
            this.npcList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.npcList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.npcList.ShowCellErrors = false;
            this.npcList.ShowCellToolTips = false;
            this.npcList.ShowEditingIcon = false;
            this.npcList.ShowRowErrors = false;
            this.npcList.Size = new System.Drawing.Size(403, 505);
            this.npcList.TabIndex = 6;
            this.npcList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            this.npcList.SelectionChanged += new System.EventHandler(this.npcList_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // npcListMenu
            // 
            this.npcListMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNpc,
            this.removeNpc,
            this.copyNpc,
            this.exportNpc,
            this.importNpc});
            this.npcListMenu.Name = "contextMenuStrip1";
            this.npcListMenu.Size = new System.Drawing.Size(145, 114);
            this.npcListMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // createNpc
            // 
            this.createNpc.Name = "createNpc";
            this.createNpc.Size = new System.Drawing.Size(144, 22);
            this.createNpc.Text = "Create NPC";
            this.createNpc.Click += new System.EventHandler(this.createNpc_Click);
            // 
            // removeNpc
            // 
            this.removeNpc.Name = "removeNpc";
            this.removeNpc.Size = new System.Drawing.Size(144, 22);
            this.removeNpc.Text = "Remove NPC";
            this.removeNpc.Click += new System.EventHandler(this.removeNpc_Click);
            // 
            // copyNpc
            // 
            this.copyNpc.Name = "copyNpc";
            this.copyNpc.Size = new System.Drawing.Size(144, 22);
            this.copyNpc.Text = "Clone NPC";
            this.copyNpc.Click += new System.EventHandler(this.copyNpc_Click);
            // 
            // exportNpc
            // 
            this.exportNpc.Image = global::NPCEditor.Properties.Resources.export;
            this.exportNpc.Name = "exportNpc";
            this.exportNpc.Size = new System.Drawing.Size(144, 22);
            this.exportNpc.Text = "Export NPC";
            this.exportNpc.Click += new System.EventHandler(this.exportNpc_Click);
            // 
            // importNpc
            // 
            this.importNpc.Image = global::NPCEditor.Properties.Resources.import;
            this.importNpc.Name = "importNpc";
            this.importNpc.Size = new System.Drawing.Size(144, 22);
            this.importNpc.Text = "Import NPC";
            this.importNpc.Click += new System.EventHandler(this.importNpc_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox5);
            this.panel1.Controls.Add(this.comboBox7);
            this.panel1.Controls.Add(this.tableLayoutPanel9);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.textBox15);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.textBox13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textBox12);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.textBox11);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(412, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(609, 505);
            this.panel1.TabIndex = 1;
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "Ground",
            "Free 3D"});
            this.comboBox5.Location = new System.Drawing.Point(86, 165);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(100, 21);
            this.comboBox5.TabIndex = 26;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // comboBox7
            // 
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "Ground",
            "Free 3D"});
            this.comboBox7.Location = new System.Drawing.Point(85, 113);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(100, 21);
            this.comboBox7.TabIndex = 25;
            this.comboBox7.SelectedIndexChanged += new System.EventHandler(this.comboBox7_SelectedIndexChanged);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.Controls.Add(this.checkBox1, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBox2, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBox3, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(9, 194);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(597, 28);
            this.tableLayoutPanel9.TabIndex = 24;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox1.Location = new System.Drawing.Point(401, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(193, 22);
            this.checkBox1.TabIndex = 8;
            this.checkBox1.Text = "bInitGen";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox2.Location = new System.Drawing.Point(202, 3);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(193, 22);
            this.checkBox2.TabIndex = 9;
            this.checkBox2.Text = "bAutoRevive";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox3.Location = new System.Drawing.Point(3, 3);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(193, 22);
            this.checkBox3.TabIndex = 10;
            this.checkBox3.Text = "bValidOnce";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 168);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "iGroupType";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox18);
            this.groupBox1.Controls.Add(this.comboBox8);
            this.groupBox1.Controls.Add(this.npcList1);
            this.groupBox1.Controls.Add(this.textBox24);
            this.groupBox1.Controls.Add(this.textBox23);
            this.groupBox1.Controls.Add(this.textBox22);
            this.groupBox1.Controls.Add(this.textBox21);
            this.groupBox1.Controls.Add(this.textBox20);
            this.groupBox1.Controls.Add(this.textBox19);
            this.groupBox1.Controls.Add(this.textBox17);
            this.groupBox1.Controls.Add(this.textBox16);
            this.groupBox1.Controls.Add(this.textBox25);
            this.groupBox1.Controls.Add(this.textBox29);
            this.groupBox1.Controls.Add(this.textBox28);
            this.groupBox1.Controls.Add(this.checkBox7);
            this.groupBox1.Controls.Add(this.checkBox6);
            this.groupBox1.Controls.Add(this.checkBox5);
            this.groupBox1.Controls.Add(this.checkBox4);
            this.groupBox1.Controls.Add(this.textBox30);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.textBox26);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(10, 228);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 274);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "NPC";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(265, 63);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(100, 20);
            this.textBox18.TabIndex = 50;
            this.textBox18.TextChanged += new System.EventHandler(this.textBox18_TextChanged_1);
            // 
            // comboBox8
            // 
            this.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "iNPCType0",
            "iNPCType1"});
            this.comboBox8.Location = new System.Drawing.Point(483, 41);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(100, 21);
            this.comboBox8.TabIndex = 49;
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged);
            // 
            // npcList1
            // 
            this.npcList1.AllowUserToAddRows = false;
            this.npcList1.AllowUserToDeleteRows = false;
            this.npcList1.AllowUserToResizeColumns = false;
            this.npcList1.AllowUserToResizeRows = false;
            this.npcList1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.npcList1.ColumnHeadersVisible = false;
            this.npcList1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2});
            this.npcList1.ContextMenuStrip = this.npcList1Menu;
            this.npcList1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.npcList1.Location = new System.Drawing.Point(6, 16);
            this.npcList1.Name = "npcList1";
            this.npcList1.ReadOnly = true;
            this.npcList1.RowHeadersVisible = false;
            this.npcList1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.npcList1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.npcList1.ShowCellErrors = false;
            this.npcList1.ShowCellToolTips = false;
            this.npcList1.ShowEditingIcon = false;
            this.npcList1.ShowRowErrors = false;
            this.npcList1.Size = new System.Drawing.Size(161, 252);
            this.npcList1.TabIndex = 48;
            this.npcList1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            this.npcList1.SelectionChanged += new System.EventHandler(this.npcList1_SelectionChanged);
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // npcList1Menu
            // 
            this.npcList1Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNpc1,
            this.removeNpc1,
            this.copyNpc1,
            this.exportNpc1,
            this.importNpc1});
            this.npcList1Menu.Name = "contextMenuStrip2";
            this.npcList1Menu.Size = new System.Drawing.Size(145, 114);
            this.npcList1Menu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // createNpc1
            // 
            this.createNpc1.Name = "createNpc1";
            this.createNpc1.Size = new System.Drawing.Size(144, 22);
            this.createNpc1.Text = "Create NPC";
            this.createNpc1.Click += new System.EventHandler(this.createNpc1_Click);
            // 
            // removeNpc1
            // 
            this.removeNpc1.Name = "removeNpc1";
            this.removeNpc1.Size = new System.Drawing.Size(144, 22);
            this.removeNpc1.Text = "Remove NPC";
            this.removeNpc1.Click += new System.EventHandler(this.removeNpc1_Click);
            // 
            // copyNpc1
            // 
            this.copyNpc1.Name = "copyNpc1";
            this.copyNpc1.Size = new System.Drawing.Size(144, 22);
            this.copyNpc1.Text = "Copy NPC";
            this.copyNpc1.Click += new System.EventHandler(this.copyNpc1_Click);
            // 
            // exportNpc1
            // 
            this.exportNpc1.Image = global::NPCEditor.Properties.Resources.export;
            this.exportNpc1.Name = "exportNpc1";
            this.exportNpc1.Size = new System.Drawing.Size(144, 22);
            this.exportNpc1.Text = "Export NPC";
            this.exportNpc1.Click += new System.EventHandler(this.exportNPCToolStripMenuItem_Click);
            // 
            // importNpc1
            // 
            this.importNpc1.Image = global::NPCEditor.Properties.Resources.import;
            this.importNpc1.Name = "importNpc1";
            this.importNpc1.Size = new System.Drawing.Size(144, 22);
            this.importNpc1.Text = "Import NPC";
            this.importNpc1.Click += new System.EventHandler(this.importNpc1_Click);
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(265, 220);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(100, 20);
            this.textBox24.TabIndex = 47;
            this.textBox24.TextChanged += new System.EventHandler(this.textBox24_TextChanged);
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(265, 194);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(100, 20);
            this.textBox23.TabIndex = 46;
            this.textBox23.TextChanged += new System.EventHandler(this.textBox23_TextChanged);
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(265, 167);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(100, 20);
            this.textBox22.TabIndex = 45;
            this.textBox22.TextChanged += new System.EventHandler(this.textBox22_TextChanged);
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(265, 141);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 20);
            this.textBox21.TabIndex = 44;
            this.textBox21.TextChanged += new System.EventHandler(this.textBox21_TextChanged);
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(265, 115);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(100, 20);
            this.textBox20.TabIndex = 43;
            this.textBox20.TextChanged += new System.EventHandler(this.textBox20_TextChanged);
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(265, 89);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(100, 20);
            this.textBox19.TabIndex = 42;
            this.textBox19.TextChanged += new System.EventHandler(this.textBox19_TextChanged);
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(265, 40);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 20);
            this.textBox17.TabIndex = 40;
            this.textBox17.TextChanged += new System.EventHandler(this.textBox17_TextChanged);
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(265, 16);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 20);
            this.textBox16.TabIndex = 39;
            this.textBox16.TextChanged += new System.EventHandler(this.textBox16_TextChanged);
            this.textBox16.LostFocus += new System.EventHandler(this.textBox16_LostFocus);
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(265, 246);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(100, 20);
            this.textBox25.TabIndex = 38;
            this.textBox25.TextChanged += new System.EventHandler(this.textBox25_TextChanged);
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(483, 94);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(100, 20);
            this.textBox29.TabIndex = 37;
            this.textBox29.TextChanged += new System.EventHandler(this.textBox29_TextChanged);
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(483, 67);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(100, 20);
            this.textBox28.TabIndex = 36;
            this.textBox28.TextChanged += new System.EventHandler(this.textBox28_TextChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(389, 215);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(101, 17);
            this.checkBox7.TabIndex = 34;
            this.checkBox7.Text = "bDefFacAccept";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(389, 192);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(98, 17);
            this.checkBox6.TabIndex = 33;
            this.checkBox6.Text = "bDefFacHelper";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(389, 169);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(84, 17);
            this.checkBox5.TabIndex = 32;
            this.checkBox5.Text = "bDefFaction";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(389, 147);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(80, 17);
            this.checkBox4.TabIndex = 22;
            this.checkBox4.Text = "bNeedHelp";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(483, 119);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(100, 20);
            this.textBox30.TabIndex = 31;
            this.textBox30.TextChanged += new System.EventHandler(this.textBox30_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(386, 124);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 13);
            this.label28.TabIndex = 30;
            this.label28.Text = "iRefreshLower";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(386, 98);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(58, 13);
            this.label27.TabIndex = 28;
            this.label27.Text = "iDeadTime";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(386, 72);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 13);
            this.label26.TabIndex = 26;
            this.label26.Text = "iSpeedFlag";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(386, 47);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(57, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "iLoopType";
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(483, 15);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(100, 20);
            this.textBox26.TabIndex = 23;
            this.textBox26.TextChanged += new System.EventHandler(this.textBox26_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(387, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(42, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "iPathID";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(173, 250);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 13);
            this.label23.TabIndex = 20;
            this.label23.Text = "dwFacAccept";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(173, 226);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 13);
            this.label22.TabIndex = 18;
            this.label22.Text = "dwFacHelper";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(173, 201);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 13);
            this.label21.TabIndex = 16;
            this.label21.Text = "dwFaction";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(173, 175);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 13);
            this.label20.TabIndex = 14;
            this.label20.Text = "fOffsetTrn";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(173, 149);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "fOffsetWater";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(173, 123);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "dwAggressive";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(173, 97);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "dwDiedTimes";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(173, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "iRefresh";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(173, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "dwNum";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(173, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "dwID";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(85, 139);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 20);
            this.textBox15.TabIndex = 20;
            this.textBox15.TextChanged += new System.EventHandler(this.textBox15_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 143);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "dwGenID";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 117);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "iType";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(85, 87);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 16;
            this.textBox13.TextChanged += new System.EventHandler(this.textBox13_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1, 90);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "iMaxNum";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(85, 61);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 14;
            this.textBox12.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "iLifeTime";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(85, 35);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 12;
            this.textBox11.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "idCtrl";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.46739F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.27174F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.46956F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.46956F));
            this.tableLayoutPanel3.Controls.Add(this.textBox2, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label7, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.textBox3, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.textBox4, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.textBox5, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.textBox6, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.textBox7, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.textBox8, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.textBox9, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.textBox10, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(190, 7);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(416, 181);
            this.tableLayoutPanel3.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox2.Location = new System.Drawing.Point(98, 54);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(87, 20);
            this.textBox2.TabIndex = 8;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "vPos";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(134, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "X";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(242, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Y";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(353, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Z";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(3, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "vExt";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox3.Location = new System.Drawing.Point(203, 54);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(91, 20);
            this.textBox3.TabIndex = 11;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox4.Location = new System.Drawing.Point(314, 54);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(92, 20);
            this.textBox4.TabIndex = 12;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // textBox5
            // 
            this.textBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox5.Location = new System.Drawing.Point(98, 100);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(87, 20);
            this.textBox5.TabIndex = 13;
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // textBox6
            // 
            this.textBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox6.Location = new System.Drawing.Point(203, 100);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(91, 20);
            this.textBox6.TabIndex = 14;
            this.textBox6.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // textBox7
            // 
            this.textBox7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox7.Location = new System.Drawing.Point(317, 100);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(86, 20);
            this.textBox7.TabIndex = 15;
            this.textBox7.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // textBox8
            // 
            this.textBox8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox8.Location = new System.Drawing.Point(98, 147);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(87, 20);
            this.textBox8.TabIndex = 16;
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // textBox9
            // 
            this.textBox9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox9.Location = new System.Drawing.Point(203, 147);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(91, 20);
            this.textBox9.TabIndex = 17;
            this.textBox9.TextChanged += new System.EventHandler(this.textBox9_TextChanged);
            // 
            // textBox10
            // 
            this.textBox10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox10.Location = new System.Drawing.Point(315, 147);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(89, 20);
            this.textBox10.TabIndex = 18;
            this.textBox10.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "vDir";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "iNPCType0",
            "iNPCType1"});
            this.comboBox1.Location = new System.Drawing.Point(85, 8);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(2, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "iNPCType";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1030, 517);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Resources";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.dataGridView1, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 511F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1024, 511);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkBox10);
            this.panel2.Controls.Add(this.checkBox8);
            this.panel2.Controls.Add(this.checkBox9);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.textBox38);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.textBox37);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.textBox36);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.tableLayoutPanel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(412, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(609, 505);
            this.panel2.TabIndex = 4;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(6, 137);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(66, 17);
            this.checkBox10.TabIndex = 19;
            this.checkBox10.Text = "bInitGen";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(6, 91);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(81, 17);
            this.checkBox8.TabIndex = 21;
            this.checkBox8.Text = "bValidOnce";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(6, 114);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(88, 17);
            this.checkBox9.TabIndex = 20;
            this.checkBox9.Text = "bAutoRevive";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox27);
            this.groupBox2.Controls.Add(this.dataGridView5);
            this.groupBox2.Controls.Add(this.textBox46);
            this.groupBox2.Controls.Add(this.textBox47);
            this.groupBox2.Controls.Add(this.textBox49);
            this.groupBox2.Controls.Add(this.textBox50);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.label51);
            this.groupBox2.Controls.Add(this.label52);
            this.groupBox2.Controls.Add(this.label53);
            this.groupBox2.Controls.Add(this.label54);
            this.groupBox2.Location = new System.Drawing.Point(6, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(597, 341);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resources";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(369, 66);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(117, 20);
            this.textBox27.TabIndex = 52;
            this.textBox27.TextChanged += new System.EventHandler(this.textBox27_TextChanged_1);
            // 
            // dataGridView5
            // 
            this.dataGridView5.AllowUserToAddRows = false;
            this.dataGridView5.AllowUserToDeleteRows = false;
            this.dataGridView5.AllowUserToResizeColumns = false;
            this.dataGridView5.AllowUserToResizeRows = false;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView5.ColumnHeadersVisible = false;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3});
            this.dataGridView5.ContextMenuStrip = this.resList1Menu;
            this.dataGridView5.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView5.Location = new System.Drawing.Point(6, 17);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.RowHeadersVisible = false;
            this.dataGridView5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView5.ShowCellErrors = false;
            this.dataGridView5.ShowCellToolTips = false;
            this.dataGridView5.ShowEditingIcon = false;
            this.dataGridView5.ShowRowErrors = false;
            this.dataGridView5.Size = new System.Drawing.Size(254, 318);
            this.dataGridView5.TabIndex = 49;
            this.dataGridView5.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            this.dataGridView5.SelectionChanged += new System.EventHandler(this.dataGridView5_SelectionChanged);
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            // 
            // resList1Menu
            // 
            this.resList1Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createResource1,
            this.removeResource1,
            this.copyResource1,
            this.exportRes1,
            this.importRes1});
            this.resList1Menu.Name = "contextMenuStrip2";
            this.resList1Menu.Size = new System.Drawing.Size(166, 114);
            this.resList1Menu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip4_Opening);
            // 
            // createResource1
            // 
            this.createResource1.Name = "createResource1";
            this.createResource1.Size = new System.Drawing.Size(165, 22);
            this.createResource1.Text = "Create resource";
            this.createResource1.Click += new System.EventHandler(this.createResource1_Click);
            // 
            // removeResource1
            // 
            this.removeResource1.Name = "removeResource1";
            this.removeResource1.Size = new System.Drawing.Size(165, 22);
            this.removeResource1.Text = "Remove resource";
            this.removeResource1.Click += new System.EventHandler(this.removeResource1_Click);
            // 
            // copyResource1
            // 
            this.copyResource1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.copyResource1.Name = "copyResource1";
            this.copyResource1.Size = new System.Drawing.Size(165, 22);
            this.copyResource1.Text = "Clone resource";
            this.copyResource1.Click += new System.EventHandler(this.copyResource1_Click);
            // 
            // exportRes1
            // 
            this.exportRes1.Image = global::NPCEditor.Properties.Resources.export;
            this.exportRes1.Name = "exportRes1";
            this.exportRes1.Size = new System.Drawing.Size(165, 22);
            this.exportRes1.Text = "Export NPC";
            this.exportRes1.Click += new System.EventHandler(this.exportRes1_Click);
            // 
            // importRes1
            // 
            this.importRes1.Image = global::NPCEditor.Properties.Resources.import;
            this.importRes1.Name = "importRes1";
            this.importRes1.Size = new System.Drawing.Size(165, 22);
            this.importRes1.Text = "Import NPC";
            this.importRes1.Click += new System.EventHandler(this.importRes1_Click);
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(369, 118);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(117, 20);
            this.textBox46.TabIndex = 43;
            this.textBox46.TextChanged += new System.EventHandler(this.textBox46_TextChanged);
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(369, 92);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(117, 20);
            this.textBox47.TabIndex = 42;
            this.textBox47.TextChanged += new System.EventHandler(this.textBox47_TextChanged);
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(369, 15);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(117, 20);
            this.textBox49.TabIndex = 40;
            this.textBox49.TextChanged += new System.EventHandler(this.textBox49_TextChanged);
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(369, 41);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(117, 20);
            this.textBox50.TabIndex = 39;
            this.textBox50.TextChanged += new System.EventHandler(this.textBox50_TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(277, 122);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(40, 13);
            this.label50.TabIndex = 10;
            this.label50.Text = "fHeiOff";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(277, 97);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(58, 13);
            this.label51.TabIndex = 8;
            this.label51.Text = "dwNumber";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(277, 71);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(81, 13);
            this.label52.TabIndex = 6;
            this.label52.Text = "dwRefreshTime";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(277, 19);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(59, 13);
            this.label53.TabIndex = 4;
            this.label53.Text = "idTemplate";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(277, 45);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(52, 13);
            this.label54.TabIndex = 2;
            this.label54.Text = "iResType";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(82, 58);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(100, 20);
            this.textBox38.TabIndex = 18;
            this.textBox38.TextChanged += new System.EventHandler(this.textBox38_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(1, 62);
            this.label36.Margin = new System.Windows.Forms.Padding(0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 13);
            this.label36.TabIndex = 17;
            this.label36.Text = "iMaxNum";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(82, 32);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(100, 20);
            this.textBox37.TabIndex = 16;
            this.textBox37.TextChanged += new System.EventHandler(this.textBox37_TextChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(1, 36);
            this.label35.Margin = new System.Windows.Forms.Padding(0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(30, 13);
            this.label35.TabIndex = 15;
            this.label35.Text = "idCtrl";
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(82, 6);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(100, 20);
            this.textBox36.TabIndex = 14;
            this.textBox36.TextChanged += new System.EventHandler(this.textBox36_TextChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(0, 10);
            this.label34.Margin = new System.Windows.Forms.Padding(0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 13);
            this.label34.TabIndex = 13;
            this.label34.Text = "dwGenID";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.82609F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.91304F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.46956F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.46956F));
            this.tableLayoutPanel5.Controls.Add(this.textBox31, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label29, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox39, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.label30, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label31, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label32, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox32, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox33, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.label33, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label39, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox34, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBox35, 3, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBox41, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox40, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox1, 3, 2);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(201, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(405, 152);
            this.tableLayoutPanel5.TabIndex = 8;
            // 
            // textBox31
            // 
            this.textBox31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox31.Location = new System.Drawing.Point(99, 52);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(82, 20);
            this.textBox31.TabIndex = 8;
            this.textBox31.TextChanged += new System.EventHandler(this.textBox31_TextChanged);
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 55);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(31, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = "vPos";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox39
            // 
            this.textBox39.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox39.Location = new System.Drawing.Point(195, 123);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(95, 20);
            this.textBox39.TabIndex = 27;
            this.textBox39.TextChanged += new System.EventHandler(this.textBox39_TextChanged);
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(133, 15);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "X";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(235, 15);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(14, 13);
            this.label31.TabIndex = 5;
            this.label31.Text = "Y";
            // 
            // label32
            // 
            this.label32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(343, 15);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Z";
            // 
            // textBox32
            // 
            this.textBox32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox32.Location = new System.Drawing.Point(197, 52);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(91, 20);
            this.textBox32.TabIndex = 11;
            this.textBox32.TextChanged += new System.EventHandler(this.textBox32_TextChanged);
            // 
            // textBox33
            // 
            this.textBox33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox33.Location = new System.Drawing.Point(305, 52);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(91, 20);
            this.textBox33.TabIndex = 12;
            this.textBox33.TextChanged += new System.EventHandler(this.textBox33_TextChanged);
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 127);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 13);
            this.label33.TabIndex = 9;
            this.label33.Text = "vExt";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.Location = new System.Drawing.Point(3, 81);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(86, 34);
            this.label39.TabIndex = 22;
            this.label39.Text = "vDir";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox34
            // 
            this.textBox34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox34.Location = new System.Drawing.Point(99, 123);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(82, 20);
            this.textBox34.TabIndex = 13;
            this.textBox34.TextChanged += new System.EventHandler(this.textBox34_TextChanged);
            // 
            // textBox35
            // 
            this.textBox35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox35.Location = new System.Drawing.Point(305, 123);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(91, 20);
            this.textBox35.TabIndex = 14;
            this.textBox35.TextChanged += new System.EventHandler(this.textBox35_TextChanged);
            // 
            // textBox41
            // 
            this.textBox41.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox41.Location = new System.Drawing.Point(98, 88);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(85, 20);
            this.textBox41.TabIndex = 23;
            this.textBox41.TextChanged += new System.EventHandler(this.textBox41_TextChanged);
            // 
            // textBox40
            // 
            this.textBox40.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox40.Location = new System.Drawing.Point(196, 88);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(93, 20);
            this.textBox40.TabIndex = 25;
            this.textBox40.TextChanged += new System.EventHandler(this.textBox40_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox1.Location = new System.Drawing.Point(304, 88);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(93, 20);
            this.textBox1.TabIndex = 28;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox39_TextChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dataGridView1.ContextMenuStrip = this.resListMenu;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.ShowRowErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(403, 505);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.resList_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // resListMenu
            // 
            this.resListMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createResource,
            this.removeResource,
            this.copyResource,
            this.exportRes,
            this.importRes});
            this.resListMenu.Name = "contextMenuStrip2";
            this.resListMenu.Size = new System.Drawing.Size(166, 114);
            this.resListMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip3_Opening);
            // 
            // createResource
            // 
            this.createResource.Name = "createResource";
            this.createResource.Size = new System.Drawing.Size(165, 22);
            this.createResource.Text = "Create resource";
            this.createResource.Click += new System.EventHandler(this.createResource_Click);
            // 
            // removeResource
            // 
            this.removeResource.Name = "removeResource";
            this.removeResource.Size = new System.Drawing.Size(165, 22);
            this.removeResource.Text = "Remove resource";
            this.removeResource.Click += new System.EventHandler(this.removeResource_Click);
            // 
            // copyResource
            // 
            this.copyResource.Name = "copyResource";
            this.copyResource.Size = new System.Drawing.Size(165, 22);
            this.copyResource.Text = "Clone resource";
            this.copyResource.Click += new System.EventHandler(this.copyResource_Click);
            // 
            // exportRes
            // 
            this.exportRes.Image = global::NPCEditor.Properties.Resources.export;
            this.exportRes.Name = "exportRes";
            this.exportRes.Size = new System.Drawing.Size(165, 22);
            this.exportRes.Text = "Export NPC";
            this.exportRes.Click += new System.EventHandler(this.exportRes_Click);
            // 
            // importRes
            // 
            this.importRes.Image = global::NPCEditor.Properties.Resources.import;
            this.importRes.Name = "importRes";
            this.importRes.Size = new System.Drawing.Size(165, 22);
            this.importRes.Text = "Import NPC";
            this.importRes.Click += new System.EventHandler(this.importRes_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1030, 517);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Dynamic objects";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel6.Controls.Add(this.dataGridView2, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 517F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1030, 517);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView2.ColumnHeadersVisible = false;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            this.dataGridView2.ContextMenuStrip = this.dynObjMenu;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.ShowCellErrors = false;
            this.dataGridView2.ShowCellToolTips = false;
            this.dataGridView2.ShowEditingIcon = false;
            this.dataGridView2.ShowRowErrors = false;
            this.dataGridView2.Size = new System.Drawing.Size(406, 511);
            this.dataGridView2.TabIndex = 6;
            this.dataGridView2.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            this.dataGridView2.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged_1);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dynObjMenu
            // 
            this.dynObjMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createDynObj,
            this.removeDynObj,
            this.copyDynObj,
            this.exportDynObj,
            this.importDynObj});
            this.dynObjMenu.Name = "contextMenuStrip2";
            this.dynObjMenu.Size = new System.Drawing.Size(203, 114);
            this.dynObjMenu.Opening += new System.ComponentModel.CancelEventHandler(this.dynObjMenu_Opening);
            // 
            // createDynObj
            // 
            this.createDynObj.Name = "createDynObj";
            this.createDynObj.Size = new System.Drawing.Size(202, 22);
            this.createDynObj.Text = "Create dynamic object";
            this.createDynObj.Click += new System.EventHandler(this.createDynObj_Click);
            // 
            // removeDynObj
            // 
            this.removeDynObj.Name = "removeDynObj";
            this.removeDynObj.Size = new System.Drawing.Size(202, 22);
            this.removeDynObj.Text = "Remove dynamic object";
            this.removeDynObj.Click += new System.EventHandler(this.removeDynObj_Click);
            // 
            // copyDynObj
            // 
            this.copyDynObj.Name = "copyDynObj";
            this.copyDynObj.Size = new System.Drawing.Size(202, 22);
            this.copyDynObj.Text = "Clone dynamic object";
            this.copyDynObj.Click += new System.EventHandler(this.copyDynObj_Click);
            // 
            // exportDynObj
            // 
            this.exportDynObj.Image = global::NPCEditor.Properties.Resources.export;
            this.exportDynObj.Name = "exportDynObj";
            this.exportDynObj.Size = new System.Drawing.Size(202, 22);
            this.exportDynObj.Text = "Export NPC";
            this.exportDynObj.Click += new System.EventHandler(this.exportDynObj_Click);
            // 
            // importDynObj
            // 
            this.importDynObj.Image = global::NPCEditor.Properties.Resources.import;
            this.importDynObj.Name = "importDynObj";
            this.importDynObj.Size = new System.Drawing.Size(202, 22);
            this.importDynObj.Text = "Import NPC";
            this.importDynObj.Click += new System.EventHandler(this.importDynObj_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(415, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(612, 511);
            this.panel3.TabIndex = 7;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.panel6, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(612, 511);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 156);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(606, 352);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Preview";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(3, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(600, 333);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.textBox42);
            this.panel6.Controls.Add(this.tableLayoutPanel14);
            this.panel6.Controls.Add(this.label49);
            this.panel6.Controls.Add(this.label40);
            this.panel6.Controls.Add(this.comboBox2);
            this.panel6.Controls.Add(this.label45);
            this.panel6.Controls.Add(this.textBox51);
            this.panel6.Controls.Add(this.label46);
            this.panel6.Controls.Add(this.textBox52);
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(606, 147);
            this.panel6.TabIndex = 31;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(76, 8);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(129, 20);
            this.textBox42.TabIndex = 16;
            this.textBox42.TextChanged += new System.EventHandler(this.textBox42_TextChanged);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 4;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.69146F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.1405F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.2011F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.51791F));
            this.tableLayoutPanel14.Controls.Add(this.label55, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.textBox43, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.label41, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.label42, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.label43, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.label44, 3, 0);
            this.tableLayoutPanel14.Controls.Add(this.textBox55, 1, 2);
            this.tableLayoutPanel14.Controls.Add(this.textBox44, 2, 1);
            this.tableLayoutPanel14.Controls.Add(this.textBox45, 3, 1);
            this.tableLayoutPanel14.Controls.Add(this.textBox56, 2, 2);
            this.tableLayoutPanel14.Controls.Add(this.textBox53, 3, 2);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(211, 6);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(391, 126);
            this.tableLayoutPanel14.TabIndex = 17;
            // 
            // label55
            // 
            this.label55.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(3, 100);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(26, 13);
            this.label55.TabIndex = 29;
            this.label55.Text = "vDir";
            // 
            // textBox43
            // 
            this.textBox43.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox43.Location = new System.Drawing.Point(100, 54);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(72, 20);
            this.textBox43.TabIndex = 8;
            this.textBox43.TextChanged += new System.EventHandler(this.textBox43_TextChanged);
            // 
            // label41
            // 
            this.label41.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(3, 58);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(31, 13);
            this.label41.TabIndex = 8;
            this.label41.Text = "vPos";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(129, 14);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(14, 13);
            this.label42.TabIndex = 4;
            this.label42.Text = "X";
            // 
            // label43
            // 
            this.label43.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(230, 14);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(14, 13);
            this.label43.TabIndex = 5;
            this.label43.Text = "Y";
            // 
            // label44
            // 
            this.label44.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(335, 14);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(14, 13);
            this.label44.TabIndex = 6;
            this.label44.Text = "Z";
            // 
            // textBox55
            // 
            this.textBox55.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox55.Location = new System.Drawing.Point(100, 97);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(72, 20);
            this.textBox55.TabIndex = 30;
            this.textBox55.TextChanged += new System.EventHandler(this.textBox55_TextChanged);
            // 
            // textBox44
            // 
            this.textBox44.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox44.Location = new System.Drawing.Point(195, 54);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(85, 20);
            this.textBox44.TabIndex = 11;
            this.textBox44.TextChanged += new System.EventHandler(this.textBox44_TextChanged);
            // 
            // textBox45
            // 
            this.textBox45.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox45.Location = new System.Drawing.Point(305, 54);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(75, 20);
            this.textBox45.TabIndex = 12;
            this.textBox45.TextChanged += new System.EventHandler(this.textBox45_TextChanged);
            // 
            // textBox56
            // 
            this.textBox56.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox56.Location = new System.Drawing.Point(193, 97);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(89, 20);
            this.textBox56.TabIndex = 32;
            this.textBox56.TextChanged += new System.EventHandler(this.textBox56_TextChanged);
            // 
            // textBox53
            // 
            this.textBox53.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox53.Location = new System.Drawing.Point(303, 97);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(79, 20);
            this.textBox53.TabIndex = 23;
            this.textBox53.TextChanged += new System.EventHandler(this.textBox53_TextChanged);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(2, 37);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(45, 13);
            this.label49.TabIndex = 27;
            this.label49.Text = "szName";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(2, 12);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(67, 13);
            this.label40.TabIndex = 15;
            this.label40.Text = "dwDynObjID";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(76, 33);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(129, 21);
            this.comboBox2.TabIndex = 26;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(2, 61);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(59, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "idController";
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(76, 57);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(129, 20);
            this.textBox51.TabIndex = 19;
            this.textBox51.TextChanged += new System.EventHandler(this.textBox51_TextChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(2, 85);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(32, 13);
            this.label46.TabIndex = 20;
            this.label46.Text = "scale";
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(76, 81);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(129, 20);
            this.textBox52.TabIndex = 21;
            this.textBox52.TextChanged += new System.EventHandler(this.textBox52_TextChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel8);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1030, 517);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Triggers";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel8.Controls.Add(this.dataGridView3, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.panel4, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1030, 517);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeColumns = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView3.ColumnHeadersVisible = false;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3});
            this.dataGridView3.ContextMenuStrip = this.triggerMenu;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.ShowCellErrors = false;
            this.dataGridView3.ShowCellToolTips = false;
            this.dataGridView3.ShowEditingIcon = false;
            this.dataGridView3.ShowRowErrors = false;
            this.dataGridView3.Size = new System.Drawing.Size(406, 511);
            this.dataGridView3.TabIndex = 6;
            this.dataGridView3.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            this.dataGridView3.SelectionChanged += new System.EventHandler(this.dataGridView3_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "ID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // triggerMenu
            // 
            this.triggerMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTrigger,
            this.removeTrigger,
            this.copyTrigger,
            this.exportTrigger,
            this.importTrigger});
            this.triggerMenu.Name = "contextMenuStrip2";
            this.triggerMenu.Size = new System.Drawing.Size(156, 136);
            this.triggerMenu.Opening += new System.ComponentModel.CancelEventHandler(this.triggerMenu_Opening);
            // 
            // createTrigger
            // 
            this.createTrigger.Name = "createTrigger";
            this.createTrigger.Size = new System.Drawing.Size(155, 22);
            this.createTrigger.Text = "Create trigger";
            this.createTrigger.Click += new System.EventHandler(this.createTrigger_Click);
            // 
            // removeTrigger
            // 
            this.removeTrigger.Name = "removeTrigger";
            this.removeTrigger.Size = new System.Drawing.Size(155, 22);
            this.removeTrigger.Text = "Remove trigger";
            this.removeTrigger.Click += new System.EventHandler(this.removeTrigger_Click);
            // 
            // copyTrigger
            // 
            this.copyTrigger.Name = "copyTrigger";
            this.copyTrigger.Size = new System.Drawing.Size(155, 22);
            this.copyTrigger.Text = "Clone trigger";
            this.copyTrigger.Click += new System.EventHandler(this.copyTrigger_Click);
            // 
            // exportTrigger
            // 
            this.exportTrigger.Image = global::NPCEditor.Properties.Resources.export;
            this.exportTrigger.Name = "exportTrigger";
            this.exportTrigger.Size = new System.Drawing.Size(155, 22);
            this.exportTrigger.Text = "Export NPC";
            this.exportTrigger.Click += new System.EventHandler(this.exportTrigger_Click);
            // 
            // importTrigger
            // 
            this.importTrigger.Image = global::NPCEditor.Properties.Resources.import;
            this.importTrigger.Name = "importTrigger";
            this.importTrigger.Size = new System.Drawing.Size(155, 22);
            this.importTrigger.Text = "Import NPC";
            this.importTrigger.Click += new System.EventHandler(this.importTrigger_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel15);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(415, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(612, 511);
            this.panel4.TabIndex = 7;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.groupBox4, 0, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 58F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(612, 511);
            this.tableLayoutPanel15.TabIndex = 61;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.checkBox13);
            this.panel7.Controls.Add(this.checkBox12);
            this.panel7.Controls.Add(this.checkBox11);
            this.panel7.Controls.Add(this.textBox54);
            this.panel7.Controls.Add(this.label63);
            this.panel7.Controls.Add(this.textBox64);
            this.panel7.Controls.Add(this.label57);
            this.panel7.Controls.Add(this.textBox58);
            this.panel7.Controls.Add(this.label56);
            this.panel7.Controls.Add(this.textBox57);
            this.panel7.Controls.Add(this.label48);
            this.panel7.Controls.Add(this.label38);
            this.panel7.Controls.Add(this.comboBox4);
            this.panel7.Controls.Add(this.textBox59);
            this.panel7.Controls.Add(this.textBox67);
            this.panel7.Controls.Add(this.label58);
            this.panel7.Controls.Add(this.textBox68);
            this.panel7.Controls.Add(this.label59);
            this.panel7.Controls.Add(this.textBox69);
            this.panel7.Controls.Add(this.label60);
            this.panel7.Controls.Add(this.textBox70);
            this.panel7.Controls.Add(this.textBox60);
            this.panel7.Controls.Add(this.textBox71);
            this.panel7.Controls.Add(this.textBox61);
            this.panel7.Controls.Add(this.comboBox3);
            this.panel7.Controls.Add(this.textBox62);
            this.panel7.Controls.Add(this.textBox66);
            this.panel7.Controls.Add(this.textBox63);
            this.panel7.Controls.Add(this.textBox65);
            this.panel7.Controls.Add(this.label67);
            this.panel7.Controls.Add(this.label66);
            this.panel7.Controls.Add(this.label65);
            this.panel7.Controls.Add(this.label64);
            this.panel7.Controls.Add(this.label62);
            this.panel7.Controls.Add(this.label61);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(606, 208);
            this.panel7.TabIndex = 60;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(392, 155);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(108, 17);
            this.checkBox13.TabIndex = 39;
            this.checkBox13.Text = "bStopTimeInvalid";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.checkBox13_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(392, 130);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(116, 17);
            this.checkBox12.TabIndex = 38;
            this.checkBox12.Text = "bActiveTimeInvalid";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.checkBox12_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(392, 182);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(77, 17);
            this.checkBox11.TabIndex = 37;
            this.checkBox11.Text = "bActivated";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.checkBox11_CheckedChanged);
            // 
            // textBox54
            // 
            this.textBox54.Location = new System.Drawing.Point(140, 102);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(246, 20);
            this.textBox54.TabIndex = 23;
            this.textBox54.TextChanged += new System.EventHandler(this.textBox54_TextChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 8);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(32, 13);
            this.label63.TabIndex = 15;
            this.label63.Text = "dwID";
            // 
            // textBox64
            // 
            this.textBox64.Location = new System.Drawing.Point(140, 3);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(246, 20);
            this.textBox64.TabIndex = 16;
            this.textBox64.TextChanged += new System.EventHandler(this.textBox64_TextChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(258, 133);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(10, 13);
            this.label65.TabIndex = 57;
            this.label65.Text = ":";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 56);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(64, 13);
            this.label57.TabIndex = 18;
            this.label57.Text = "iControllerID";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(203, 136);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(10, 13);
            this.label66.TabIndex = 56;
            this.label66.Text = ".";
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(140, 53);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(246, 20);
            this.textBox58.TabIndex = 19;
            this.textBox58.TextChanged += new System.EventHandler(this.textBox58_TextChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(172, 136);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(10, 13);
            this.label67.TabIndex = 55;
            this.label67.Text = ".";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 82);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(54, 13);
            this.label56.TabIndex = 20;
            this.label56.Text = "iWaitTime";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(258, 159);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(10, 13);
            this.label64.TabIndex = 54;
            this.label64.Text = ":";
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(140, 79);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(246, 20);
            this.textBox57.TabIndex = 21;
            this.textBox57.TextChanged += new System.EventHandler(this.textBox57_TextChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(203, 160);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(10, 13);
            this.label62.TabIndex = 53;
            this.label62.Text = ".";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 107);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(54, 13);
            this.label48.TabIndex = 22;
            this.label48.Text = "iStopTime";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(172, 160);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(10, 13);
            this.label61.TabIndex = 52;
            this.label61.Text = ".";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 33);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 13);
            this.label38.TabIndex = 27;
            this.label38.Text = "szName";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Everyday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.comboBox4.Location = new System.Drawing.Point(295, 154);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(91, 21);
            this.comboBox4.TabIndex = 51;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(140, 26);
            this.textBox59.MaxLength = 128;
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(246, 20);
            this.textBox59.TabIndex = 28;
            this.textBox59.TextChanged += new System.EventHandler(this.textBox59_TextChanged);
            // 
            // textBox67
            // 
            this.textBox67.Location = new System.Drawing.Point(266, 155);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(23, 20);
            this.textBox67.TabIndex = 50;
            this.textBox67.TextChanged += new System.EventHandler(this.textBox67_TextChanged);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 134);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(60, 13);
            this.label58.TabIndex = 29;
            this.label58.Text = "ActiveTime";
            // 
            // textBox68
            // 
            this.textBox68.Location = new System.Drawing.Point(237, 155);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(23, 20);
            this.textBox68.TabIndex = 49;
            this.textBox68.TextChanged += new System.EventHandler(this.textBox68_TextChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 158);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(52, 13);
            this.label59.TabIndex = 31;
            this.label59.Text = "StopTime";
            // 
            // textBox69
            // 
            this.textBox69.Location = new System.Drawing.Point(140, 155);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(33, 20);
            this.textBox69.TabIndex = 48;
            this.textBox69.TextChanged += new System.EventHandler(this.textBox69_TextChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 184);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(94, 13);
            this.label60.TabIndex = 35;
            this.label60.Text = "iActiveTimeRange";
            // 
            // textBox70
            // 
            this.textBox70.Location = new System.Drawing.Point(182, 155);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(23, 20);
            this.textBox70.TabIndex = 47;
            this.textBox70.TextChanged += new System.EventHandler(this.textBox70_TextChanged);
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(140, 181);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(246, 20);
            this.textBox60.TabIndex = 36;
            this.textBox60.TextChanged += new System.EventHandler(this.textBox60_TextChanged);
            // 
            // textBox71
            // 
            this.textBox71.Location = new System.Drawing.Point(210, 155);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(23, 20);
            this.textBox71.TabIndex = 46;
            this.textBox71.TextChanged += new System.EventHandler(this.textBox71_TextChanged);
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(210, 130);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(23, 20);
            this.textBox61.TabIndex = 40;
            this.textBox61.TextChanged += new System.EventHandler(this.textBox61_TextChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Everyday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.comboBox3.Location = new System.Drawing.Point(295, 128);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(91, 21);
            this.comboBox3.TabIndex = 45;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(182, 130);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(23, 20);
            this.textBox62.TabIndex = 41;
            this.textBox62.TextChanged += new System.EventHandler(this.textBox62_TextChanged);
            // 
            // textBox66
            // 
            this.textBox66.Location = new System.Drawing.Point(266, 129);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(23, 20);
            this.textBox66.TabIndex = 44;
            this.textBox66.TextChanged += new System.EventHandler(this.textBox66_TextChanged);
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(140, 130);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(33, 20);
            this.textBox63.TabIndex = 42;
            this.textBox63.TextChanged += new System.EventHandler(this.textBox63_TextChanged);
            // 
            // textBox65
            // 
            this.textBox65.Location = new System.Drawing.Point(237, 129);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(23, 20);
            this.textBox65.TabIndex = 43;
            this.textBox65.TextChanged += new System.EventHandler(this.textBox65_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dataGridView6);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 217);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(606, 291);
            this.groupBox4.TabIndex = 59;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Objects where this trigger used";
            // 
            // dataGridView6
            // 
            this.dataGridView6.AllowUserToAddRows = false;
            this.dataGridView6.AllowUserToDeleteRows = false;
            this.dataGridView6.AllowUserToResizeColumns = false;
            this.dataGridView6.AllowUserToResizeRows = false;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView6.ColumnHeadersVisible = false;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5});
            this.dataGridView6.ContextMenuStrip = this.triggerMenu;
            this.dataGridView6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView6.Location = new System.Drawing.Point(3, 16);
            this.dataGridView6.MultiSelect = false;
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.ReadOnly = true;
            this.dataGridView6.RowHeadersVisible = false;
            this.dataGridView6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView6.ShowCellErrors = false;
            this.dataGridView6.ShowCellToolTips = false;
            this.dataGridView6.ShowEditingIcon = false;
            this.dataGridView6.ShowRowErrors = false;
            this.dataGridView6.Size = new System.Drawing.Size(600, 272);
            this.dataGridView6.TabIndex = 7;
            this.dataGridView6.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView6_CellMouseDoubleClick);
            this.dataGridView6.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "ID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel12);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1030, 517);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Search";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel12.Controls.Add(this.dataGridView4, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 517F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 517F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1030, 517);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.AllowUserToResizeColumns = false;
            this.dataGridView4.AllowUserToResizeRows = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.ColumnHeadersVisible = false;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4});
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Location = new System.Drawing.Point(3, 3);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowHeadersVisible = false;
            this.dataGridView4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView4.ShowCellErrors = false;
            this.dataGridView4.ShowCellToolTips = false;
            this.dataGridView4.ShowEditingIcon = false;
            this.dataGridView4.ShowRowErrors = false;
            this.dataGridView4.Size = new System.Drawing.Size(406, 511);
            this.dataGridView4.TabIndex = 7;
            this.dataGridView4.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView4_CellMouseDoubleClick);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "ID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.tableLayoutPanel13);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(415, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(612, 511);
            this.panel5.TabIndex = 0;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 3;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.label68, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.searchText, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.checkBox14, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.searchType, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.label71, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.label69, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.searchCategory, 1, 2);
            this.tableLayoutPanel13.Controls.Add(this.textBox14, 1, 3);
            this.tableLayoutPanel13.Controls.Add(this.label72, 0, 3);
            this.tableLayoutPanel13.Controls.Add(this.checkBox15, 2, 3);
            this.tableLayoutPanel13.Controls.Add(this.button1, 1, 4);
            this.tableLayoutPanel13.Controls.Add(this.checkBox16, 2, 1);
            this.tableLayoutPanel13.Controls.Add(this.checkBox17, 2, 2);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 6;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.69004F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.69004F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.69004F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.38546F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.22643F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.31799F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(612, 239);
            this.tableLayoutPanel13.TabIndex = 44;
            // 
            // label68
            // 
            this.label68.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(14, 13);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(61, 13);
            this.label68.TabIndex = 32;
            this.label68.Text = "Search text";
            // 
            // searchText
            // 
            this.searchText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.searchText.Location = new System.Drawing.Point(81, 9);
            this.searchText.Name = "searchText";
            this.searchText.Size = new System.Drawing.Size(261, 20);
            this.searchText.TabIndex = 33;
            // 
            // checkBox14
            // 
            this.checkBox14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBox14.AutoSize = true;
            this.checkBox14.Checked = true;
            this.checkBox14.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox14.Location = new System.Drawing.Point(348, 11);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(85, 17);
            this.checkBox14.TabIndex = 40;
            this.checkBox14.Text = "Exact match";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // searchType
            // 
            this.searchType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.searchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.searchType.FormattingEnabled = true;
            this.searchType.Location = new System.Drawing.Point(81, 48);
            this.searchType.Name = "searchType";
            this.searchType.Size = new System.Drawing.Size(261, 21);
            this.searchType.TabIndex = 38;
            // 
            // label71
            // 
            this.label71.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(20, 52);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(55, 13);
            this.label71.TabIndex = 39;
            this.label71.Text = "Search by";
            // 
            // label69
            // 
            this.label69.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(23, 91);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(52, 13);
            this.label69.TabIndex = 35;
            this.label69.Text = "Search in";
            // 
            // searchCategory
            // 
            this.searchCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.searchCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.searchCategory.FormattingEnabled = true;
            this.searchCategory.Location = new System.Drawing.Point(81, 87);
            this.searchCategory.Name = "searchCategory";
            this.searchCategory.Size = new System.Drawing.Size(261, 21);
            this.searchCategory.TabIndex = 34;
            // 
            // textBox14
            // 
            this.textBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox14.Location = new System.Drawing.Point(81, 127);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(261, 20);
            this.textBox14.TabIndex = 43;
            this.textBox14.Text = "50";
            this.textBox14.TextChanged += new System.EventHandler(this.textBox14_TextChanged_1);
            // 
            // label72
            // 
            this.label72.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(3, 131);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(72, 13);
            this.label72.TabIndex = 42;
            this.label72.Text = "Search radius";
            // 
            // checkBox15
            // 
            this.checkBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(348, 122);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(261, 30);
            this.checkBox15.TabIndex = 41;
            this.checkBox15.Text = "Find only NPC in selected radius \r\naround character.";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(174, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox16
            // 
            this.checkBox16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBox16.AutoSize = true;
            this.checkBox16.Checked = true;
            this.checkBox16.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox16.Location = new System.Drawing.Point(348, 50);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(94, 17);
            this.checkBox16.TabIndex = 44;
            this.checkBox16.Text = "Case sensitive";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(348, 89);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(261, 17);
            this.checkBox17.TabIndex = 45;
            this.checkBox17.Text = "Search everywhere";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point(3, 552);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1038, 17);
            this.progressBar1.TabIndex = 1;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openNPCDataMenuItem,
            this.openElementsdataToolStripMenuItem,
            this.saveNPCDataMenuItem,
            this.saveAsNPCDataMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openNPCDataMenuItem
            // 
            this.openNPCDataMenuItem.Name = "openNPCDataMenuItem";
            this.openNPCDataMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openNPCDataMenuItem.Size = new System.Drawing.Size(184, 22);
            this.openNPCDataMenuItem.Text = "Open";
            this.openNPCDataMenuItem.Click += new System.EventHandler(this.openNPCDataMenuItem_Click);
            // 
            // openElementsdataToolStripMenuItem
            // 
            this.openElementsdataToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.open;
            this.openElementsdataToolStripMenuItem.Name = "openElementsdataToolStripMenuItem";
            this.openElementsdataToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.openElementsdataToolStripMenuItem.Text = "Open elements.data";
            this.openElementsdataToolStripMenuItem.Click += new System.EventHandler(this.openElementsdataToolStripMenuItem_Click);
            // 
            // saveNPCDataMenuItem
            // 
            this.saveNPCDataMenuItem.Name = "saveNPCDataMenuItem";
            this.saveNPCDataMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveNPCDataMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveNPCDataMenuItem.Text = "Save";
            this.saveNPCDataMenuItem.Click += new System.EventHandler(this.saveNPCDataMenuItem_Click);
            // 
            // saveAsNPCDataMenuItem
            // 
            this.saveAsNPCDataMenuItem.Name = "saveAsNPCDataMenuItem";
            this.saveAsNPCDataMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsNPCDataMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveAsNPCDataMenuItem.Text = "Save as";
            this.saveAsNPCDataMenuItem.Click += new System.EventHandler(this.saveAsNPCDataMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languageToolStripMenuItem,
            this.clientVersionToolStripMenuItem,
            this.clientProcessNameToolStripMenuItem,
            this.databaseToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            this.languageToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.languageToolStripMenuItem.Text = "Language";
            // 
            // clientVersionToolStripMenuItem
            // 
            this.clientVersionToolStripMenuItem.Name = "clientVersionToolStripMenuItem";
            this.clientVersionToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.clientVersionToolStripMenuItem.Text = "Client version";
            // 
            // clientProcessNameToolStripMenuItem
            // 
            this.clientProcessNameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.clientProcessNameToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.triggers;
            this.clientProcessNameToolStripMenuItem.Name = "clientProcessNameToolStripMenuItem";
            this.clientProcessNameToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.clientProcessNameToolStripMenuItem.Text = "Client process name";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.russianToolStripMenuItem,
            this.englishPWIToolStripMenuItem,
            this.pWIEUFrenchToolStripMenuItem,
            this.pWIEUGermanToolStripMenuItem,
            this.pWChinaToolStripMenuItem,
            this.pWBrazilToolStripMenuItem,
            this.pWJapanToolStripMenuItem,
            this.pWPhiliToolStripMenuItem,
            this.malaysianEnglishToolStripMenuItem});
            this.databaseToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.pwdb;
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // russianToolStripMenuItem
            // 
            this.russianToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.russian;
            this.russianToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.russianToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.russianToolStripMenuItem.Name = "russianToolStripMenuItem";
            this.russianToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.russianToolStripMenuItem.Tag = "0";
            this.russianToolStripMenuItem.Text = "Russian";
            this.russianToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // englishPWIToolStripMenuItem
            // 
            this.englishPWIToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.pwi;
            this.englishPWIToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.englishPWIToolStripMenuItem.Name = "englishPWIToolStripMenuItem";
            this.englishPWIToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.englishPWIToolStripMenuItem.Tag = "1";
            this.englishPWIToolStripMenuItem.Text = "PWI";
            this.englishPWIToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // pWIEUFrenchToolStripMenuItem
            // 
            this.pWIEUFrenchToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.french;
            this.pWIEUFrenchToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pWIEUFrenchToolStripMenuItem.Name = "pWIEUFrenchToolStripMenuItem";
            this.pWIEUFrenchToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pWIEUFrenchToolStripMenuItem.Tag = "2";
            this.pWIEUFrenchToolStripMenuItem.Text = "PWI - EU - French";
            this.pWIEUFrenchToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // pWIEUGermanToolStripMenuItem
            // 
            this.pWIEUGermanToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.german;
            this.pWIEUGermanToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pWIEUGermanToolStripMenuItem.Name = "pWIEUGermanToolStripMenuItem";
            this.pWIEUGermanToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pWIEUGermanToolStripMenuItem.Tag = "3";
            this.pWIEUGermanToolStripMenuItem.Text = "PWI - EU - German";
            this.pWIEUGermanToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // pWChinaToolStripMenuItem
            // 
            this.pWChinaToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.china;
            this.pWChinaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pWChinaToolStripMenuItem.Name = "pWChinaToolStripMenuItem";
            this.pWChinaToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pWChinaToolStripMenuItem.Tag = "4";
            this.pWChinaToolStripMenuItem.Text = "PW - China";
            this.pWChinaToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // pWBrazilToolStripMenuItem
            // 
            this.pWBrazilToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.brazil;
            this.pWBrazilToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pWBrazilToolStripMenuItem.Name = "pWBrazilToolStripMenuItem";
            this.pWBrazilToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pWBrazilToolStripMenuItem.Tag = "5";
            this.pWBrazilToolStripMenuItem.Text = "PW - Brazil";
            this.pWBrazilToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // pWJapanToolStripMenuItem
            // 
            this.pWJapanToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.japan;
            this.pWJapanToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pWJapanToolStripMenuItem.Name = "pWJapanToolStripMenuItem";
            this.pWJapanToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pWJapanToolStripMenuItem.Tag = "6";
            this.pWJapanToolStripMenuItem.Text = "PW - Japan";
            this.pWJapanToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // pWPhiliToolStripMenuItem
            // 
            this.pWPhiliToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.ph;
            this.pWPhiliToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pWPhiliToolStripMenuItem.Name = "pWPhiliToolStripMenuItem";
            this.pWPhiliToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.pWPhiliToolStripMenuItem.Tag = "7";
            this.pWPhiliToolStripMenuItem.Text = "PW - Philippines";
            this.pWPhiliToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // malaysianEnglishToolStripMenuItem
            // 
            this.malaysianEnglishToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.my;
            this.malaysianEnglishToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.malaysianEnglishToolStripMenuItem.Name = "malaysianEnglishToolStripMenuItem";
            this.malaysianEnglishToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.malaysianEnglishToolStripMenuItem.Tag = "8";
            this.malaysianEnglishToolStripMenuItem.Text = "Malaysian/English";
            this.malaysianEnglishToolStripMenuItem.Click += new System.EventHandler(this.russianToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // settingsMenu
            // 
            this.settingsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.toolStripMenuItem1});
            this.settingsMenu.Location = new System.Drawing.Point(0, 0);
            this.settingsMenu.Name = "settingsMenu";
            this.settingsMenu.Size = new System.Drawing.Size(1044, 24);
            this.settingsMenu.TabIndex = 0;
            this.settingsMenu.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dynamicobjectsdataEditorToolStripMenuItem,
            this.converterToolStripMenuItem,
            this.exportToCoordsdatatxtToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // dynamicobjectsdataEditorToolStripMenuItem
            // 
            this.dynamicobjectsdataEditorToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.dynobjs;
            this.dynamicobjectsdataEditorToolStripMenuItem.Name = "dynamicobjectsdataEditorToolStripMenuItem";
            this.dynamicobjectsdataEditorToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.dynamicobjectsdataEditorToolStripMenuItem.Text = "dynamicobjects.data editor";
            this.dynamicobjectsdataEditorToolStripMenuItem.Click += new System.EventHandler(this.dynamicobjectsdataEditorToolStripMenuItem_Click);
            // 
            // converterToolStripMenuItem
            // 
            this.converterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.v1ToolStripMenuItem,
            this.v2ToolStripMenuItem,
            this.v3ToolStripMenuItem,
            this.v4ToolStripMenuItem,
            this.v5ToolStripMenuItem,
            this.v6ToolStripMenuItem,
            this.v7ToolStripMenuItem,
            this.v8ToolStripMenuItem,
            this.v9ToolStripMenuItem,
            this.v10ToolStripMenuItem,
            this.v11ToolStripMenuItem});
            this.converterToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("converterToolStripMenuItem.Image")));
            this.converterToolStripMenuItem.Name = "converterToolStripMenuItem";
            this.converterToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.converterToolStripMenuItem.Text = "Change npcdata version";
            // 
            // v1ToolStripMenuItem
            // 
            this.v1ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v1ToolStripMenuItem.Image")));
            this.v1ToolStripMenuItem.Name = "v1ToolStripMenuItem";
            this.v1ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v1ToolStripMenuItem.Tag = "1";
            this.v1ToolStripMenuItem.Text = "v1";
            this.v1ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v2ToolStripMenuItem
            // 
            this.v2ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v2ToolStripMenuItem.Image")));
            this.v2ToolStripMenuItem.Name = "v2ToolStripMenuItem";
            this.v2ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v2ToolStripMenuItem.Tag = "2";
            this.v2ToolStripMenuItem.Text = "v2";
            this.v2ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v3ToolStripMenuItem
            // 
            this.v3ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v3ToolStripMenuItem.Image")));
            this.v3ToolStripMenuItem.Name = "v3ToolStripMenuItem";
            this.v3ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v3ToolStripMenuItem.Tag = "3";
            this.v3ToolStripMenuItem.Text = "v3";
            this.v3ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v4ToolStripMenuItem
            // 
            this.v4ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v4ToolStripMenuItem.Image")));
            this.v4ToolStripMenuItem.Name = "v4ToolStripMenuItem";
            this.v4ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v4ToolStripMenuItem.Tag = "4";
            this.v4ToolStripMenuItem.Text = "v4";
            this.v4ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v5ToolStripMenuItem
            // 
            this.v5ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v5ToolStripMenuItem.Image")));
            this.v5ToolStripMenuItem.Name = "v5ToolStripMenuItem";
            this.v5ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v5ToolStripMenuItem.Tag = "5";
            this.v5ToolStripMenuItem.Text = "v5";
            this.v5ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v6ToolStripMenuItem
            // 
            this.v6ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v6ToolStripMenuItem.Image")));
            this.v6ToolStripMenuItem.Name = "v6ToolStripMenuItem";
            this.v6ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v6ToolStripMenuItem.Tag = "6";
            this.v6ToolStripMenuItem.Text = "v6";
            this.v6ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v7ToolStripMenuItem
            // 
            this.v7ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v7ToolStripMenuItem.Image")));
            this.v7ToolStripMenuItem.Name = "v7ToolStripMenuItem";
            this.v7ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v7ToolStripMenuItem.Tag = "7";
            this.v7ToolStripMenuItem.Text = "v7";
            this.v7ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v8ToolStripMenuItem
            // 
            this.v8ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v8ToolStripMenuItem.Image")));
            this.v8ToolStripMenuItem.Name = "v8ToolStripMenuItem";
            this.v8ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v8ToolStripMenuItem.Tag = "8";
            this.v8ToolStripMenuItem.Text = "v8";
            this.v8ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v9ToolStripMenuItem
            // 
            this.v9ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v9ToolStripMenuItem.Image")));
            this.v9ToolStripMenuItem.Name = "v9ToolStripMenuItem";
            this.v9ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v9ToolStripMenuItem.Tag = "9";
            this.v9ToolStripMenuItem.Text = "v9";
            this.v9ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v10ToolStripMenuItem
            // 
            this.v10ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v10ToolStripMenuItem.Image")));
            this.v10ToolStripMenuItem.Name = "v10ToolStripMenuItem";
            this.v10ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v10ToolStripMenuItem.Tag = "10";
            this.v10ToolStripMenuItem.Text = "v10";
            this.v10ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // v11ToolStripMenuItem
            // 
            this.v11ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("v11ToolStripMenuItem.Image")));
            this.v11ToolStripMenuItem.Name = "v11ToolStripMenuItem";
            this.v11ToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.v11ToolStripMenuItem.Tag = "11";
            this.v11ToolStripMenuItem.Text = "v11";
            this.v11ToolStripMenuItem.Click += new System.EventHandler(this.v1ToolStripMenuItem_Click);
            // 
            // exportToCoordsdatatxtToolStripMenuItem
            // 
            this.exportToCoordsdatatxtToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.open;
            this.exportToCoordsdatatxtToolStripMenuItem.Name = "exportToCoordsdatatxtToolStripMenuItem";
            this.exportToCoordsdatatxtToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.exportToCoordsdatatxtToolStripMenuItem.Text = "Экспорт НПС в coord_data.txt";
            this.exportToCoordsdatatxtToolStripMenuItem.Click += new System.EventHandler(this.exportToCoordsdatatxtToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.toolStripMenuItem1.Size = new System.Drawing.Size(125, 20);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 596);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.settingsMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "Regular NPC Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.npcList)).EndInit();
            this.npcListMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.npcList1)).EndInit();
            this.npcList1Menu.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.resList1Menu.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.resListMenu.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.dynObjMenu.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.triggerMenu.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.settingsMenu.ResumeLayout(false);
            this.settingsMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip npcListMenu;
        public System.Windows.Forms.ToolStripMenuItem createNpc;
        public System.Windows.Forms.ToolStripMenuItem removeNpc;
        public System.Windows.Forms.ToolStripMenuItem copyNpc;
        private System.Windows.Forms.ContextMenuStrip npcList1Menu;
        private System.Windows.Forms.ToolStripMenuItem createNpc1;
        private System.Windows.Forms.ToolStripMenuItem removeNpc1;
        private System.Windows.Forms.ToolStripMenuItem copyNpc1;
        private System.Windows.Forms.ContextMenuStrip resListMenu;
        private System.Windows.Forms.ToolStripMenuItem createResource;
        private System.Windows.Forms.ToolStripMenuItem removeResource;
        private System.Windows.Forms.ToolStripMenuItem copyResource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.ContextMenuStrip resList1Menu;
        private System.Windows.Forms.ToolStripMenuItem createResource1;
        private System.Windows.Forms.ToolStripMenuItem removeResource1;
        private System.Windows.Forms.ToolStripMenuItem copyResource1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ContextMenuStrip dynObjMenu;
        private System.Windows.Forms.ToolStripMenuItem createDynObj;
        private System.Windows.Forms.ToolStripMenuItem removeDynObj;
        private System.Windows.Forms.ToolStripMenuItem copyDynObj;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ContextMenuStrip triggerMenu;
        private System.Windows.Forms.ToolStripMenuItem createTrigger;
        private System.Windows.Forms.ToolStripMenuItem removeTrigger;
        private System.Windows.Forms.ToolStripMenuItem copyTrigger;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.ComboBox searchCategory;
        private System.Windows.Forms.TextBox searchText;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.ComboBox searchType;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openNPCDataMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveNPCDataMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsNPCDataMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.MenuStrip settingsMenu;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView npcList1;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.ToolStripMenuItem clientProcessNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem converterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v6ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v7ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v9ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v10ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem v11ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.ToolStripMenuItem openElementsdataToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem russianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishPWIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pWIEUFrenchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pWIEUGermanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pWChinaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pWBrazilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pWJapanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pWPhiliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem malaysianEnglishToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.ToolStripMenuItem dynamicobjectsdataEditorToolStripMenuItem;
        public System.Windows.Forms.DataGridView dataGridView3;
        public System.Windows.Forms.DataGridView npcList;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.ToolStripMenuItem exportToCoordsdatatxtToolStripMenuItem;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem exportNpc1;
        private System.Windows.Forms.ToolStripMenuItem exportNpc;
        private System.Windows.Forms.ToolStripMenuItem importNpc;
        private System.Windows.Forms.ToolStripMenuItem importNpc1;
        private System.Windows.Forms.ToolStripMenuItem exportRes;
        private System.Windows.Forms.ToolStripMenuItem importRes;
        private System.Windows.Forms.ToolStripMenuItem exportRes1;
        private System.Windows.Forms.ToolStripMenuItem importRes1;
        private System.Windows.Forms.ToolStripMenuItem exportDynObj;
        private System.Windows.Forms.ToolStripMenuItem importDynObj;
        private System.Windows.Forms.ToolStripMenuItem exportTrigger;
        private System.Windows.Forms.ToolStripMenuItem importTrigger;
    }
}

