﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace NPCEditor
{
    public partial class LicenseKeyWindow : Form
    {
        char[] a = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M' };
        char[] b = { 'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N' };
        public LicenseKeyWindow()
        {
            InitializeComponent();
            string uniqieid = identifier("Win32_BaseBoard", "SerialNumber");
            uniqieid += identifier("Win32_PhysicalMedia", "SerialNumber");
            uniqieid = ComputeHash(uniqieid, new MD5CryptoServiceProvider());
            textBox1.Text = uniqieid;
        }

        public static string getUniqieId()
        {
            string uniqieid = identifier("Win32_BaseBoard", "SerialNumber");
            uniqieid += identifier("Win32_PhysicalMedia", "SerialNumber");
            uniqieid = ComputeHash(uniqieid, new MD5CryptoServiceProvider());
            return uniqieid;
        }

        public static string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        private static string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string check = textBox1.Text;
            check = ComputeHash("G7-" + check + "-6T", new MD5CryptoServiceProvider());
            if (textBox2.Text == check)
            {
                DialogResult = DialogResult.OK;
                using (BinaryWriter bw = new BinaryWriter(File.Open(AppDomain.CurrentDomain.BaseDirectory + "//key.data", FileMode.OpenOrCreate, FileAccess.ReadWrite)))
                {
                    bw.Write(check);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }
    }
}
