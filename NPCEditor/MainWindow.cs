﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Management;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Collections;
using System.Reflection;

namespace NPCEditor
{
    public partial class MainWindow : Form
    {
        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        public NPCDATA npcdata;
        public Dictionary<uint, String> npcs = new Dictionary<uint, String>();
        public Dictionary<int, String> mines = new Dictionary<int, String>();
        public Dictionary<uint, String> dynobjs = new Dictionary<uint, String>();
        public Dictionary<String, uint> dynobjsInverse = new Dictionary<String, uint>();
        public Dictionary<string, Language> langs = new Dictionary<string, Language>();
        private List<Offset> offsets = new List<Offset>();
        private bool npcdataLoaded = false;
        private String filePath;
        private String filePath1;
        private Dictionary<uint, Image> dynobjImages = new Dictionary<uint, Image>();
        private static Dictionary<String, String> Settings = new Dictionary<String, String>();
        string appPath;
        bool saveDialog = false;
        bool updateVars = false;
        public static MainWindow instance;
        float PosX, PosY, PosZ, DirX, DirY, DirZ;
        public eListCollection eLC;
        public string[] itemlists;
        public SortedList InstanceList;
        public SortedList LocalizationText;
        public SortedList items;
        public SortedList monsters_npcs_mines;
        public SortedList titles;
        public SortedList homeitems;
        public SortedList addonslist;
        public DYNAMICOBJECTSDATA dynObjsdata;
        public string dynObjsFilePath;
        public string pathFilePath;
        public DynamicObjectWindow dynObjsWindow;
        public bool dynObjsDataLoaded = false;

        public MainWindow()
        {
            InitializeComponent();
            instance = this;
            appPath = Path.GetDirectoryName(Application.ExecutablePath);
            openNPCDataMenuItem.Image = NPCEditor.Properties.Resources.open;
            saveAsNPCDataMenuItem.Image = NPCEditor.Properties.Resources.save;
            saveNPCDataMenuItem.Image = NPCEditor.Properties.Resources.save;
            createNpc.Image = NPCEditor.Properties.Resources.add;
            createNpc1.Image = NPCEditor.Properties.Resources.add;
            removeNpc.Image = NPCEditor.Properties.Resources.remove;
            removeNpc1.Image = NPCEditor.Properties.Resources.remove;
            copyNpc.Image = NPCEditor.Properties.Resources.clone;
            copyNpc1.Image = NPCEditor.Properties.Resources.clone;
            createResource.Image = NPCEditor.Properties.Resources.add;
            createResource1.Image = NPCEditor.Properties.Resources.add;
            removeResource.Image = NPCEditor.Properties.Resources.remove;
            removeResource1.Image = NPCEditor.Properties.Resources.remove;
            copyResource.Image = NPCEditor.Properties.Resources.clone;
            copyResource1.Image = NPCEditor.Properties.Resources.clone;
            createDynObj.Image = NPCEditor.Properties.Resources.add;
            removeDynObj.Image = NPCEditor.Properties.Resources.remove;
            copyDynObj.Image = NPCEditor.Properties.Resources.clone;
            createTrigger.Image = NPCEditor.Properties.Resources.add;
            removeTrigger.Image = NPCEditor.Properties.Resources.remove;
            copyTrigger.Image = NPCEditor.Properties.Resources.clone;
            aboutToolStripMenuItem.Image = NPCEditor.Properties.Resources.about;
            languageToolStripMenuItem.Image = NPCEditor.Properties.Resources.lang;
            clientVersionToolStripMenuItem.Image = NPCEditor.Properties.Resources.ver;
            Icon = NPCEditor.Properties.Resources.appIcon;
            //loadNPCNames(appPath);
            loadLanguages(appPath);
            loadDynObjs(appPath);
            loadDynObjsImages(appPath);
            loadOffsets(appPath);
            loadSettings(appPath);
            ImageList imgs = new ImageList();
            imgs.Images.Add("npcs", NPCEditor.Properties.Resources.npcs);
            imgs.Images.Add("resources", NPCEditor.Properties.Resources.resources);
            imgs.Images.Add("dynobjs", NPCEditor.Properties.Resources.dynobjs);
            imgs.Images.Add("triggers", NPCEditor.Properties.Resources.triggers);
            imgs.Images.Add("search", NPCEditor.Properties.Resources.search);
            tabControl1.ImageList = imgs;
            tabPage1.ImageIndex = 0;
            tabPage2.ImageIndex = 1;
            tabPage3.ImageIndex = 2;
            tabPage4.ImageIndex = 3;
            tabPage5.ImageIndex = 4;
            SetDoubleBuffered(npcList, true);
            SetDoubleBuffered(dataGridView1, true);
            SetDoubleBuffered(dataGridView2, true);
            SetDoubleBuffered(dataGridView3, true);
            npcdata = new NPCDATA();
            npcdata.Header = new NPCGENFILEHEADER();
            npcdata.Header.iNumAIGen = 0;
            npcdata.Header.iNumDynObj = 0;
            npcdata.Header.iNumNPCCtrl = 0;
            npcdata.Header.iNumResArea = 0;
            foreach (ToolStripMenuItem it in languageToolStripMenuItem.DropDownItems)
            {
                if (it.Text == Settings["language"])
                {
                    it.Checked = true;
                    break;
                }
            }
            foreach (ToolStripMenuItem it in clientVersionToolStripMenuItem.DropDownItems)
            {
                if (it.Text == Settings["clientVersion"])
                {
                    it.Checked = true;
                    break;
                }
            }
            toolStripTextBox1.Text = Settings["processName"];
            ((ToolStripMenuItem)databaseToolStripMenuItem.DropDownItems[Int32.Parse(Settings["selectedDatabase"])]).Checked = true;
            ControlHelper.setupTooltip();
            ControlHelper.addButtonToTextBox(textBox11, Properties.Resources.search, "Go to this trigger", (o, e) =>
            {
                if (npcdataLoaded)
                {
                    uint value;
                    if (!uint.TryParse(textBox11.Text, out value)) return;
                    for (int i = 0; i < npcdata.Header.iNumNPCCtrl; i++)
                    {
                        if (npcdata.m_aControllers[i].id == value)
                        {
                            tabControl1.SelectedTab = tabPage4;
                            selectRow(dataGridView3, i);
                            break;
                        }
                    }
                    if (tabControl1.SelectedTab != tabPage4)
                    {
                        MessageBox.Show(getLocalizedString("This trigger not found!"), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            });
            ControlHelper.addButtonToTextBox(textBox37, Properties.Resources.search, "Go to this trigger", (o, e) =>
            {
                if (npcdataLoaded)
                {
                    uint value;
                    if (!uint.TryParse(textBox37.Text, out value)) return;
                    for (int i = 0; i < npcdata.Header.iNumNPCCtrl; i++)
                    {
                        if (npcdata.m_aControllers[i].id == value)
                        {
                            tabControl1.SelectedTab = tabPage4;
                            selectRow(dataGridView3, i);
                            break;
                        }
                    }
                    if (tabControl1.SelectedTab != tabPage4)
                    {
                        MessageBox.Show(getLocalizedString("This trigger not found!"), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            });
            ControlHelper.addButtonToTextBox(textBox51, Properties.Resources.search, "Go to this trigger", (o, e) =>
            {
                if (npcdataLoaded)
                {
                    uint value;
                    if (!uint.TryParse(textBox51.Text, out value)) return;
                    for (int i = 0; i < npcdata.Header.iNumNPCCtrl; i++)
                    {
                        if (npcdata.m_aControllers[i].id == value)
                        {
                            tabControl1.SelectedTab = tabPage4;
                            selectRow(dataGridView3, i);
                            break;
                        }
                    }
                    if (tabControl1.SelectedTab != tabPage4)
                    {
                        MessageBox.Show(getLocalizedString("This trigger not found!"), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            });
            ControlHelper.addButtonToTextBox(textBox16, Properties.Resources.pwdb, "Search this in PW database", (o, e) =>
            {
                if (npcdataLoaded)
                {
                    uint id;
                    if (!uint.TryParse(textBox16.Text, out id)) return;
                    openDatabase(id, "npc");
                }
            });
            ControlHelper.addButtonToTextBox(textBox49, Properties.Resources.pwdb, "Search this in PW database", (o, e) =>
            {
                if (npcdataLoaded)
                {
                    uint id;
                    if (!uint.TryParse(textBox16.Text, out id)) return;
                    openDatabase(id, "mine");
                }
            });
            ignoreControlsList.Add(searchText);
            ignoreControlsList.Add(checkBox14);
            ignoreControlsList.Add(checkBox16);
            ignoreControlsList.Add(searchType);
            ignoreControlsList.Add(searchCategory);
            ignoreControlsList.Add(checkBox15);
            ignoreControlsList.Add(textBox14);
            updateLanguage();
        }

        public float getCurrentSelectionX()
        {
            if (tabControl1.SelectedTab == tabPage1)
            {
                return npcdata.m_aAreas[npcList.SelectedRows[0].Index].vPos[0];
            }
            if (tabControl1.SelectedTab == tabPage2)
            {
                return npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].vPos[0];
            }
            if (tabControl1.SelectedTab == tabPage3)
            {
                return npcdata.m_aDynObjs[dataGridView2.SelectedRows[0].Index].vPos[0];
            }
            return 0f;
        }

        public float getCurrentSelectionY()
        {
            if (tabControl1.SelectedTab == tabPage1)
            {
                return npcdata.m_aAreas[npcList.SelectedRows[0].Index].vPos[1];
            }
            if (tabControl1.SelectedTab == tabPage2)
            {
                return npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].vPos[1];
            }
            if (tabControl1.SelectedTab == tabPage3)
            {
                return npcdata.m_aDynObjs[dataGridView2.SelectedRows[0].Index].vPos[1];
            }
            return 0f;
        }

        public float getCurrentSelectionZ()
        {
            if (tabControl1.SelectedTab == tabPage1)
            {
                return npcdata.m_aAreas[npcList.SelectedRows[0].Index].vPos[2];
            }
            if (tabControl1.SelectedTab == tabPage2)
            {
                return npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].vPos[2];
            }
            if (tabControl1.SelectedTab == tabPage3)
            {
                return npcdata.m_aDynObjs[dataGridView2.SelectedRows[0].Index].vPos[2];
            }
            return 0f;
        }

        public byte[] TrimByteArray(byte[] packet)
        {
            var i = packet.Length - 1;
            while (packet[i] == 0 && (i - 1) >= 0)
            {
                --i;
            }
            var temp = new byte[i + 1];
            Array.Copy(packet, temp, i + 1);
            return temp;
        }

        void openDatabase(uint id, string type)
        {
            string url = "http://www.pwdatabase.com/";
            switch (Int32.Parse(Settings["selectedDatabase"]))
            {
                case 0: url += "ru"; break;
                case 1: url += "pwi"; break;
                case 2: url += "fr"; break;
                case 3: url += "de"; break;
                case 4: url += "cn"; break;
                case 5: url += "br"; break;
                case 6: url += "jp"; break;
                case 7: url += "ph"; break;
                case 8: url += "my"; break;
            }
            url += "/" + type + "/" + id.ToString();
            System.Diagnostics.Process.Start(url);
        }

        private void AnyControl_StateChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && !updateVars)
            {
                this.Text = "Regular NPC Editor* [" + filePath + "] v" + npcdata.version;
                saveDialog = true;
            }
        }

        public String getLocalizedString(string value)
        {
            String result;
            if (Settings.TryGetValue("language", out result))
            {
                if (langs[result].values.TryGetValue(value, out result))
                {
                    return result.Replace(@"\n", System.Environment.NewLine);
                }
                else
                {
                    return value;
                }
            }
            else
            {
                return value;
            }
        }

        void updateLanguage()
        {
            fileToolStripMenuItem.Text = getLocalizedString("File");
            openNPCDataMenuItem.Text = getLocalizedString("Open");
            if (npcdataLoaded)
            {
                saveNPCDataMenuItem.Text = getLocalizedString("Save") + " (v" + npcdata.version + ")";
                saveAsNPCDataMenuItem.Text = getLocalizedString("Save As") + " (v" + npcdata.version + ")";
            }
            else
            {
                saveNPCDataMenuItem.Text = getLocalizedString("Save");
                saveAsNPCDataMenuItem.Text = getLocalizedString("Save As");
            }
            settingsToolStripMenuItem.Text = getLocalizedString("Settings");
            languageToolStripMenuItem.Text = getLocalizedString("Language");
            toolsToolStripMenuItem.Text = getLocalizedString("Tools");
            converterToolStripMenuItem.Text = getLocalizedString("Change npcgen.data version");
            tabPage1.Text = getLocalizedString("NPCs") + " [" + npcList.Rows.Count + "]";
            groupBox1.Text = getLocalizedString("NPCs group");
            tabPage2.Text = getLocalizedString("Resources") + " [" + dataGridView1.Rows.Count + "]";
            tabPage3.Text = getLocalizedString("Dynamic objects") + " [" + dataGridView2.Rows.Count + "]";
            tabPage4.Text = getLocalizedString("Triggers") + " [" + dataGridView3.Rows.Count + "]";
            label2.Text = getLocalizedString("iNPCType");
            comboBox1.Items.Clear();
            comboBox1.Items.Add(getLocalizedString("iNPCType0"));
            comboBox1.Items.Add(getLocalizedString("iNPCType1"));
            label9.Text = getLocalizedString("idCtrl");
            label10.Text = getLocalizedString("iLifeTime");
            label11.Text = getLocalizedString("iMaxNum");
            label12.Text = getLocalizedString("iType");
            label13.Text = getLocalizedString("dwGenID");
            label1.Text = getLocalizedString("iGroupType");
            checkBox3.Text = getLocalizedString("bValidOnce");
            checkBox2.Text = getLocalizedString("bAutoRevive");
            checkBox1.Text = getLocalizedString("bInitGen");
            label4.Text = getLocalizedString("vPos");
            label5.Text = getLocalizedString("vDir");
            label8.Text = getLocalizedString("vExt");
            label14.Text = getLocalizedString("dwID");
            createNpc.Text = getLocalizedString("Create NPC");
            removeNpc.Text = getLocalizedString("Remove NPC");
            copyNpc.Text = getLocalizedString("Clone NPC");
            createNpc1.Text = getLocalizedString("Create NPC");
            removeNpc1.Text = getLocalizedString("Remove NPC");
            copyNpc1.Text = getLocalizedString("Clone NPC");
            createResource.Text = getLocalizedString("Create resource");
            removeResource.Text = getLocalizedString("Remove resource");
            copyResource.Text = getLocalizedString("Clone resource");
            createResource1.Text = getLocalizedString("Create resource");
            removeResource1.Text = getLocalizedString("Remove resource");
            copyResource1.Text = getLocalizedString("Clone resource");
            createDynObj.Text = getLocalizedString("Create dynamic object");
            removeDynObj.Text = getLocalizedString("Remove dynamic object");
            copyDynObj.Text = getLocalizedString("Clone dynamic object");
            createTrigger.Text = getLocalizedString("Create trigger");
            removeTrigger.Text = getLocalizedString("Remove trigger");
            copyTrigger.Text = getLocalizedString("Clone trigger");
            label15.Text = getLocalizedString("dwNum");
            label16.Text = getLocalizedString("iRefresh");
            label17.Text = getLocalizedString("dwDiedTimes");
            label18.Text = getLocalizedString("dwAggressive");
            label19.Text = getLocalizedString("fOffsetWater");
            label20.Text = getLocalizedString("fOffsetTrn");
            label21.Text = getLocalizedString("dwFaction");
            label22.Text = getLocalizedString("dwFacHelper");
            label23.Text = getLocalizedString("dwFacAccept");
            label24.Text = getLocalizedString("iPathID");
            label25.Text = getLocalizedString("iLoopType");
            label26.Text = getLocalizedString("iSpeedFlag");
            label27.Text = getLocalizedString("iDeadTime");
            label28.Text = getLocalizedString("iRefreshLower");
            checkBox4.Text = getLocalizedString("bNeedHelp");
            checkBox7.Text = getLocalizedString("bDefFacAccept");
            checkBox5.Text = getLocalizedString("bDefFaction");
            checkBox6.Text = getLocalizedString("bDefFacHelper");
            label34.Text = getLocalizedString("dwGenID");
            label35.Text = getLocalizedString("idCtrl");
            label36.Text = getLocalizedString("iMaxNum");
            checkBox8.Text = getLocalizedString("bValidOnce");
            checkBox9.Text = getLocalizedString("bAutoRevive");
            checkBox10.Text = getLocalizedString("bInitGen");
            label29.Text = getLocalizedString("vPos");
            label33.Text = getLocalizedString("vExt");
            label39.Text = getLocalizedString("vDir");
            groupBox2.Text = getLocalizedString("Resources group");
            label54.Text = getLocalizedString("iResType");
            label53.Text = getLocalizedString("idTemplate");
            label52.Text = getLocalizedString("dwRefreshTime");
            label51.Text = getLocalizedString("dwNumber");
            label50.Text = getLocalizedString("fHeiOff");
            label40.Text = getLocalizedString("dwDynObjID");
            label49.Text = getLocalizedString("szName");
            label45.Text = getLocalizedString("idController");
            label46.Text = getLocalizedString("scale");
            label41.Text = getLocalizedString("vPos");
            label55.Text = getLocalizedString("vDir");
            groupBox3.Text = getLocalizedString("Preview");
            label63.Text = getLocalizedString("dwID");
            label38.Text = getLocalizedString("szName");
            label57.Text = getLocalizedString("iControllerID");
            label56.Text = getLocalizedString("iWaitTime");
            label48.Text = getLocalizedString("iStopTime");
            label58.Text = getLocalizedString("ActiveTime");
            label59.Text = getLocalizedString("StopTime");
            label60.Text = getLocalizedString("iActiveTimeRange");
            checkBox11.Text = getLocalizedString("bActivated");
            checkBox12.Text = getLocalizedString("bActiveTimeInvalid");
            checkBox13.Text = getLocalizedString("bStopTimeInvalid");
            comboBox3.Items.Clear();
            comboBox3.Items.Add(getLocalizedString("Everyday"));
            comboBox3.Items.Add(getLocalizedString("Monday"));
            comboBox3.Items.Add(getLocalizedString("Tuesday"));
            comboBox3.Items.Add(getLocalizedString("Wednesday"));
            comboBox3.Items.Add(getLocalizedString("Thursday"));
            comboBox3.Items.Add(getLocalizedString("Friday"));
            comboBox3.Items.Add(getLocalizedString("Saturday"));
            comboBox3.Items.Add(getLocalizedString("Sunday"));
            comboBox4.Items.Clear();
            comboBox4.Items.Add(getLocalizedString("Everyday"));
            comboBox4.Items.Add(getLocalizedString("Monday"));
            comboBox4.Items.Add(getLocalizedString("Tuesday"));
            comboBox4.Items.Add(getLocalizedString("Wednesday"));
            comboBox4.Items.Add(getLocalizedString("Thursday"));
            comboBox4.Items.Add(getLocalizedString("Friday"));
            comboBox4.Items.Add(getLocalizedString("Saturday"));
            comboBox4.Items.Add(getLocalizedString("Sunday"));
            tabPage5.Text = getLocalizedString("Search") + " [" + dataGridView4.Rows.Count + "]";
            button1.Text = getLocalizedString("Search");
            checkBox14.Text = getLocalizedString("Exact match");
            label68.Text = getLocalizedString("Search text");
            label71.Text = getLocalizedString("Search by");
            label69.Text = getLocalizedString("Search in");
            searchType.Items.Clear();
            searchType.Items.Add(getLocalizedString("ID"));
            searchType.Items.Add(getLocalizedString("szName"));
            searchType.SelectedItem = searchType.Items[0];
            searchCategory.Items.Clear();
            searchCategory.Items.Add(getLocalizedString("NPCs"));
            searchCategory.Items.Add(getLocalizedString("Resources"));
            searchCategory.Items.Add(getLocalizedString("Dynamic objects"));
            searchCategory.Items.Add(getLocalizedString("Triggers"));
            searchCategory.SelectedItem = searchCategory.Items[0];
            clientVersionToolStripMenuItem.Text = getLocalizedString("Client version");
            aboutToolStripMenuItem.Text = getLocalizedString("About");
            helpToolStripMenuItem.Text = getLocalizedString("Help");
            comboBox7.Items.Clear();
            comboBox7.Items.Add(getLocalizedString("Ground"));
            comboBox7.Items.Add(getLocalizedString("Free 3D"));
            comboBox7.SelectedItem = comboBox7.Items[0];
            clientProcessNameToolStripMenuItem.Text = getLocalizedString("Client process name");
            comboBox8.Items.Clear();
            comboBox8.Items.Add(getLocalizedString("LoopType0"));
            comboBox8.Items.Add(getLocalizedString("LoopType1"));
            comboBox8.Items.Add(getLocalizedString("LoopType2"));
            label72.Text = getLocalizedString("Search radius");
            openElementsdataToolStripMenuItem.Text = getLocalizedString("Open elements.data");
            for (int i = 0; i < npcList.Rows.Count; i++)
            {
                npcList.Rows[i].SetValues(getnpcList1Node1Name(i));
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1.Rows[i].SetValues(getResListNode1Name(i));
            }
            for (int i = 0; i < dataGridView2.Rows.Count; i++)
            {
                dataGridView2.Rows[i].SetValues(getDynObj1Name(i));
            }
            for (int i = 0; i < dataGridView3.Rows.Count; i++)
            {
                dataGridView3.Rows[i].SetValues(getTriggerName(i));
            }
            ControlHelper.updateTooltip(textBox51.Controls[0], getLocalizedString("Go to this trigger"));
            ControlHelper.updateTooltip(textBox37.Controls[0], getLocalizedString("Go to this trigger"));
            ControlHelper.updateTooltip(textBox11.Controls[0], getLocalizedString("Go to this trigger"));
            ControlHelper.updateTooltip(textBox49.Controls[0], getLocalizedString("Search this resource in PW database"));
            ControlHelper.updateTooltip(textBox16.Controls[0], getLocalizedString("Search this NPC in PW database"));
            checkBox16.Text = getLocalizedString("Case sensitive");
            dynamicobjectsdataEditorToolStripMenuItem.Text = getLocalizedString("dynamicobjects.data editor");
            databaseToolStripMenuItem.Text = getLocalizedString("Database");
            ControlHelper.updateTooltip(label2, getLocalizedString("iNPCTypeHelp"));
            ControlHelper.updateTooltip(label9, getLocalizedString("idCtrlHelp"));
            ControlHelper.updateTooltip(label45, getLocalizedString("idCtrlHelp"));
            ControlHelper.updateTooltip(label35, getLocalizedString("idCtrlHelp"));
            ControlHelper.updateTooltip(label11, getLocalizedString("iMaxNumHelp"));
            ControlHelper.updateTooltip(label36, getLocalizedString("iMaxNumHelp"));
            ControlHelper.updateTooltip(label12, getLocalizedString("iTypeHelp"));
            ControlHelper.updateTooltip(label8, getLocalizedString("vExtHelp"));
            ControlHelper.updateTooltip(label33, getLocalizedString("vExtHelp"));
            ControlHelper.updateTooltip(label15, getLocalizedString("dwNumHelp"));
            ControlHelper.updateTooltip(label51, getLocalizedString("dwNumHelp"));
            ControlHelper.updateTooltip(label24, getLocalizedString("iPathIDHelp"));
            ControlHelper.updateTooltip(label57, getLocalizedString("iControllerIDHelp"));
            ControlHelper.updateTooltip(label56, getLocalizedString("iWaitTimeHelp"));
            ControlHelper.updateTooltip(label48, getLocalizedString("iStopTimeHelp"));
            ControlHelper.updateTooltip(label58, getLocalizedString("StartTimeHelp"));
            ControlHelper.updateTooltip(label59, getLocalizedString("StopTimeHelp"));
            ControlHelper.updateTooltip(label60, getLocalizedString("iActiveTimeRangeHelp"));
            ControlHelper.updateTooltip(label16, getLocalizedString("iRefreshHelp"));
            ControlHelper.updateTooltip(label52, getLocalizedString("iRefreshHelp"));
            ControlHelper.updateTooltip(label21, getLocalizedString("dwFactionHelp"));
            ControlHelper.updateTooltip(label22, getLocalizedString("dwFactionHelperHelp"));
            ControlHelper.updateTooltip(label23, getLocalizedString("dwFactionAcceptHelp"));
            ControlHelper.updateTooltip(label1, getLocalizedString("iGroupTypeHelp"));
            ControlHelper.updateTooltip(label39, getLocalizedString("Value must be between $MIN$ and $MAX$").Replace("$MIN$", "0").Replace("$MAX$", "359"));
            ControlHelper.updateTooltip(label55, getLocalizedString("Value must be between $MIN$ and $MAX$").Replace("$MIN$", "0").Replace("$MAX$", "359"));
            groupBox4.Text = getLocalizedString("Objects where this trigger used");
            checkBox17.Text = getLocalizedString("Search everywhere");
            comboBox5.Items.Clear();
            comboBox5.Items.Add(getLocalizedString("iGroupType0"));
            comboBox5.Items.Add(getLocalizedString("iGroupType1"));
            comboBox5.Items.Add(getLocalizedString("iGroupType2"));
            exportToCoordsdatatxtToolStripMenuItem.Text = getLocalizedString("Export NPCs to coords_data.txt");
            importNpc.Text = getLocalizedString("Import NPC");
            exportNpc.Text = getLocalizedString("Export NPC");
            importNpc1.Text = getLocalizedString("Import NPC");
            exportNpc1.Text = getLocalizedString("Export NPC");
            importRes1.Text = getLocalizedString("Import resource");
            exportRes1.Text = getLocalizedString("Export resource");
            importRes.Text = getLocalizedString("Import resource");
            exportRes.Text = getLocalizedString("Export resource");
            importDynObj.Text = getLocalizedString("Import dynamic object");
            exportDynObj.Text = getLocalizedString("Export dynamic object");
            importTrigger.Text = getLocalizedString("Import trigger");
            exportTrigger.Text = getLocalizedString("Export trigger");
            updatenpcList1Data();
            updateResListData();
            updateDynObjListData();
            updateTriggerData();
        }

        void loadOffsets(string appPath)
        {
            if (File.Exists(appPath + "//config//offsets.txt"))
            {
                StreamReader sr = new StreamReader(appPath + "//config//offsets.txt", Encoding.Unicode);
                String line;
                String[] values;

                while ((line = sr.ReadLine()) != null)
                {
                    if (line.StartsWith("Name="))
                    {
                        clientVersionToolStripMenuItem.DropDownItems.Add(line.Substring(5), null, clientVersionToolStripMenuItem_Click);
                        Offset offset = new Offset();

                        // target base offset chain
                        values = sr.ReadLine().Split(' ');
                        offset.BaseChain = new int[values.Length];
                        for (int i = 0; i < values.Length; i++)
                        {
                            offset.BaseChain[i] = Int32.Parse(values[i], System.Globalization.NumberStyles.HexNumber);
                        }
                        offset.DirX = Int32.Parse(sr.ReadLine(), System.Globalization.NumberStyles.HexNumber);
                        offset.DirY = Int32.Parse(sr.ReadLine(), System.Globalization.NumberStyles.HexNumber);
                        offset.DirZ = Int32.Parse(sr.ReadLine(), System.Globalization.NumberStyles.HexNumber);
                        offset.PosX = Int32.Parse(sr.ReadLine(), System.Globalization.NumberStyles.HexNumber);
                        offset.PosY = Int32.Parse(sr.ReadLine(), System.Globalization.NumberStyles.HexNumber);
                        offset.PosZ = Int32.Parse(sr.ReadLine(), System.Globalization.NumberStyles.HexNumber);
                        offset.name = line;
                        offsets.Add(offset);
                    }
                }
                sr.Close();
            }
        }

        void loadSettings(string appPath)
        {
            if (File.Exists(appPath + "//config//settings.txt"))
            {
                using (FileStream fs = File.Open(appPath + "//config//settings.txt", FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (StreamReader sr = new StreamReader(bs))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] settings = line.Split('=');
                        Settings.Add(settings[0], settings[1]);
                    }
                }
            }
        }

        void loadDynObjsImages(string appPath)
        {
            if (Directory.Exists(appPath + "//config//dynobjs//"))
            {
                string[] images = Directory.GetFiles(appPath + "//config//dynobjs//", "*.jpg");
                for (int i = 0; i < images.Length; i++)
                {
                    dynobjImages.Add(uint.Parse(Path.GetFileNameWithoutExtension(images[i])), Image.FromFile(images[i]));
                }
            }
        }

        void loadDynObjs(string appPath)
        {
            if (File.Exists(appPath + "//config//dynobjs.txt"))
            {
                using (FileStream fs = File.Open(appPath + "//config//dynobjs.txt", FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (StreamReader sr = new StreamReader(bs))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] id_text = line.Split('\t');
                        dynobjs.Add(uint.Parse(id_text[0]), id_text[1]);
                        dynobjsInverse.Add(id_text[1], uint.Parse(id_text[0]));
                    }
                }
                foreach (KeyValuePair<uint, String> element in dynobjs)
                {
                    comboBox2.Items.Add(element.Value);
                }
            }
        }

        void loadLanguages(string appPath)
        {
            if (Directory.Exists(appPath + "//config//lang//"))
            {
                string[] langs1 = Directory.GetFiles(appPath + "//config//lang//");
                foreach (string lang in langs1)
                {
                    using (FileStream fs = File.Open(lang, FileMode.Open, FileAccess.Read))
                    using (BufferedStream bs = new BufferedStream(fs))
                    using (StreamReader sr = new StreamReader(bs))
                    {
                        string line = sr.ReadLine();
                        string[] lang_name = line.Split('=');
                        if (lang_name[0] != "LanguageName")
                        {
                            continue;
                        }
                        else
                        {
                            languageToolStripMenuItem.DropDownItems.Add(lang_name[1], null, languageToolStripMenuItem_Click);
                            Language lg = new Language();
                            langs.Add(lang_name[1], lg);
                            while ((line = sr.ReadLine()) != null)
                            {
                                string[] id_text = line.Split('=');
                                lg.values.Add(id_text[0], id_text[1]);
                            }
                        }
                    }
                }
            }
        }

        void loadNPCNames(string appPath)
        {
            if (File.Exists(appPath + "//config//npcs.txt"))
            {
                using (FileStream fs = File.Open(appPath + "//config//npcs.txt", FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (StreamReader sr = new StreamReader(bs))
                {
                    string line;
                    while (sr.Peek() > 0)
                    {
                        line = sr.ReadLine();
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            string[] id_text = line.Split('\t');
                            npcs.Add(uint.Parse(id_text[0]), id_text[1]);
                        }
                    }
                }
            }
        }

        public String getnpcList1Node1Name(int i)
        {
            String result = "";
            if (npcdata.m_aAreas[i].iNumGen > 0)
            {
                npcs.TryGetValue(npcdata.m_aAreas[i].m_aNPCGens[0].dwID, out result);
                if (result != null)
                {
                    result = "#" + (i + 1).ToString() + ", " + npcdata.m_aAreas[i].m_aNPCGens[0].dwID.ToString() + "(" + npcdata.m_aAreas[i].iNumGen.ToString() + ") - " + result;
                }
                else
                {
                    result = "#" + (i + 1).ToString() + ", " + npcdata.m_aAreas[i].m_aNPCGens[0].dwID.ToString() + "(" + npcdata.m_aAreas[i].iNumGen.ToString() + ") - " + (eLC == null ? getLocalizedString("[Names require loaded elements.data]") : "NOT FOUND");
                }
            }
            else
            {
                result = "#" + (i + 1).ToString() + " 0(0) - EMPTY";
            }
            return result;
        }

        String getnpcList1Node2NameSearch(int i, int j)
        {
            String result;
            npcs.TryGetValue(npcdata.m_aAreas[i].m_aNPCGens[j].dwID, out result);
            if (result == null) return "";
            return result;
        }

        String getnpcList1Node2Name(int i, int j)
        {
            String result = "";
            npcs.TryGetValue(npcdata.m_aAreas[i].m_aNPCGens[j].dwID, out result);
            if (result != null)
            {
                result = npcdata.m_aAreas[i].m_aNPCGens[j].dwID.ToString() + "(" + npcdata.m_aAreas[i].m_aNPCGens[j].dwNum + ") - " + result;
            }
            else
            {
                result = npcdata.m_aAreas[i].m_aNPCGens[j].dwID.ToString() + "(" + npcdata.m_aAreas[i].m_aNPCGens[j].dwNum + ") - " + (eLC == null ? getLocalizedString("[Names require loaded elements.data]") : "NOT FOUND");
            }
            return result;
        }

        String getResListNode2Name(int i, int j)
        {
            String result = "0";
            if (npcdata.m_aResAreas[i].iNumRes > 0)
            {
                mines.TryGetValue(npcdata.m_aResAreas[i].m_aRes[j].idTemplate, out result);
                if (result != null)
                {
                    result = npcdata.m_aResAreas[i].m_aRes[j].idTemplate.ToString() + " - " + result;
                }
                else
                {
                    result = npcdata.m_aResAreas[i].m_aRes[j].idTemplate.ToString() + " - " + (eLC == null ? getLocalizedString("[Names require loaded elements.data]") : "NOT FOUND");
                }
            }
            return result;
        }

        String getResListNode2NameSearch(int i, int j)
        {
            String result;
            mines.TryGetValue(npcdata.m_aResAreas[i].m_aRes[j].idTemplate, out result);
            if (result == null) return "";
            return result;
        }

        public String getResListNode1Name(int i)
        {
            String result = "#" + (i + 1).ToString() + " " + "0(0) - EMPTY";
            if (npcdata.m_aResAreas[i].iNumRes > 0)
            {
                mines.TryGetValue(npcdata.m_aResAreas[i].m_aRes[0].idTemplate, out result);
                if (result != null)
                {
                    result = "#" + (i + 1).ToString() + ", " + npcdata.m_aResAreas[i].m_aRes[0].idTemplate.ToString() + "(" + npcdata.m_aResAreas[i].iNumRes.ToString() + ") - " + result;
                }
                else
                {
                    result = "#" + (i + 1).ToString() + ", " + npcdata.m_aResAreas[i].m_aRes[0].idTemplate.ToString() + "(" + npcdata.m_aResAreas[i].iNumRes.ToString() + ") - " + (eLC == null ? getLocalizedString("[Names require loaded elements.data]") : "NOT FOUND");
                }
            }
            return result;
        }

        String getDynObj1Name(int i)
        {
            String result = "";
            dynobjs.TryGetValue(npcdata.m_aDynObjs[i].dwDynObjID, out result);
            if (result != null)
            {
                result = "#" + (i + 1).ToString() + ", " + npcdata.m_aDynObjs[i].dwDynObjID.ToString() + " - " + result;
            }
            else
            {
                result = "#" + (i + 1).ToString() + ", " + npcdata.m_aDynObjs[i].dwDynObjID.ToString() + " - NOT FOUND";
            }
            return result;
        }

        public String getTriggerName(int i)
        {
            return "#" + (i + 1).ToString() + ", " + npcdata.m_aControllers[i].id + " - " + getStringGBK(TrimByteArray(npcdata.m_aControllers[i].szName));
        }

        void SetDoubleBuffered(Control c, bool value)
        {
            PropertyInfo pi = typeof(Control).GetProperty("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic);
            if (pi != null)
            {
                pi.SetValue(c, value, null);
            }
        }

        void openNPCDataFile(String filename)
        {
            if (saveDialog)
            {
                DialogResult dr = MessageBox.Show(getLocalizedString("You have unsaved changes in this npcgen!") + "\n" + getLocalizedString("Do you want to save them?"), "Regular NPC Editor", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (dr)
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        saveNPCDataFile(filePath, npcdata.version);
                        saveDialog = false;
                        break;
                    case System.Windows.Forms.DialogResult.No:
                        this.Text = "Regular NPC Editor [" + filePath + "] v" + npcdata.version;
                        saveDialog = false;
                        break;
                }
            }
            // Stopwatch sw = new Stopwatch();
            //  sw.Start();
            try
            {
                setOpenSaveControlStatus(false);
                npcdataLoaded = false;
                filePath = filename;
                TaskbarProgress.SetState(this.Handle, TaskbarProgress.TaskbarStates.Normal);
                npcList.Rows.Clear();
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
                dataGridView3.Rows.Clear();
                using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(filename))
                using (MemoryMappedViewStream mms = mmf.CreateViewStream())
                using (BinaryReader br = new BinaryReader(mms))
                {
                    npcdata = new NPCDATA();
                    progressBar1.Value = 0;
                    npcdata.version = br.ReadInt32();
                    if (npcdata.version > 11 || npcdata.version < 0)
                    {
                        MessageBox.Show(this, getLocalizedString("This version of npcgen.data is not supported"), getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                        setOpenSaveControlStatus(true);
                        npcdataLoaded = false;
                        filePath = null;
                        return;
                    }
                    npcdata.Header = new NPCGENFILEHEADER();
                    npcdata.Header.iNumAIGen = br.ReadInt32();
                    npcdata.Header.iNumResArea = br.ReadInt32();
                    if (npcdata.version >= 6)
                    {
                        npcdata.Header.iNumDynObj = br.ReadInt32();
                    }
                    if (npcdata.version >= 7)
                    {
                        npcdata.Header.iNumNPCCtrl = br.ReadInt32();
                    }
                    progressBar1.Maximum = npcdata.Header.iNumAIGen * 2 + npcdata.Header.iNumResArea * 2 + npcdata.Header.iNumDynObj * 2 + npcdata.Header.iNumNPCCtrl * 2;
                    npcdata.m_aAreas = new List<AREA>();
                    npcList.Rows.Clear();
                    for (int i = 0; i < npcdata.Header.iNumAIGen; i++)
                    {
                        npcdata.m_aAreas.Add(new AREA());
                        npcdata.m_aAreas[i].iType = br.ReadInt32();
                        npcdata.m_aAreas[i].iNumGen = br.ReadInt32();
                        npcdata.m_aAreas[i].vPos = new float[3];
                        npcdata.m_aAreas[i].vPos[0] = br.ReadSingle();
                        npcdata.m_aAreas[i].vPos[1] = br.ReadSingle();
                        npcdata.m_aAreas[i].vPos[2] = br.ReadSingle();
                        npcdata.m_aAreas[i].vDir = new float[3];
                        npcdata.m_aAreas[i].vDir[0] = br.ReadSingle();
                        npcdata.m_aAreas[i].vDir[1] = br.ReadSingle();
                        npcdata.m_aAreas[i].vDir[2] = br.ReadSingle();
                        npcdata.m_aAreas[i].vExts = new float[3];
                        npcdata.m_aAreas[i].vExts[0] = br.ReadSingle();
                        npcdata.m_aAreas[i].vExts[1] = br.ReadSingle();
                        npcdata.m_aAreas[i].vExts[2] = br.ReadSingle();
                        npcdata.m_aAreas[i].iNPCType = br.ReadInt32();
                        npcdata.m_aAreas[i].iGroupType = br.ReadInt32();
                        npcdata.m_aAreas[i].bInitGen = br.ReadBoolean();
                        npcdata.m_aAreas[i].bAutoRevive = br.ReadBoolean();
                        npcdata.m_aAreas[i].bValidOnce = br.ReadBoolean();
                        npcdata.m_aAreas[i].dwGenID = br.ReadUInt32();
                        if (npcdata.version >= 7)
                        {
                            npcdata.m_aAreas[i].idCtrl = br.ReadUInt32();
                            npcdata.m_aAreas[i].iLifeTime = br.ReadInt32();
                            npcdata.m_aAreas[i].iMaxNum = br.ReadInt32();
                        }
                        npcdata.m_aAreas[i].m_aNPCGens = new List<NPCGENFILEAIGEN>();
                        for (int j = 0; j < npcdata.m_aAreas[i].iNumGen; j++)
                        {
                            npcdata.m_aAreas[i].m_aNPCGens.Add(new NPCGENFILEAIGEN());
                            npcdata.m_aAreas[i].m_aNPCGens[j].dwID = br.ReadUInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].dwNum = br.ReadUInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].iRefresh = br.ReadInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].dwDiedTimes = br.ReadUInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].dwAggressive = br.ReadUInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetWater = br.ReadSingle();
                            npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetTrn = br.ReadSingle();
                            npcdata.m_aAreas[i].m_aNPCGens[j].dwFaction = br.ReadUInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].dwFacHelper = br.ReadUInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].dwFacAccept = br.ReadUInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].bNeedHelp = br.ReadBoolean();
                            npcdata.m_aAreas[i].m_aNPCGens[j].bDefFaction = br.ReadBoolean();
                            npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacHelper = br.ReadBoolean();
                            npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacAccept = br.ReadBoolean();
                            npcdata.m_aAreas[i].m_aNPCGens[j].iPathID = br.ReadInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].iLoopType = br.ReadInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].iSpeedFlag = br.ReadInt32();
                            npcdata.m_aAreas[i].m_aNPCGens[j].iDeadTime = br.ReadInt32();
                            if (npcdata.version >= 11)
                            {
                                npcdata.m_aAreas[i].m_aNPCGens[j].iRefreshLower = br.ReadInt32();
                            }
                        }
                        progressBar1.Value += 2;
                        Application.DoEvents();
                        npcList.Rows.Add(getnpcList1Node1Name(i));
                        TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                    }
                    //npcList.RowCount = npcdata.Header.iNumAIGen;
                    //dataGridView1.RowCount = 0;
                    dataGridView1.Rows.Clear();
                    npcdata.m_aResAreas = new List<NPCGENFILERESAREA>();
                    for (int i = 0; i < npcdata.Header.iNumResArea; i++)
                    {
                        npcdata.m_aResAreas.Add(new NPCGENFILERESAREA());
                        npcdata.m_aResAreas[i].vPos = new float[3];
                        npcdata.m_aResAreas[i].vPos[0] = br.ReadSingle();
                        npcdata.m_aResAreas[i].vPos[1] = br.ReadSingle();
                        npcdata.m_aResAreas[i].vPos[2] = br.ReadSingle();
                        npcdata.m_aResAreas[i].fExtX = br.ReadSingle();
                        npcdata.m_aResAreas[i].fExtZ = br.ReadSingle();
                        npcdata.m_aResAreas[i].iNumRes = br.ReadInt32();
                        npcdata.m_aResAreas[i].bInitGen = br.ReadBoolean();
                        npcdata.m_aResAreas[i].bAutoRevive = br.ReadBoolean();
                        npcdata.m_aResAreas[i].bValidOnce = br.ReadBoolean();
                        npcdata.m_aResAreas[i].dwGenID = br.ReadUInt32();
                        if (npcdata.version >= 6)
                        {
                            npcdata.m_aResAreas[i].dir = new byte[2];
                            npcdata.m_aResAreas[i].dir[0] = br.ReadByte();
                            npcdata.m_aResAreas[i].dir[1] = br.ReadByte();
                            npcdata.m_aResAreas[i].rad = br.ReadByte();
                        }
                        if (npcdata.version >= 7)
                        {
                            npcdata.m_aResAreas[i].idCtrl = br.ReadUInt32();
                            npcdata.m_aResAreas[i].iMaxNum = br.ReadInt32();
                        }
                        npcdata.m_aResAreas[i].m_aRes = new List<NPCGENFILERES>();
                        for (int j = 0; j < npcdata.m_aResAreas[i].iNumRes; j++)
                        {
                            npcdata.m_aResAreas[i].m_aRes.Add(new NPCGENFILERES());
                            npcdata.m_aResAreas[i].m_aRes[j].iResType = br.ReadInt32();
                            npcdata.m_aResAreas[i].m_aRes[j].idTemplate = br.ReadInt32();
                            npcdata.m_aResAreas[i].m_aRes[j].dwRefreshTime = br.ReadUInt32();
                            npcdata.m_aResAreas[i].m_aRes[j].dwNumber = br.ReadUInt32();
                            npcdata.m_aResAreas[i].m_aRes[j].fHeiOff = br.ReadSingle();
                        }
                        progressBar1.Value += 2;
                        Application.DoEvents();
                        dataGridView1.Rows.Add(getResListNode1Name(i));
                        TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                    }
                    //dataGridView1.RowCount = npcdata.Header.iNumResArea;
                    //dataGridView2.RowCount = 0;
                    dataGridView2.Rows.Clear();
                    if (npcdata.version >= 6)
                    {
                        npcdata.m_aDynObjs = new List<NPCGENFILEDYNOBJ>();
                        for (int i = 0; i < npcdata.Header.iNumDynObj; i++)
                        {
                            npcdata.m_aDynObjs.Add(new NPCGENFILEDYNOBJ());
                            npcdata.m_aDynObjs[i].dwDynObjID = br.ReadUInt32();
                            npcdata.m_aDynObjs[i].vPos = new float[3];
                            npcdata.m_aDynObjs[i].vPos[0] = br.ReadSingle();
                            npcdata.m_aDynObjs[i].vPos[1] = br.ReadSingle();
                            npcdata.m_aDynObjs[i].vPos[2] = br.ReadSingle();
                            npcdata.m_aDynObjs[i].dir = new byte[2];
                            npcdata.m_aDynObjs[i].dir[0] = br.ReadByte();
                            npcdata.m_aDynObjs[i].dir[1] = br.ReadByte();
                            npcdata.m_aDynObjs[i].rad = br.ReadByte();
                            if (npcdata.version >= 9)
                            {
                                npcdata.m_aDynObjs[i].idController = br.ReadUInt32();
                            }
                            if (npcdata.version >= 10)
                            {
                                npcdata.m_aDynObjs[i].scale = br.ReadByte();
                            }
                            Application.DoEvents();
                            dataGridView2.Rows.Add(getDynObj1Name(i));
                            progressBar1.Value += 2;
                            TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                        }
                    }
                    // dataGridView2.RowCount = npcdata.Header.iNumDynObj;
                    // dataGridView3.RowCount = 0;
                    dataGridView3.Rows.Clear();
                    if (npcdata.version >= 7)
                    {
                        npcdata.m_aControllers = new List<NPCGENFILECTRL>();
                        for (int i = 0; i < npcdata.Header.iNumNPCCtrl; i++)
                        {
                            npcdata.m_aControllers.Add(new NPCGENFILECTRL());
                            npcdata.m_aControllers[i].id = br.ReadUInt32();
                            npcdata.m_aControllers[i].iControllerID = br.ReadInt32();
                            npcdata.m_aControllers[i].szName = new byte[128];
                            npcdata.m_aControllers[i].szName = br.ReadBytes(128);
                            npcdata.m_aControllers[i].bActived = br.ReadBoolean();
                            npcdata.m_aControllers[i].iWaitTime = br.ReadInt32();
                            npcdata.m_aControllers[i].iStopTime = br.ReadInt32();
                            npcdata.m_aControllers[i].bActiveTimeInvalid = br.ReadBoolean();
                            npcdata.m_aControllers[i].bStopTimeInvalid = br.ReadBoolean();
                            npcdata.m_aControllers[i].ActiveTime = new NPCCTRLTIME();
                            npcdata.m_aControllers[i].ActiveTime.iYear = br.ReadInt32();
                            npcdata.m_aControllers[i].ActiveTime.iMouth = br.ReadInt32();
                            npcdata.m_aControllers[i].ActiveTime.iWeek = br.ReadInt32();
                            npcdata.m_aControllers[i].ActiveTime.iDay = br.ReadInt32();
                            npcdata.m_aControllers[i].ActiveTime.iHours = br.ReadInt32();
                            npcdata.m_aControllers[i].ActiveTime.iMinutes = br.ReadInt32();
                            npcdata.m_aControllers[i].StopTime = new NPCCTRLTIME();
                            npcdata.m_aControllers[i].StopTime.iYear = br.ReadInt32();
                            npcdata.m_aControllers[i].StopTime.iMouth = br.ReadInt32();
                            npcdata.m_aControllers[i].StopTime.iWeek = br.ReadInt32();
                            npcdata.m_aControllers[i].StopTime.iDay = br.ReadInt32();
                            npcdata.m_aControllers[i].StopTime.iHours = br.ReadInt32();
                            npcdata.m_aControllers[i].StopTime.iMinutes = br.ReadInt32();
                            if (npcdata.version >= 8)
                            {
                                npcdata.m_aControllers[i].iActiveTimeRange = br.ReadUInt32();
                            }
                            progressBar1.Value += 2;
                            Application.DoEvents();
                            dataGridView3.Rows.Add(getTriggerName(i));
                            TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                        }
                        //dataGridView3.RowCount = npcdata.Header.iNumNPCCtrl;
                    }
                }
                setOpenSaveControlStatus(true);
                this.Text = "Regular NPC Editor [" + filePath + "] v" + npcdata.version;
                saveNPCDataMenuItem.Text = getLocalizedString("Save") + " (v" + npcdata.version + ")";
                saveAsNPCDataMenuItem.Text = getLocalizedString("Save As") + " (v" + npcdata.version + ")";
                npcdataLoaded = true;
                if (npcdata.version >= 11)
                {
                    textBox30.Enabled = true;
                }
                else
                {
                    textBox30.Enabled = false;
                }
                if (npcdata.version >= 7)
                {
                    textBox11.Enabled = true;
                    textBox12.Enabled = true;
                    textBox13.Enabled = true;
                    textBox37.Enabled = true;
                    textBox38.Enabled = true;
                    if (!tabControl1.TabPages.Contains(tabPage4))
                    {
                        tabControl1.TabPages.Insert(3, tabPage4);
                        tabPage4.Show();
                    }
                }
                else
                {
                    textBox11.Enabled = false;
                    textBox12.Enabled = false;
                    textBox13.Enabled = false;
                    textBox37.Enabled = false;
                    textBox38.Enabled = false;
                    tabPage4.Hide();
                    tabControl1.TabPages.Remove(tabPage4);
                }
                if (npcdata.version >= 6)
                {
                    textBox39.Enabled = true;
                    textBox40.Enabled = true;
                    textBox41.Enabled = true;
                    if (!tabControl1.TabPages.Contains(tabPage3))
                    {
                        tabControl1.TabPages.Insert(2, tabPage3);
                        tabPage3.Show();
                    }
                }
                else
                {
                    textBox39.Enabled = false;
                    textBox40.Enabled = false;
                    textBox41.Enabled = false;
                    tabPage3.Hide();
                    tabControl1.TabPages.Remove(tabPage3);
                }
                if (npcdata.version >= 9)
                {
                    textBox51.Enabled = true;
                }
                else
                {
                    textBox51.Enabled = false;
                }
                if (npcdata.version >= 10)
                {
                    textBox52.Enabled = true;
                }
                else
                {
                    textBox52.Enabled = false;
                }
                if (npcdata.version >= 8)
                {
                    textBox60.Enabled = true;
                }
                else
                {
                    textBox60.Enabled = false;
                }
                TaskbarProgress.SetState(this.Handle, TaskbarProgress.TaskbarStates.Indeterminate);
                if (npcdata.Header.iNumAIGen > 0)
                {
                    selectRow(npcList, 0);
                }
                if (npcdata.Header.iNumResArea > 0)
                {
                    selectRow(dataGridView1, 0);
                }
                if (npcdata.Header.iNumDynObj > 0)
                {
                    selectRow(dataGridView2, 0);
                }
                if (npcdata.Header.iNumNPCCtrl > 0)
                {
                    selectRow(dataGridView3, 0);
                }
            }
            catch (Exception exc)
            {
                setOpenSaveControlStatus(true);
                npcdataLoaded = false;
                filePath = null;
                MessageBox.Show(getLocalizedString("File is already opened by another program.Close that program and try again") + "\n" + exc.Message, "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // sw.Stop();
            //  MessageBox.Show((sw.ElapsedMilliseconds / 1000f).ToString());
        }

        private void openNPCDataMenuItem_Click(object sender, EventArgs e)
        {
            using (var openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "npcgen (*.data) | *.data";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    openNPCDataFile(openFileDialog.FileName);
                }
                tabPage1.Text = getLocalizedString("NPCs") + " [" + npcList.Rows.Count + "]";
                tabPage2.Text = getLocalizedString("Resources") + " [" + dataGridView1.Rows.Count + "]";
                tabPage3.Text = getLocalizedString("Dynamic objects") + " [" + dataGridView2.Rows.Count + "]";
                tabPage4.Text = getLocalizedString("Triggers") + " [" + dataGridView3.Rows.Count + "]";
            }
        }
        void setOpenSaveControlStatus(bool status)
        {
            openNPCDataMenuItem.Enabled = status;
            saveNPCDataMenuItem.Enabled = status;
            saveAsNPCDataMenuItem.Enabled = status;
            converterToolStripMenuItem.Enabled = status;
            openElementsdataToolStripMenuItem.Enabled = status;
        }

        void saveNPCDataFile(string filePath, int version)
        {
            try
            {
                setOpenSaveControlStatus(false);
                TaskbarProgress.SetState(this.Handle, TaskbarProgress.TaskbarStates.Normal);
                using (BinaryWriter bw = new BinaryWriter(File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)))
                {
                    progressBar1.Value = 0;
                    bw.Write(version);
                    bw.Write(npcdata.Header.iNumAIGen);
                    bw.Write(npcdata.Header.iNumResArea);
                    if (version >= 6)
                    {
                        bw.Write(npcdata.Header.iNumDynObj);
                    }
                    if (version >= 7)
                    {
                        bw.Write(npcdata.Header.iNumNPCCtrl);
                    }
                    progressBar1.Maximum = npcdata.Header.iNumAIGen + npcdata.Header.iNumResArea + npcdata.Header.iNumDynObj + npcdata.Header.iNumNPCCtrl;
                    for (int i = 0; i < npcdata.Header.iNumAIGen; i++)
                    {
                        bw.Write(npcdata.m_aAreas[i].iType);
                        bw.Write(npcdata.m_aAreas[i].iNumGen);
                        bw.Write(npcdata.m_aAreas[i].vPos[0]);
                        bw.Write(npcdata.m_aAreas[i].vPos[1]);
                        bw.Write(npcdata.m_aAreas[i].vPos[2]);
                        bw.Write(npcdata.m_aAreas[i].vDir[0]);
                        bw.Write(npcdata.m_aAreas[i].vDir[1]);
                        bw.Write(npcdata.m_aAreas[i].vDir[2]);
                        bw.Write(npcdata.m_aAreas[i].vExts[0]);
                        bw.Write(npcdata.m_aAreas[i].vExts[1]);
                        bw.Write(npcdata.m_aAreas[i].vExts[2]);
                        bw.Write(npcdata.m_aAreas[i].iNPCType);
                        bw.Write(npcdata.m_aAreas[i].iGroupType);
                        bw.Write(npcdata.m_aAreas[i].bInitGen);
                        bw.Write(npcdata.m_aAreas[i].bAutoRevive);
                        bw.Write(npcdata.m_aAreas[i].bValidOnce);
                        bw.Write(npcdata.m_aAreas[i].dwGenID);
                        if (version >= 7)
                        {
                            bw.Write(npcdata.m_aAreas[i].idCtrl);
                            bw.Write(npcdata.m_aAreas[i].iLifeTime);
                            bw.Write(npcdata.m_aAreas[i].iMaxNum);
                        }
                        for (int j = 0; j < npcdata.m_aAreas[i].iNumGen; j++)
                        {
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].dwID);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].dwNum);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].iRefresh);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].dwDiedTimes);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].dwAggressive);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetWater);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetTrn);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].dwFaction);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].dwFacHelper);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].dwFacAccept);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].bNeedHelp);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].bDefFaction);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacHelper);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacAccept);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].iPathID);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].iLoopType);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].iSpeedFlag);
                            bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].iDeadTime);
                            if (version >= 11)
                            {
                                bw.Write(npcdata.m_aAreas[i].m_aNPCGens[j].iRefreshLower);
                            }
                        }
                        progressBar1.Value++;
                        TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                    }
                    for (int i = 0; i < npcdata.Header.iNumResArea; i++)
                    {
                        bw.Write(npcdata.m_aResAreas[i].vPos[0]);
                        bw.Write(npcdata.m_aResAreas[i].vPos[1]);
                        bw.Write(npcdata.m_aResAreas[i].vPos[2]);
                        bw.Write(npcdata.m_aResAreas[i].fExtX);
                        bw.Write(npcdata.m_aResAreas[i].fExtZ);
                        bw.Write(npcdata.m_aResAreas[i].iNumRes);
                        bw.Write(npcdata.m_aResAreas[i].bInitGen);
                        bw.Write(npcdata.m_aResAreas[i].bAutoRevive);
                        bw.Write(npcdata.m_aResAreas[i].bValidOnce);
                        bw.Write(npcdata.m_aResAreas[i].dwGenID);
                        if (version >= 6)
                        {
                            bw.Write(npcdata.m_aResAreas[i].dir[0]);
                            bw.Write(npcdata.m_aResAreas[i].dir[1]);
                            bw.Write(npcdata.m_aResAreas[i].rad);
                        }
                        if (version >= 7)
                        {
                            bw.Write(npcdata.m_aResAreas[i].idCtrl);
                            bw.Write(npcdata.m_aResAreas[i].iMaxNum);
                        }
                        for (int j = 0; j < npcdata.m_aResAreas[i].iNumRes; j++)
                        {
                            bw.Write(npcdata.m_aResAreas[i].m_aRes[j].iResType);
                            bw.Write(npcdata.m_aResAreas[i].m_aRes[j].idTemplate);
                            bw.Write(npcdata.m_aResAreas[i].m_aRes[j].dwRefreshTime);
                            bw.Write(npcdata.m_aResAreas[i].m_aRes[j].dwNumber);
                            bw.Write(npcdata.m_aResAreas[i].m_aRes[j].fHeiOff);
                        }
                        progressBar1.Value++;
                        TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                    }
                    if (version >= 6)
                    {
                        for (int i = 0; i < npcdata.Header.iNumDynObj; i++)
                        {
                            bw.Write(npcdata.m_aDynObjs[i].dwDynObjID);
                            bw.Write(npcdata.m_aDynObjs[i].vPos[0]);
                            bw.Write(npcdata.m_aDynObjs[i].vPos[1]);
                            bw.Write(npcdata.m_aDynObjs[i].vPos[2]);
                            bw.Write(npcdata.m_aDynObjs[i].dir[0]);
                            bw.Write(npcdata.m_aDynObjs[i].dir[1]);
                            bw.Write(npcdata.m_aDynObjs[i].rad);
                            if (version >= 9)
                            {
                                bw.Write(npcdata.m_aDynObjs[i].idController);
                            }
                            if (version >= 10)
                            {
                                bw.Write(npcdata.m_aDynObjs[i].scale);
                            }
                            progressBar1.Value++;
                            TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                        }
                    }
                    if (version >= 7)
                    {
                        for (int i = 0; i < npcdata.Header.iNumNPCCtrl; i++)
                        {
                            bw.Write(npcdata.m_aControllers[i].id);
                            bw.Write(npcdata.m_aControllers[i].iControllerID);
                            bw.Write(npcdata.m_aControllers[i].szName);
                            bw.Write(npcdata.m_aControllers[i].bActived);
                            bw.Write(npcdata.m_aControllers[i].iWaitTime);
                            bw.Write(npcdata.m_aControllers[i].iStopTime);
                            bw.Write(npcdata.m_aControllers[i].bActiveTimeInvalid);
                            bw.Write(npcdata.m_aControllers[i].bStopTimeInvalid);
                            bw.Write(npcdata.m_aControllers[i].ActiveTime.iYear);
                            bw.Write(npcdata.m_aControllers[i].ActiveTime.iMouth);
                            bw.Write(npcdata.m_aControllers[i].ActiveTime.iWeek);
                            bw.Write(npcdata.m_aControllers[i].ActiveTime.iDay);
                            bw.Write(npcdata.m_aControllers[i].ActiveTime.iHours);
                            bw.Write(npcdata.m_aControllers[i].ActiveTime.iMinutes);
                            bw.Write(npcdata.m_aControllers[i].StopTime.iYear);
                            bw.Write(npcdata.m_aControllers[i].StopTime.iMouth);
                            bw.Write(npcdata.m_aControllers[i].StopTime.iWeek);
                            bw.Write(npcdata.m_aControllers[i].StopTime.iDay);
                            bw.Write(npcdata.m_aControllers[i].StopTime.iHours);
                            bw.Write(npcdata.m_aControllers[i].StopTime.iMinutes);
                            if (version >= 8)
                            {
                                bw.Write(npcdata.m_aControllers[i].iActiveTimeRange);
                            }
                            progressBar1.Value++;
                            TaskbarProgress.SetValue(this.Handle, progressBar1.Value, progressBar1.Maximum);
                        }
                    }
                }
                TaskbarProgress.SetState(this.Handle, TaskbarProgress.TaskbarStates.Indeterminate);
                setOpenSaveControlStatus(true);
                saveDialog = false;
                this.Text = "Regular NPC Editor [" + filePath + "] v" + npcdata.version;
            }
            catch (Exception)
            {
                TaskbarProgress.SetState(this.Handle, TaskbarProgress.TaskbarStates.Indeterminate);
                setOpenSaveControlStatus(true);
                saveDialog = true;
                MessageBox.Show(getLocalizedString("File is already opened by another program.Close that program and try again") + "\n" + getLocalizedString("Tip: You can also use \"Save as\""), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void saveNPCDataMenuItem_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded && filePath != null)
            {
                saveNPCDataFile(filePath, npcdata.version);
            }
        }

        private void saveAsNPCDataMenuItem_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                SaveFileDialog sd = new SaveFileDialog();
                sd.Filter = "npcgen (*.data) | *.data";
                if (sd.ShowDialog() == DialogResult.OK)
                {
                    saveNPCDataFile(sd.FileName, npcdata.version);
                }
                sd.Dispose();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[i].Index].iNPCType = comboBox1.SelectedIndex;
                }
            }
        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].idCtrl = value;
                    }
                    updateTriggerData();
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].iLifeTime = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].iMaxNum = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.CurrentCell != null)
            {
                TextBox tb = (TextBox)sender;
                int value;
                try
                {
                    value = Int32.Parse(tb.Text);
                    tb.BackColor = Color.White;
                }
                catch (Exception)
                {
                    tb.BackColor = Color.Red;
                    return;
                }
                npcdata.m_aAreas[npcList.CurrentCell.RowIndex].iType = value;
            }
        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].dwGenID = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vPos[0] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vPos[1] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vPos[2] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vDir[0] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vDir[1] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vDir[2] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }

            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vExts[0] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vExts[1] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[i].Index].vExts[2] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[i].Index].bValidOnce = checkBox3.Checked;
                }
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[i].Index].bAutoRevive = checkBox2.Checked;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[i].Index].bInitGen = checkBox1.Checked;
                }
            }
        }

        private void textBox16_LostFocus(object sender, EventArgs e)
        {
            int i = npcList.CurrentCell.RowIndex;
            int j = npcList1.CurrentCell.RowIndex;
            npcList.Rows[i].SetValues(getnpcList1Node1Name(i));
            npcList1.Rows[j].SetValues(getnpcList1Node2Name(i, j));
        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwID = value;
                        npcList1.Rows[npcList1.SelectedRows[i].Index].SetValues(getnpcList1Node2Name(npcList.SelectedRows[0].Index, npcList1.SelectedRows[i].Index));
                    }
                    npcList.Rows[npcList.SelectedRows[0].Index].SetValues(getnpcList1Node1Name(npcList.SelectedRows[0].Index));
                    updateTriggerData();
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwNum = value;
                        npcList1.Rows[npcList1.SelectedRows[i].Index].SetValues(getnpcList1Node2Name(npcList.SelectedRows[0].Index, npcList1.SelectedRows[i].Index));
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox18_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox19_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwDiedTimes = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox20_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwAggressive = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox23_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwFaction = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox24_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwFacHelper = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox25_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwFacAccept = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox26_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iPathID = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox27_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.CurrentCell != null)
            {
                TextBox tb = (TextBox)sender;
                int value;
                try
                {
                    value = Int32.Parse(tb.Text);
                    tb.BackColor = Color.White;
                }
                catch (Exception)
                {
                    tb.BackColor = Color.Red;
                    return;
                }
                npcdata.m_aAreas[npcList.CurrentCell.RowIndex].m_aNPCGens[npcList1.CurrentCell.RowIndex].iLoopType = value;
            }
        }

        private void textBox28_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iSpeedFlag = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox29_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iDeadTime = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox30_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iRefreshLower = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bNeedHelp = checkBox4.Checked;
                }
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bDefFaction = checkBox5.Checked;
                }
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bDefFacHelper = checkBox6.Checked;
                }
            }
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bDefFacAccept = checkBox7.Checked;
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (npcdataLoaded)
            {
                createNpc.Enabled = true;
                importNpc.Enabled = true;
                if (npcList.CurrentCell != null)
                {
                    copyNpc.Enabled = true;
                    removeNpc.Enabled = true;
                    exportNpc.Enabled = true;
                }
                else
                {
                    removeNpc.Enabled = false;
                    copyNpc.Enabled = false;
                    exportNpc.Enabled = false;
                }
            }
            else
            {
                createNpc.Enabled = false;
                removeNpc.Enabled = false;
                copyNpc.Enabled = false;
                importNpc.Enabled = false;
                exportNpc.Enabled = false;
            }
        }

        private void createNpc_Click(object sender, EventArgs e)
        {
            int i = npcdata.Header.iNumAIGen;
            npcdata.m_aAreas.Add(new AREA());
            npcdata.m_aAreas[i].iType = 0;
            npcdata.m_aAreas[i].iNumGen = 0;
            npcdata.m_aAreas[i].vPos = new float[3];
            npcdata.m_aAreas[i].vPos[0] = 0;
            npcdata.m_aAreas[i].vPos[1] = 0;
            npcdata.m_aAreas[i].vPos[2] = 0;
            npcdata.m_aAreas[i].vDir = new float[3];
            npcdata.m_aAreas[i].vDir[0] = 0;
            npcdata.m_aAreas[i].vDir[1] = 0;
            npcdata.m_aAreas[i].vDir[2] = 0;
            npcdata.m_aAreas[i].vExts = new float[3];
            npcdata.m_aAreas[i].vExts[0] = 0;
            npcdata.m_aAreas[i].vExts[1] = 0;
            npcdata.m_aAreas[i].vExts[2] = 0;
            npcdata.m_aAreas[i].iNPCType = 0;
            npcdata.m_aAreas[i].iGroupType = 0;
            npcdata.m_aAreas[i].bInitGen = false;
            npcdata.m_aAreas[i].bAutoRevive = false;
            npcdata.m_aAreas[i].bValidOnce = false;
            npcdata.m_aAreas[i].dwGenID = 0;
            if (npcdata.version >= 7)
            {
                npcdata.m_aAreas[i].idCtrl = 0;
                npcdata.m_aAreas[i].iLifeTime = 0;
                npcdata.m_aAreas[i].iMaxNum = 0;
            }
            npcdata.m_aAreas[i].m_aNPCGens = new List<NPCGENFILEAIGEN>();
            npcList.Rows.Add(getnpcList1Node1Name(npcdata.Header.iNumAIGen));
            selectRow(npcList, npcdata.Header.iNumAIGen);
            npcdata.Header.iNumAIGen++;
            updatenpcList1Data();
            disablenpcList1Controls();
            tabPage1.Text = getLocalizedString("NPCs") + " [" + npcList.Rows.Count + "]";
            button1_Click(null, null);
        }

        private void removeNpc_Click(object sender, EventArgs e)
        {
            npcList.Enabled = false;
            foreach (DataGridViewRow row in npcList.SelectedRows)
            {
                npcdata.m_aAreas.RemoveAt(row.Index);
                npcdata.Header.iNumAIGen--;
                npcList.Rows.RemoveAt(row.Index);
                Application.DoEvents();
            }
            for (int j = 0; j < npcList.Rows.Count; j++)
            {
                npcList.Rows[j].SetValues(getnpcList1Node1Name(j));
            }
            npcList.Enabled = true;
            tabPage1.Text = getLocalizedString("NPCs") + " [" + npcList.Rows.Count + "]";
            updatenpcList1Data();
            button1_Click(null, null);
            updateTriggerData();
        }

        private void copyNpc_Click(object sender, EventArgs e)
        {
            int start = npcdata.Header.iNumAIGen;
            int i;
            for (int k = npcList.SelectedRows.Count - 1; k >= 0; k--)
            {
                i = npcList.SelectedRows[k].Index;
                npcdata.m_aAreas.Add(new AREA());
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].iType = npcdata.m_aAreas[i].iType;
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].iNumGen = npcdata.m_aAreas[i].iNumGen;
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vPos = new float[3];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vPos[0] = npcdata.m_aAreas[i].vPos[0];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vPos[1] = npcdata.m_aAreas[i].vPos[1];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vPos[2] = npcdata.m_aAreas[i].vPos[2];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vDir = new float[3];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vDir[0] = npcdata.m_aAreas[i].vDir[0];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vDir[1] = npcdata.m_aAreas[i].vDir[1];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vDir[2] = npcdata.m_aAreas[i].vDir[2];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vExts = new float[3];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vExts[0] = npcdata.m_aAreas[i].vExts[0];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vExts[1] = npcdata.m_aAreas[i].vExts[1];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].vExts[2] = npcdata.m_aAreas[i].vExts[2];
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].iNPCType = npcdata.m_aAreas[i].iNPCType;
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].iGroupType = npcdata.m_aAreas[i].iGroupType;
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].bInitGen = npcdata.m_aAreas[i].bInitGen;
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].bAutoRevive = npcdata.m_aAreas[i].bAutoRevive;
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].bValidOnce = npcdata.m_aAreas[i].bValidOnce;
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].dwGenID = npcdata.m_aAreas[i].dwGenID;
                if (npcdata.version >= 7)
                {
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].idCtrl = npcdata.m_aAreas[i].idCtrl;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].iLifeTime = npcdata.m_aAreas[i].iLifeTime;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].iMaxNum = npcdata.m_aAreas[i].iMaxNum;
                }
                npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens = new List<NPCGENFILEAIGEN>();
                for (int j = 0; j < npcdata.m_aAreas[i].iNumGen; j++)
                {
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens.Add(new NPCGENFILEAIGEN());
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].dwID = npcdata.m_aAreas[i].m_aNPCGens[j].dwID;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].dwNum = npcdata.m_aAreas[i].m_aNPCGens[j].dwNum;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].iRefresh = npcdata.m_aAreas[i].m_aNPCGens[j].iRefresh;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].dwDiedTimes = npcdata.m_aAreas[i].m_aNPCGens[j].dwDiedTimes;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].dwAggressive = npcdata.m_aAreas[i].m_aNPCGens[j].dwAggressive;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].fOffsetWater = npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetWater;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].fOffsetTrn = npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetTrn;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].dwFaction = npcdata.m_aAreas[i].m_aNPCGens[j].dwFaction;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].dwFacHelper = npcdata.m_aAreas[i].m_aNPCGens[j].dwFacHelper;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].dwFacAccept = npcdata.m_aAreas[i].m_aNPCGens[j].dwFacAccept;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].bNeedHelp = npcdata.m_aAreas[i].m_aNPCGens[j].bNeedHelp;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].bDefFaction = npcdata.m_aAreas[i].m_aNPCGens[j].bDefFaction;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].bDefFacHelper = npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacHelper;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].bDefFacAccept = npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacAccept;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].iPathID = npcdata.m_aAreas[i].m_aNPCGens[j].iPathID;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].iLoopType = npcdata.m_aAreas[i].m_aNPCGens[j].iLoopType;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].iSpeedFlag = npcdata.m_aAreas[i].m_aNPCGens[j].iSpeedFlag;
                    npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].iDeadTime = npcdata.m_aAreas[i].m_aNPCGens[j].iDeadTime;
                    if (npcdata.version >= 11)
                    {
                        npcdata.m_aAreas[npcdata.Header.iNumAIGen].m_aNPCGens[j].iRefreshLower = npcdata.m_aAreas[i].m_aNPCGens[j].iRefreshLower;
                    }
                }
                npcList.Rows.Add(getnpcList1Node1Name(npcdata.Header.iNumAIGen));
                npcdata.Header.iNumAIGen++;
            }
            selectRows(npcList, start, npcdata.Header.iNumAIGen - 1);
            tabPage1.Text = getLocalizedString("NPCs") + " [" + npcList.Rows.Count + "]";
            button1_Click(null, null);
        }

        void enablenpcList1Controls()
        {
            textBox16.Enabled = true;
            textBox17.Enabled = true;
            textBox18.Enabled = true;
            textBox19.Enabled = true;
            textBox20.Enabled = true;
            textBox21.Enabled = true;
            textBox22.Enabled = true;
            textBox23.Enabled = true;
            textBox24.Enabled = true;
            textBox25.Enabled = true;
            textBox26.Enabled = true;
            comboBox8.Enabled = true;
            textBox28.Enabled = true;
            textBox29.Enabled = true;
            if (npcdata.version >= 11)
            {
                textBox30.Enabled = true;
            }
            checkBox4.Enabled = true;
            checkBox5.Enabled = true;
            checkBox6.Enabled = true;
            checkBox7.Enabled = true;
        }

        void disablenpcList1Controls()
        {
            textBox16.Enabled = false;
            textBox17.Enabled = false;
            textBox18.Enabled = false;
            textBox19.Enabled = false;
            textBox20.Enabled = false;
            textBox21.Enabled = false;
            textBox22.Enabled = false;
            textBox23.Enabled = false;
            textBox24.Enabled = false;
            textBox25.Enabled = false;
            textBox26.Enabled = false;
            comboBox8.Enabled = false;
            textBox28.Enabled = false;
            textBox29.Enabled = false;
            updateVars = true;
            textBox16.Text = String.Empty;
            textBox17.Text = String.Empty;
            textBox18.Text = String.Empty;
            textBox19.Text = String.Empty;
            textBox20.Text = String.Empty;
            textBox21.Text = String.Empty;
            textBox22.Text = String.Empty;
            textBox23.Text = String.Empty;
            textBox24.Text = String.Empty;
            textBox25.Text = String.Empty;
            textBox26.Text = String.Empty;
            textBox28.Text = String.Empty;
            textBox29.Text = String.Empty;
            if (npcdata.version >= 11)
            {
                textBox30.Enabled = false;
                textBox30.Text = String.Empty;
            }
            updateVars = false;
            checkBox4.Enabled = false;
            checkBox5.Enabled = false;
            checkBox6.Enabled = false;
            checkBox7.Enabled = false;
        }

        void updatenpcList1Data()
        {
            if (npcdataLoaded)
            {
                if (npcList.SelectedRows.Count == 1)
                {
                    updateVars = true;
                    int num = npcList.SelectedRows[0].Index;
                    textBox11.BackColor = Color.White;
                    textBox12.BackColor = Color.White;
                    textBox13.BackColor = Color.White;
                    textBox15.BackColor = Color.White;
                    textBox2.BackColor = Color.White;
                    textBox3.BackColor = Color.White;
                    textBox4.BackColor = Color.White;
                    textBox5.BackColor = Color.White;
                    textBox6.BackColor = Color.White;
                    textBox7.BackColor = Color.White;
                    textBox8.BackColor = Color.White;
                    textBox9.BackColor = Color.White;
                    comboBox1.SelectedIndex = npcdata.m_aAreas[num].iNPCType;
                    textBox11.Text = npcdata.m_aAreas[num].idCtrl.ToString();
                    textBox12.Text = npcdata.m_aAreas[num].iLifeTime.ToString();
                    textBox13.Text = npcdata.m_aAreas[num].iMaxNum.ToString();
                    comboBox7.SelectedIndex = npcdata.m_aAreas[num].iType;
                    textBox15.Text = npcdata.m_aAreas[num].dwGenID.ToString();
                    comboBox5.SelectedIndex = npcdata.m_aAreas[num].iGroupType;
                    textBox2.Text = npcdata.m_aAreas[num].vPos[0].ToString("N3");
                    textBox3.Text = npcdata.m_aAreas[num].vPos[1].ToString("N3");
                    textBox4.Text = npcdata.m_aAreas[num].vPos[2].ToString("N3");
                    textBox5.Text = npcdata.m_aAreas[num].vDir[0].ToString("N3");//.toDegrees().ToString();
                    textBox6.Text = npcdata.m_aAreas[num].vDir[1].ToString("N3");//.toDegrees().ToString();
                    textBox7.Text = npcdata.m_aAreas[num].vDir[2].ToString("N3");//.toDegrees().ToString();
                    textBox8.Text = npcdata.m_aAreas[num].vExts[0].ToString("N3");
                    textBox9.Text = npcdata.m_aAreas[num].vExts[1].ToString("N3");
                    textBox10.Text = npcdata.m_aAreas[num].vExts[2].ToString("N3");
                    checkBox1.Checked = npcdata.m_aAreas[num].bInitGen;
                    checkBox2.Checked = npcdata.m_aAreas[num].bAutoRevive;
                    checkBox3.Checked = npcdata.m_aAreas[num].bValidOnce;
                    checkBox1.CheckState = npcdata.m_aAreas[num].bInitGen ? CheckState.Checked : CheckState.Unchecked;
                    checkBox2.CheckState = npcdata.m_aAreas[num].bAutoRevive ? CheckState.Checked : CheckState.Unchecked;
                    checkBox3.CheckState = npcdata.m_aAreas[num].bValidOnce ? CheckState.Checked : CheckState.Unchecked;
                    groupBox1.Enabled = true;
                    npcList1.Enabled = true;
                    npcList1.Rows.Clear();
                    for (int j = 0; j < npcdata.m_aAreas[num].iNumGen; j++)
                    {
                        npcList1.Rows.Add(getnpcList1Node2Name(num, j));
                    }
                    if (npcdata.m_aAreas[num].iNumGen > 0)
                    {
                        selectRow(npcList1, 0);
                        //npcList1.CurrentCell = npcList1.Rows[0].Cells[0];
                        enablenpcList1Controls();
                        updatenpcList2Data();
                    }
                    else
                    {
                        disablenpcList1Controls();
                    }
                    updateVars = false;
                }
                else
                {
                    updateVars = true;
                    comboBox1.SelectedItem = null;
                    textBox11.Text = String.Empty;
                    textBox12.Text = String.Empty;
                    textBox13.Text = String.Empty;
                    comboBox7.SelectedItem = null;
                    textBox15.Text = String.Empty;
                    comboBox5.SelectedItem = null;
                    textBox2.Text = String.Empty;
                    textBox3.Text = String.Empty;
                    textBox4.Text = String.Empty;
                    textBox5.Text = String.Empty;
                    textBox6.Text = String.Empty;
                    textBox7.Text = String.Empty;
                    textBox8.Text = String.Empty;
                    textBox9.Text = String.Empty;
                    textBox10.Text = String.Empty;
                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                    checkBox3.Checked = false;
                    checkBox1.CheckState = CheckState.Indeterminate;
                    checkBox2.CheckState = CheckState.Indeterminate;
                    checkBox3.CheckState = CheckState.Indeterminate;
                    comboBox5.SelectedItem = null;
                    comboBox7.SelectedItem = null;
                    comboBox1.SelectedItem = null;
                    textBox11.BackColor = Color.White;
                    textBox12.BackColor = Color.White;
                    textBox13.BackColor = Color.White;
                    textBox15.BackColor = Color.White;
                    textBox2.BackColor = Color.White;
                    textBox3.BackColor = Color.White;
                    textBox4.BackColor = Color.White;
                    textBox5.BackColor = Color.White;
                    textBox6.BackColor = Color.White;
                    textBox7.BackColor = Color.White;
                    textBox8.BackColor = Color.White;
                    textBox9.BackColor = Color.White;
                    npcList1.Rows.Clear();
                    groupBox1.Enabled = false;
                    npcList1.Enabled = false;
                    comboBox8.SelectedItem = null;
                    disablenpcList1Controls();
                    textBox10.BackColor = Color.White;
                    textBox16.BackColor = Color.White;
                    textBox17.BackColor = Color.White;
                    textBox18.BackColor = Color.White;
                    textBox19.BackColor = Color.White;
                    textBox20.BackColor = Color.White;
                    textBox21.BackColor = Color.White;
                    textBox22.BackColor = Color.White;
                    textBox23.BackColor = Color.White;
                    textBox24.BackColor = Color.White;
                    textBox25.BackColor = Color.White;
                    textBox26.BackColor = Color.White;
                    textBox28.BackColor = Color.White;
                    textBox29.BackColor = Color.White;
                    textBox30.BackColor = Color.White;
                    updateVars = false;
                }
            }
        }

        void updatenpcList2Data()
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && npcList1.SelectedRows.Count > 0)
            {
                if (npcList1.SelectedRows.Count == 1)
                {
                    int num = npcList.SelectedRows[0].Index;
                    int j = npcList1.SelectedRows[0].Index;
                    updateVars = true;
                    textBox16.BackColor = Color.White;
                    textBox17.BackColor = Color.White;
                    textBox18.BackColor = Color.White;
                    textBox19.BackColor = Color.White;
                    textBox20.BackColor = Color.White;
                    textBox21.BackColor = Color.White;
                    textBox22.BackColor = Color.White;
                    textBox23.BackColor = Color.White;
                    textBox24.BackColor = Color.White;
                    textBox25.BackColor = Color.White;
                    textBox26.BackColor = Color.White;
                    textBox28.BackColor = Color.White;
                    textBox29.BackColor = Color.White;
                    textBox16.Text = npcdata.m_aAreas[num].m_aNPCGens[j].dwID.ToString();
                    textBox17.Text = npcdata.m_aAreas[num].m_aNPCGens[j].dwNum.ToString();
                    textBox18.Text = npcdata.m_aAreas[num].m_aNPCGens[j].iRefresh.ToString();
                    textBox19.Text = npcdata.m_aAreas[num].m_aNPCGens[j].dwDiedTimes.ToString();
                    textBox20.Text = npcdata.m_aAreas[num].m_aNPCGens[j].dwAggressive.ToString();
                    textBox21.Text = npcdata.m_aAreas[num].m_aNPCGens[j].fOffsetWater.ToString();
                    textBox22.Text = npcdata.m_aAreas[num].m_aNPCGens[j].fOffsetTrn.ToString();
                    textBox23.Text = npcdata.m_aAreas[num].m_aNPCGens[j].dwFaction.ToString();
                    textBox24.Text = npcdata.m_aAreas[num].m_aNPCGens[j].dwFacHelper.ToString();
                    textBox25.Text = npcdata.m_aAreas[num].m_aNPCGens[j].dwFacAccept.ToString();
                    checkBox4.Checked = npcdata.m_aAreas[num].m_aNPCGens[j].bNeedHelp;
                    checkBox5.Checked = npcdata.m_aAreas[num].m_aNPCGens[j].bDefFaction;
                    checkBox6.Checked = npcdata.m_aAreas[num].m_aNPCGens[j].bDefFacHelper;
                    checkBox7.Checked = npcdata.m_aAreas[num].m_aNPCGens[j].bDefFacAccept;
                    checkBox4.CheckState = npcdata.m_aAreas[num].m_aNPCGens[j].bNeedHelp ? CheckState.Checked : CheckState.Unchecked;
                    checkBox5.CheckState = npcdata.m_aAreas[num].m_aNPCGens[j].bDefFaction ? CheckState.Checked : CheckState.Unchecked;
                    checkBox6.CheckState = npcdata.m_aAreas[num].m_aNPCGens[j].bDefFacHelper ? CheckState.Checked : CheckState.Unchecked;
                    checkBox7.CheckState = npcdata.m_aAreas[num].m_aNPCGens[j].bDefFacAccept ? CheckState.Checked : CheckState.Unchecked;
                    textBox26.Text = npcdata.m_aAreas[num].m_aNPCGens[j].iPathID.ToString();
                    comboBox8.SelectedIndex = npcdata.m_aAreas[num].m_aNPCGens[j].iLoopType;
                    textBox28.Text = npcdata.m_aAreas[num].m_aNPCGens[j].iSpeedFlag.ToString();
                    textBox29.Text = npcdata.m_aAreas[num].m_aNPCGens[j].iDeadTime.ToString();
                    if (npcdata.version >= 11)
                    {
                        textBox30.Text = npcdata.m_aAreas[num].m_aNPCGens[j].iRefreshLower.ToString();
                    }
                    updateVars = false;
                }
                else
                {
                    int num = npcList.SelectedRows[0].Index;
                    int j = npcList1.SelectedRows[0].Index;
                    updateVars = true;
                    textBox16.Text = "";
                    textBox17.Text = "";
                    textBox18.Text = "";
                    textBox19.Text = "";
                    textBox20.Text = "";
                    textBox21.Text = "";
                    textBox22.Text = "";
                    textBox23.Text = "";
                    textBox24.Text = "";
                    textBox25.Text = "";
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;
                    checkBox6.Checked = false;
                    checkBox7.Checked = false;
                    textBox26.Text = "";
                    comboBox8.SelectedItem = null;
                    textBox28.Text = "";
                    textBox29.Text = "";
                    textBox16.BackColor = Color.White;
                    textBox17.BackColor = Color.White;
                    textBox18.BackColor = Color.White;
                    textBox19.BackColor = Color.White;
                    textBox20.BackColor = Color.White;
                    textBox21.BackColor = Color.White;
                    textBox22.BackColor = Color.White;
                    textBox23.BackColor = Color.White;
                    textBox24.BackColor = Color.White;
                    textBox25.BackColor = Color.White;
                    textBox26.BackColor = Color.White;
                    textBox28.BackColor = Color.White;
                    textBox29.BackColor = Color.White;
                    checkBox4.CheckState = CheckState.Indeterminate;
                    checkBox5.CheckState = CheckState.Indeterminate;
                    checkBox6.CheckState = CheckState.Indeterminate;
                    checkBox7.CheckState = CheckState.Indeterminate;
                    if (npcdata.version >= 11)
                    {
                        textBox30.Text = "";
                        textBox30.BackColor = Color.White;

                    }
                    updateVars = false;
                }
            }
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            if (npcdataLoaded && npcList.CurrentCell != null)
            {
                createNpc1.Enabled = true;
                importNpc1.Enabled = true;
                if (npcList1.CurrentCell != null)
                {
                    copyNpc1.Enabled = true;
                    removeNpc1.Enabled = true;
                    exportNpc1.Enabled = true;
                }
                else
                {
                    removeNpc1.Enabled = false;
                    copyNpc1.Enabled = false;
                    exportNpc1.Enabled = false;
                }
            }
            else
            {
                createNpc1.Enabled = false;
                removeNpc1.Enabled = false;
                copyNpc1.Enabled = false;
                exportNpc1.Enabled = false;
                importNpc1.Enabled = false;
            }
        }

        private void createNpc1_Click(object sender, EventArgs e)
        {
            int i = npcList.SelectedRows[0].Index;
            npcdata.m_aAreas[i].m_aNPCGens.Add(new NPCGENFILEAIGEN());
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwID = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwNum = 1;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iRefresh = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwDiedTimes = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwAggressive = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].fOffsetWater = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].fOffsetTrn = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwFaction = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwFacHelper = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwFacAccept = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bNeedHelp = false;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bDefFaction = false;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bDefFacHelper = false;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bDefFacAccept = false;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iPathID = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iLoopType = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iSpeedFlag = 0;
            npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iDeadTime = 0;
            if (npcdata.version >= 11)
            {
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iRefreshLower = 0;
            }
            npcList1.Rows.Add(getnpcList1Node2Name(i, npcdata.m_aAreas[i].iNumGen));
            enablenpcList1Controls();
            selectRow(npcList1, npcdata.m_aAreas[i].iNumGen);
            npcdata.m_aAreas[i].iNumGen++;
            npcList.Rows[npcList.CurrentCell.RowIndex].SetValues(getnpcList1Node1Name(npcList.CurrentCell.RowIndex));
            button1_Click(null, null);
        }

        private void removeNpc1_Click(object sender, EventArgs e)
        {
            int i = npcList.SelectedRows[0].Index;
            npcList1.Enabled = false;
            foreach (DataGridViewRow row in npcList1.SelectedRows)
            {
                npcdata.m_aAreas[i].m_aNPCGens.RemoveAt(row.Index);
                npcdata.m_aAreas[i].iNumGen--;
                npcList1.Rows.RemoveAt(row.Index);
                Application.DoEvents();
            }
            for (int j = 0; j < npcList.Rows.Count; j++)
            {
                npcList.Rows[j].SetValues(getnpcList1Node1Name(j));
            }
            npcList1.Enabled = true;
            if (npcdata.m_aAreas[i].iNumGen > 0)
            {
                enablenpcList1Controls();
            }
            else
            {
                disablenpcList1Controls();
            }
            npcList.Rows[i].SetValues(getnpcList1Node1Name(i));
            button1_Click(null, null);
        }

        private void copyNpc1_Click(object sender, EventArgs e)
        {
            int i = npcList.SelectedRows[0].Index;
            int start = npcdata.m_aAreas[i].iNumGen;
            int j;
            for (int k = npcList1.SelectedRows.Count - 1; k >= 0; k--)
            {
                j = npcList1.SelectedRows[k].Index;
                npcdata.m_aAreas[i].m_aNPCGens.Add(new NPCGENFILEAIGEN());
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwID = npcdata.m_aAreas[i].m_aNPCGens[j].dwID;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwNum = npcdata.m_aAreas[i].m_aNPCGens[j].dwNum;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iRefresh = npcdata.m_aAreas[i].m_aNPCGens[j].iRefresh;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwDiedTimes = npcdata.m_aAreas[i].m_aNPCGens[j].dwDiedTimes;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwAggressive = npcdata.m_aAreas[i].m_aNPCGens[j].dwAggressive;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].fOffsetWater = npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetWater;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].fOffsetTrn = npcdata.m_aAreas[i].m_aNPCGens[j].fOffsetTrn;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwFaction = npcdata.m_aAreas[i].m_aNPCGens[j].dwFaction;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwFacHelper = npcdata.m_aAreas[i].m_aNPCGens[j].dwFacHelper;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].dwFacAccept = npcdata.m_aAreas[i].m_aNPCGens[j].dwFacAccept;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bNeedHelp = npcdata.m_aAreas[i].m_aNPCGens[j].bNeedHelp;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bDefFaction = npcdata.m_aAreas[i].m_aNPCGens[j].bDefFaction;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bDefFacHelper = npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacHelper;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].bDefFacAccept = npcdata.m_aAreas[i].m_aNPCGens[j].bDefFacAccept;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iPathID = npcdata.m_aAreas[i].m_aNPCGens[j].iPathID;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iLoopType = npcdata.m_aAreas[i].m_aNPCGens[j].iLoopType;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iSpeedFlag = npcdata.m_aAreas[i].m_aNPCGens[j].iSpeedFlag;
                npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iDeadTime = npcdata.m_aAreas[i].m_aNPCGens[j].iDeadTime;
                if (npcdata.version >= 11)
                {
                    npcdata.m_aAreas[i].m_aNPCGens[npcdata.m_aAreas[i].iNumGen].iRefreshLower = npcdata.m_aAreas[i].m_aNPCGens[j].iRefreshLower;
                }
                npcList1.Rows.Add(getnpcList1Node2Name(i, npcdata.m_aAreas[i].iNumGen));
                npcdata.m_aAreas[i].iNumGen++;
            }
            selectRows(npcList1, start, npcdata.m_aAreas[i].iNumGen - 1);
            npcList.Rows[i].SetValues(getnpcList1Node1Name(i));
            button1_Click(null, null);
        }

        private void enableResList1Controls()
        {
            textBox50.Enabled = true;
            textBox49.Enabled = true;
            textBox27.Enabled = true;
            textBox47.Enabled = true;
            textBox46.Enabled = true;
        }

        private void disableResList1Controls()
        {
            textBox50.Enabled = false;
            textBox49.Enabled = false;
            textBox27.Enabled = false;
            textBox47.Enabled = false;
            textBox46.Enabled = false;
        }

        void updateRes2ListData()
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int i = dataGridView1.SelectedRows[0].Index;
                if (npcdata.m_aResAreas[i].iNumRes > 0 && dataGridView5.CurrentCell != null)
                {
                    int j = dataGridView5.CurrentCell.RowIndex;
                    updateVars = true;
                    textBox50.Text = npcdata.m_aResAreas[i].m_aRes[j].iResType.ToString();
                    textBox49.Text = npcdata.m_aResAreas[i].m_aRes[j].idTemplate.ToString();
                    textBox27.Text = npcdata.m_aResAreas[i].m_aRes[j].dwRefreshTime.ToString();
                    textBox47.Text = npcdata.m_aResAreas[i].m_aRes[j].dwNumber.ToString();
                    textBox46.Text = npcdata.m_aResAreas[i].m_aRes[j].fHeiOff.ToString();
                    updateVars = false;
                }
            }
        }

        void updateResListData()
        {
            if (npcdataLoaded)
            {
                if (dataGridView1.SelectedRows.Count == 1)
                {
                    updateVars = true;
                    int i = dataGridView1.SelectedRows[0].Index;
                    textBox36.BackColor = Color.White;
                    textBox37.BackColor = Color.White;
                    textBox41.BackColor = Color.White;
                    textBox40.BackColor = Color.White;
                    textBox38.BackColor = Color.White;
                    textBox39.BackColor = Color.White;
                    textBox31.BackColor = Color.White;
                    textBox32.BackColor = Color.White;
                    textBox33.BackColor = Color.White;
                    textBox34.BackColor = Color.White;
                    textBox35.BackColor = Color.White;
                    textBox33.BackColor = Color.White;
                    textBox1.BackColor = Color.White;
                    textBox1.Text = npcdata.m_aResAreas[i].rad.ToString();
                    textBox36.Text = npcdata.m_aResAreas[i].dwGenID.ToString();
                    textBox37.Text = npcdata.m_aResAreas[i].idCtrl.ToString();
                    textBox41.Text = npcdata.m_aResAreas[i].dir[0].ToString();
                    textBox40.Text = npcdata.m_aResAreas[i].dir[1].ToString();
                    textBox38.Text = npcdata.m_aResAreas[i].iMaxNum.ToString();
                    textBox39.Text = npcdata.m_aResAreas[i].rad.ToString();
                    textBox31.Text = npcdata.m_aResAreas[i].vPos[0].ToString("N3");
                    textBox32.Text = npcdata.m_aResAreas[i].vPos[1].ToString("N3");
                    textBox33.Text = npcdata.m_aResAreas[i].vPos[2].ToString("N3");
                    textBox34.Text = npcdata.m_aResAreas[i].fExtX.ToString("N3");
                    textBox35.Text = npcdata.m_aResAreas[i].fExtZ.ToString("N3");
                    checkBox8.Checked = npcdata.m_aResAreas[i].bValidOnce;
                    checkBox9.Checked = npcdata.m_aResAreas[i].bAutoRevive;
                    checkBox10.Checked = npcdata.m_aResAreas[i].bInitGen;
                    checkBox8.CheckState = npcdata.m_aResAreas[i].bValidOnce ? CheckState.Checked : CheckState.Unchecked;
                    checkBox9.CheckState = npcdata.m_aResAreas[i].bAutoRevive ? CheckState.Checked : CheckState.Unchecked;
                    checkBox10.CheckState = npcdata.m_aResAreas[i].bInitGen ? CheckState.Checked : CheckState.Unchecked;
                    groupBox2.Enabled = true;
                    dataGridView5.Rows.Clear();
                    for (int i1 = 0; i1 < npcdata.m_aResAreas[i].iNumRes; i1++)
                    {
                        dataGridView5.Rows.Add(getResListNode2Name(i, i1));
                    }
                    if (npcdata.m_aResAreas[i].iNumRes > 0)
                    {
                        selectRow(dataGridView5, 0);
                        enableResList1Controls();
                        // updateRes2ListData();
                    }
                    else
                    {
                        disableResList1Controls();
                    }
                    updateVars = false;
                }
                else
                {
                    updateVars = true;
                    textBox36.Text = "";
                    textBox37.Text = "";
                    textBox41.Text = "";
                    textBox40.Text = "";
                    textBox38.Text = "";
                    textBox39.Text = "";
                    textBox31.Text = "";
                    textBox32.Text = "";
                    textBox33.Text = "";
                    textBox34.Text = "";
                    textBox35.Text = "";
                    textBox33.Text = "";
                    textBox1.Text = "";
                    textBox1.BackColor = Color.White;
                    textBox36.BackColor = Color.White;
                    textBox37.BackColor = Color.White;
                    textBox41.BackColor = Color.White;
                    textBox40.BackColor = Color.White;
                    textBox38.BackColor = Color.White;
                    textBox39.BackColor = Color.White;
                    textBox31.BackColor = Color.White;
                    textBox32.BackColor = Color.White;
                    textBox33.BackColor = Color.White;
                    textBox34.BackColor = Color.White;
                    textBox35.BackColor = Color.White;
                    textBox33.BackColor = Color.White;
                    checkBox8.Checked = false;
                    checkBox9.Checked = false;
                    checkBox10.Checked = false;
                    checkBox8.CheckState = CheckState.Indeterminate;
                    checkBox9.CheckState = CheckState.Indeterminate;
                    checkBox10.CheckState = CheckState.Indeterminate;
                    dataGridView5.Rows.Clear();
                    groupBox2.Enabled = false;
                    disableResList1Controls();
                    updateVars = false;
                }
            }
        }

        private void npcList_SelectionChanged(object sender, EventArgs e)
        {
            updatenpcList1Data();
        }

        private void resList_SelectionChanged(object sender, EventArgs e)
        {
            updateResListData();
        }

        private void textBox36_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].dwGenID = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox37_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].idCtrl = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox38_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].iMaxNum = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox41_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                byte value;
                tb.BackColor = Color.White;
                if (byte.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].dir[0] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox40_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                byte value;
                tb.BackColor = Color.White;
                if (byte.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].dir[1] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox39_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                byte value;
                tb.BackColor = Color.White;
                if (byte.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].rad = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox31_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].vPos[0] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox32_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].vPos[1] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox34_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].fExtX = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox35_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].fExtZ = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                {
                    npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].bValidOnce = checkBox8.Checked;
                }
            }
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                {
                    npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].bAutoRevive = checkBox9.Checked;
                }
            }
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                {
                    npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].bInitGen = checkBox10.Checked;
                }
            }
        }

        private void textBox50_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView5.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView5.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[i].Index].iResType = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox49_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView5.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView5.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[i].Index].idTemplate = value;
                        dataGridView5.Rows[dataGridView5.SelectedRows[0].Index].SetValues(getResListNode2Name(dataGridView1.SelectedRows[0].Index, dataGridView5.SelectedRows[i].Index));
                    }
                    dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].SetValues(getResListNode1Name(dataGridView1.SelectedRows[0].Index));
                    updateTriggerData();
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox48_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView5.CurrentCell != null)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                try
                {
                    value = uint.Parse(tb.Text);
                    tb.BackColor = Color.White;
                }
                catch (Exception)
                {
                    tb.BackColor = Color.Red;
                    return;
                }
                npcdata.m_aResAreas[dataGridView1.CurrentCell.RowIndex].m_aRes[dataGridView5.CurrentCell.RowIndex].dwRefreshTime = value;
            }
        }

        private void textBox47_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView5.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView5.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[i].Index].dwNumber = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox46_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView5.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView5.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[i].Index].fHeiOff = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void dataGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView dg = (DataGridView)sender;
                if (dg.SelectedRows.Count == 0)
                {
                    //dg.ClearSelection();
                    // Add this
                    //dg.CurrentCell = dg.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    // Can leave these here - doesn't hurt
                    dg.Rows[e.RowIndex].Selected = true;
                }
                dg.Focus();
            }
        }

        private void contextMenuStrip3_Opening(object sender, CancelEventArgs e)
        {
            if (npcdataLoaded)
            {
                createResource.Enabled = true;
                importRes.Enabled = true;
                if (dataGridView1.CurrentCell != null)
                {
                    copyResource.Enabled = true;
                    removeResource.Enabled = true;
                    exportRes.Enabled = true;
                }
                else
                {
                    removeResource.Enabled = false;
                    copyResource.Enabled = false;
                    exportRes.Enabled = false;
                }
            }
            else
            {
                createResource.Enabled = false;
                removeResource.Enabled = false;
                copyResource.Enabled = false;
                exportRes.Enabled = false;
                importRes.Enabled = false;
            }
        }

        public void selectRows(DataGridView dg, int start, int end)
        {
            dg.ClearSelection();
            dg.FirstDisplayedScrollingRowIndex = start;
            for (int i = start; i <= end; i++)
            {
                dg.Rows[i].Selected = true;
            }
        }

        public void selectRow(DataGridView dg, int i)
        {
            dg.ClearSelection();
            dg.FirstDisplayedScrollingRowIndex = i;
            dg.Rows[i].Selected = true;
        }

        private void createResource_Click(object sender, EventArgs e)
        {
            npcdata.m_aResAreas.Add(new NPCGENFILERESAREA());
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos = new float[3];
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos[0] = 0;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos[1] = 0;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos[2] = 0;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].fExtX = 0;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].fExtZ = 0;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].iNumRes = 0;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].bInitGen = false;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].bAutoRevive = false;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].bValidOnce = false;
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].dwGenID = 0;
            if (npcdata.version >= 6)
            {
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].dir = new byte[2];
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].dir[0] = 0;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].dir[1] = 0;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].rad = 0;
            }
            if (npcdata.version >= 7)
            {
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].idCtrl = 0;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].iMaxNum = 0;
            }
            npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes = new List<NPCGENFILERES>();
            dataGridView1.Rows.Add(getResListNode1Name(npcdata.Header.iNumResArea));
            selectRow(dataGridView1, npcdata.Header.iNumResArea);
            npcdata.Header.iNumResArea++;
            tabPage2.Text = getLocalizedString("Resources") + " [" + dataGridView1.Rows.Count + "]";
            button1_Click(null, null);
            updateTriggerData();
        }

        private void removeResource_Click(object sender, EventArgs e)
        {
            dataGridView1.Enabled = false;
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                npcdata.m_aResAreas.RemoveAt(row.Index);
                npcdata.Header.iNumResArea--;
                dataGridView1.Rows.RemoveAt(row.Index);
                Application.DoEvents();
            }
            dataGridView1.Enabled = true;
            for (int j = 0; j < dataGridView1.Rows.Count; j++)
            {
                dataGridView1.Rows[j].SetValues(getResListNode1Name(j));
            }
            tabPage2.Text = getLocalizedString("Resources") + " [" + dataGridView1.Rows.Count + "]";
            button1_Click(null, null);
            updateTriggerData();
        }

        private void copyResource_Click(object sender, EventArgs e)
        {
            int start = npcdata.Header.iNumResArea;
            int i;
            for (int k = dataGridView1.SelectedRows.Count - 1; k >= 0; k--)
            {
                i = dataGridView1.SelectedRows[k].Index;
                npcdata.m_aResAreas.Add(new NPCGENFILERESAREA());
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos = new float[3];
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos[0] = npcdata.m_aResAreas[i].vPos[0];
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos[1] = npcdata.m_aResAreas[i].vPos[1];
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].vPos[2] = npcdata.m_aResAreas[i].vPos[2];
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].fExtX = npcdata.m_aResAreas[i].fExtX;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].fExtZ = npcdata.m_aResAreas[i].fExtZ;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].iNumRes = npcdata.m_aResAreas[i].iNumRes;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].bInitGen = npcdata.m_aResAreas[i].bInitGen;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].bAutoRevive = npcdata.m_aResAreas[i].bAutoRevive;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].bValidOnce = npcdata.m_aResAreas[i].bValidOnce;
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].dwGenID = npcdata.m_aResAreas[i].dwGenID;
                if (npcdata.version >= 6)
                {
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].dir = new byte[2];
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].dir[0] = npcdata.m_aResAreas[i].dir[0];
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].dir[1] = npcdata.m_aResAreas[i].dir[1];
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].rad = npcdata.m_aResAreas[i].rad;
                }
                if (npcdata.version >= 7)
                {
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].idCtrl = npcdata.m_aResAreas[i].idCtrl;
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].iMaxNum = npcdata.m_aResAreas[i].iMaxNum;
                }
                npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes = new List<NPCGENFILERES>();
                for (int j = 0; j < npcdata.m_aResAreas[i].iNumRes; j++)
                {
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes.Add(new NPCGENFILERES());
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes[j].dwNumber = npcdata.m_aResAreas[i].m_aRes[j].dwNumber;
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes[j].dwRefreshTime = npcdata.m_aResAreas[i].m_aRes[j].dwRefreshTime;
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes[j].fHeiOff = npcdata.m_aResAreas[i].m_aRes[j].fHeiOff;
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes[j].idTemplate = npcdata.m_aResAreas[i].m_aRes[j].idTemplate;
                    npcdata.m_aResAreas[npcdata.Header.iNumResArea].m_aRes[j].iResType = npcdata.m_aResAreas[i].m_aRes[j].iResType;
                }
                dataGridView1.Rows.Add(getResListNode1Name(npcdata.Header.iNumResArea));
                npcdata.Header.iNumResArea++;
            }
            selectRows(dataGridView1, start, npcdata.Header.iNumResArea - 1);
            tabPage2.Text = getLocalizedString("Resources") + " [" + dataGridView1.Rows.Count + "]";
            button1_Click(null, null);
            updateTriggerData();
        }

        private void contextMenuStrip4_Opening(object sender, CancelEventArgs e)
        {
            if (npcdataLoaded && dataGridView1.CurrentCell != null)
            {
                createResource1.Enabled = true;
                importRes1.Enabled = true;
                if (dataGridView5.CurrentCell != null)
                {
                    exportRes1.Enabled = true;
                    removeResource1.Enabled = true;
                    copyResource1.Enabled = true;
                }
                else
                {
                    removeResource1.Enabled = false;
                    copyResource1.Enabled = false;
                    exportRes1.Enabled = false;
                }
            }
            else
            {
                createResource1.Enabled = false;
                removeResource1.Enabled = false;
                copyResource1.Enabled = false;
                exportRes1.Enabled = false;
                importRes1.Enabled = false;
            }
        }

        private void createResource1_Click(object sender, EventArgs e)
        {
            int i = dataGridView1.SelectedRows[0].Index;
            npcdata.m_aResAreas[i].m_aRes.Add(new NPCGENFILERES());
            npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].dwNumber = 1;
            npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].dwRefreshTime = 0;
            npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].fHeiOff = 0;
            npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].idTemplate = 0;
            npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].iResType = 0;
            npcdata.m_aResAreas[i].iNumRes++;
            dataGridView5.Rows.Add(getResListNode2Name(i, npcdata.m_aResAreas[i].iNumRes - 1));
            selectRow(dataGridView5, npcdata.m_aResAreas[i].iNumRes - 1);
            dataGridView1.Rows[i].SetValues(getResListNode1Name(i));
            enableResList1Controls();
            updateRes2ListData();
        }

        private void removeResource1_Click(object sender, EventArgs e)
        {
            int i = dataGridView5.SelectedRows[0].Index;
            dataGridView5.Enabled = false;
            foreach (DataGridViewRow row in dataGridView5.SelectedRows)
            {
                npcdata.m_aResAreas[i].m_aRes.RemoveAt(row.Index);
                npcdata.m_aResAreas[i].iNumRes--;
                dataGridView5.Rows.RemoveAt(row.Index);
                Application.DoEvents();
            }
            dataGridView5.Enabled = true;
            if (npcdata.m_aAreas[i].iNumGen > 0)
            {
                enableResList1Controls();
            }
            else
            {
                disableResList1Controls();
            }
            dataGridView1.Rows[i].SetValues(getResListNode1Name(i));
            button1_Click(null, null);
        }

        private void copyResource1_Click(object sender, EventArgs e)
        {
            int i = dataGridView1.SelectedRows[0].Index;
            int start = npcdata.m_aResAreas[i].iNumRes;
            int j;
            for (int k = dataGridView5.SelectedRows.Count - 1; k >= 0; k--)
            {
                j = dataGridView5.SelectedRows[k].Index;
                npcdata.m_aResAreas[i].m_aRes.Add(new NPCGENFILERES());
                npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].dwNumber = npcdata.m_aResAreas[i].m_aRes[j].dwNumber;
                npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].dwRefreshTime = npcdata.m_aResAreas[i].m_aRes[j].dwRefreshTime;
                npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].fHeiOff = npcdata.m_aResAreas[i].m_aRes[j].fHeiOff;
                npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].idTemplate = npcdata.m_aResAreas[i].m_aRes[j].idTemplate;
                npcdata.m_aResAreas[i].m_aRes[npcdata.m_aResAreas[i].iNumRes].iResType = npcdata.m_aResAreas[i].m_aRes[j].iResType;
                dataGridView5.Rows.Add(getResListNode2Name(i, j));
                npcdata.m_aResAreas[i].iNumRes++;
            }
            selectRows(dataGridView5, start, npcdata.m_aResAreas[i].iNumRes - 1);
        }

        private void updateDynObjListData()
        {
            if (npcdataLoaded)
            {
                if (dataGridView2.SelectedRows.Count == 1)
                {
                    int i = dataGridView2.SelectedRows[0].Index;
                    updateVars = true;
                    textBox42.BackColor = Color.White;
                    textBox43.BackColor = Color.White;
                    textBox44.BackColor = Color.White;
                    textBox45.BackColor = Color.White;
                    textBox51.BackColor = Color.White;
                    textBox53.BackColor = Color.White;
                    textBox55.BackColor = Color.White;
                    textBox56.BackColor = Color.White;
                    textBox52.BackColor = Color.White;
                    textBox42.Text = npcdata.m_aDynObjs[i].dwDynObjID.ToString();
                    textBox43.Text = npcdata.m_aDynObjs[i].vPos[0].ToString("N3");
                    textBox44.Text = npcdata.m_aDynObjs[i].vPos[1].ToString("N3");
                    textBox45.Text = npcdata.m_aDynObjs[i].vPos[2].ToString("N3");
                    textBox51.Text = npcdata.m_aDynObjs[i].idController.ToString();
                    textBox53.Text = npcdata.m_aDynObjs[i].rad.ToString();
                    textBox55.Text = npcdata.m_aDynObjs[i].dir[0].ToString();
                    textBox56.Text = npcdata.m_aDynObjs[i].dir[1].ToString();
                    textBox52.Text = npcdata.m_aDynObjs[i].scale.ToString();
                    Image img;
                    dynobjImages.TryGetValue(npcdata.m_aDynObjs[i].dwDynObjID, out img);
                    if (img != null)
                    {
                        pictureBox1.Image = img;
                    }
                    else
                    {
                        pictureBox1.Image = null;
                    }
                    String s;
                    if (dynobjs.TryGetValue(npcdata.m_aDynObjs[i].dwDynObjID, out s))
                    {
                        comboBox2.SelectedItem = s;
                    }
                    updateVars = false;
                }
                else
                {
                    updateVars = true;
                    textBox42.Text = "";
                    textBox43.Text = "";
                    textBox44.Text = "";
                    textBox45.Text = "";
                    textBox51.Text = "";
                    textBox53.Text = "";
                    textBox55.Text = "";
                    textBox56.Text = "";
                    textBox52.Text = "";
                    comboBox2.SelectedItem = null;
                    textBox42.BackColor = Color.White;
                    textBox43.BackColor = Color.White;
                    textBox44.BackColor = Color.White;
                    textBox45.BackColor = Color.White;
                    textBox51.BackColor = Color.White;
                    textBox53.BackColor = Color.White;
                    textBox55.BackColor = Color.White;
                    textBox56.BackColor = Color.White;
                    textBox52.BackColor = Color.White;
                    pictureBox1.Image = null;
                    updateVars = false;
                }
            }
        }
        private void dataGridView2_SelectionChanged_1(object sender, EventArgs e)
        {
            updateDynObjListData();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && comboBox2.SelectedItem != null)
            {
                uint s = 0;
                if (dynobjsInverse.TryGetValue((String)comboBox2.SelectedItem, out s))
                {
                    textBox42.Text = s.ToString();
                    Image img;
                    if (dynobjImages.TryGetValue(npcdata.m_aDynObjs[dataGridView2.CurrentCell.RowIndex].dwDynObjID, out img))
                    {
                        pictureBox1.Image = img;
                    }
                    else
                    {
                        pictureBox1.Image = null;
                    }
                }
            }
        }

        private void textBox42_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    string name;
                    if (dataGridView2.SelectedRows.Count == 1 && dynobjs.TryGetValue(value, out name))
                    {
                        comboBox2.SelectedItem = name;
                    }
                    comboBox2_SelectedIndexChanged(null, null);
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].dwDynObjID = value;
                        dataGridView2.Rows[dataGridView2.SelectedRows[i].Index].SetValues(getDynObj1Name(dataGridView2.SelectedRows[i].Index));
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox51_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].idController = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox52_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                byte value;
                tb.BackColor = Color.White;
                if (byte.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].scale = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox53_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                byte value;
                tb.BackColor = Color.White;
                if (byte.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].rad = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox55_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                byte value;
                tb.BackColor = Color.White;
                if (byte.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].dir[0] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox56_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                byte value;
                tb.BackColor = Color.White;
                if (byte.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].dir[1] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox43_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].vPos[0] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox44_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].vPos[1] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox45_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView2.SelectedRows.Count; i++)
                    {
                        npcdata.m_aDynObjs[dataGridView2.SelectedRows[i].Index].vPos[2] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void dynObjMenu_Opening(object sender, CancelEventArgs e)
        {
            if (npcdataLoaded)
            {
                createDynObj.Enabled = true;
                importDynObj.Enabled = true;
                if (dataGridView2.CurrentCell != null)
                {
                    copyDynObj.Enabled = true;
                    removeDynObj.Enabled = true;
                    exportDynObj.Enabled = true;
                }
                else
                {
                    removeDynObj.Enabled = false;
                    copyDynObj.Enabled = false;
                    exportDynObj.Enabled = false;
                }
            }
            else
            {
                createDynObj.Enabled = false;
                removeDynObj.Enabled = false;
                copyDynObj.Enabled = false;
                importDynObj.Enabled = false;
                exportDynObj.Enabled = false;
            }
        }

        private void createDynObj_Click(object sender, EventArgs e)
        {
            npcdata.m_aDynObjs.Add(new NPCGENFILEDYNOBJ());
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dwDynObjID = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dir = new byte[2];
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dir[0] = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dir[1] = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].idController = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].rad = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].scale = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos = new float[3];
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos[0] = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos[1] = 0;
            npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos[2] = 0;
            dataGridView2.Rows.Add(getDynObj1Name(npcdata.Header.iNumDynObj));
            selectRow(dataGridView2, npcdata.Header.iNumDynObj);
            npcdata.Header.iNumDynObj++;
            updateDynObjListData();
            tabPage3.Text = getLocalizedString("Dynamic objects") + " [" + dataGridView2.Rows.Count + "]";
            button1_Click(null, null);
            updateTriggerData();
        }

        private void removeDynObj_Click(object sender, EventArgs e)
        {
            dataGridView2.Enabled = false;
            foreach (DataGridViewRow row in dataGridView2.SelectedRows)
            {
                npcdata.m_aDynObjs.RemoveAt(row.Index);
                npcdata.Header.iNumDynObj--;
                dataGridView2.Rows.RemoveAt(row.Index);
                Application.DoEvents();
            }
            for (int j = 0; j < dataGridView2.Rows.Count; j++)
            {
                dataGridView2.Rows[j].SetValues(getDynObj1Name(j));
            }
            dataGridView2.Enabled = true;
            button1_Click(null, null);
            updateTriggerData();
            tabPage3.Text = getLocalizedString("Dynamic objects") + " [" + dataGridView2.Rows.Count + "]";
        }

        private void copyDynObj_Click(object sender, EventArgs e)
        {
            int start = npcdata.Header.iNumDynObj;
            int i;
            for (int k = dataGridView2.SelectedRows.Count - 1; k >= 0; k--)
            {
                i = dataGridView2.SelectedRows[k].Index;
                npcdata.m_aDynObjs.Add(new NPCGENFILEDYNOBJ());
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dwDynObjID = npcdata.m_aDynObjs[i].dwDynObjID;
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dir = new byte[2];
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dir[0] = npcdata.m_aDynObjs[i].dir[0];
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].dir[1] = npcdata.m_aDynObjs[i].dir[1];
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].idController = npcdata.m_aDynObjs[i].idController;
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].rad = npcdata.m_aDynObjs[i].rad;
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].scale = npcdata.m_aDynObjs[i].scale;
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos = new float[3];
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos[0] = npcdata.m_aDynObjs[i].vPos[0];
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos[1] = npcdata.m_aDynObjs[i].vPos[1];
                npcdata.m_aDynObjs[npcdata.Header.iNumDynObj].vPos[2] = npcdata.m_aDynObjs[i].vPos[2];
                dataGridView2.Rows.Add(getDynObj1Name(npcdata.Header.iNumDynObj));
                npcdata.Header.iNumDynObj++;
            }
            selectRows(dataGridView2, start, npcdata.Header.iNumDynObj - 1);
            tabPage3.Text = getLocalizedString("Dynamic objects") + " [" + dataGridView2.Rows.Count + "]";
            button1_Click(null, null);
            updateTriggerData();
        }

        public byte[] getBytesGBK(String Value)
        {
            Encoding enc = Encoding.GetEncoding("GBK");
            byte[] target = new byte[128];
            byte[] source = enc.GetBytes(Value);
            if (target.Length > source.Length)
            {
                Array.Copy(source, target, source.Length);
            }
            else
            {
                Array.Copy(source, target, target.Length);
            }
            return target;
        }

        public String getStringGBK(byte[] value)
        {
            return Encoding.GetEncoding("GBK").GetString(value);
        }

        private void textBox59_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars && textBox59.Text.Length > 0)
            {
                for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                {
                    npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].szName = getBytesGBK(textBox59.Text);
                    dataGridView3.Rows[dataGridView3.SelectedRows[i].Index].SetValues(getTriggerName(dataGridView3.SelectedRows[i].Index));
                }
            }
        }

        private int convertWeekDayBack(int week)
        {
            switch (week)
            {
                case 7: return 0;
                case 0: return -1;
                default: return week;
            }
        }

        private String convertWeekDay(int week)
        {
            switch (week)
            {
                case -1: return (String)comboBox3.Items[0];
                case 0: return (String)comboBox3.Items[7];
                case 1: return (String)comboBox3.Items[1];
                case 2: return (String)comboBox3.Items[2];
                case 3: return (String)comboBox3.Items[3];
                case 4: return (String)comboBox3.Items[4];
                case 5: return (String)comboBox3.Items[5];
                case 6: return (String)comboBox3.Items[6];
                default: return (String)comboBox3.Items[0];
            }
        }

        private void updateTriggerData()
        {
            if (npcdataLoaded)
            {
                if (dataGridView3.SelectedRows.Count == 1)
                {
                    int i = dataGridView3.SelectedRows[0].Index;
                    updateVars = true;
                    textBox64.BackColor = Color.White;
                    textBox59.BackColor = Color.White;
                    textBox58.BackColor = Color.White;
                    textBox57.BackColor = Color.White;
                    textBox54.BackColor = Color.White;
                    textBox61.BackColor = Color.White;
                    textBox62.BackColor = Color.White;
                    textBox63.BackColor = Color.White;
                    textBox65.BackColor = Color.White;
                    textBox66.BackColor = Color.White;
                    textBox71.BackColor = Color.White;
                    textBox70.BackColor = Color.White;
                    textBox69.BackColor = Color.White;
                    textBox68.BackColor = Color.White;
                    textBox67.BackColor = Color.White;
                    textBox60.BackColor = Color.White;
                    textBox64.Text = npcdata.m_aControllers[i].id.ToString();
                    textBox59.Text = getStringGBK(npcdata.m_aControllers[i].szName);
                    textBox58.Text = npcdata.m_aControllers[i].iControllerID.ToString();
                    textBox57.Text = npcdata.m_aControllers[i].iWaitTime.ToString();
                    textBox54.Text = npcdata.m_aControllers[i].iStopTime.ToString();
                    textBox61.Text = npcdata.m_aControllers[i].ActiveTime.iDay.ToString();
                    textBox62.Text = npcdata.m_aControllers[i].ActiveTime.iMouth.ToString();
                    textBox63.Text = npcdata.m_aControllers[i].ActiveTime.iYear.ToString();
                    textBox65.Text = npcdata.m_aControllers[i].ActiveTime.iHours.ToString();
                    textBox66.Text = npcdata.m_aControllers[i].ActiveTime.iMinutes.ToString();
                    comboBox3.Text = convertWeekDay(npcdata.m_aControllers[i].ActiveTime.iWeek);
                    textBox71.Text = npcdata.m_aControllers[i].StopTime.iDay.ToString();
                    textBox70.Text = npcdata.m_aControllers[i].StopTime.iMouth.ToString();
                    textBox69.Text = npcdata.m_aControllers[i].StopTime.iYear.ToString();
                    textBox68.Text = npcdata.m_aControllers[i].StopTime.iHours.ToString();
                    textBox67.Text = npcdata.m_aControllers[i].StopTime.iMinutes.ToString();
                    comboBox4.Text = convertWeekDay(npcdata.m_aControllers[i].StopTime.iWeek);
                    textBox60.Text = npcdata.m_aControllers[i].iActiveTimeRange.ToString();
                    checkBox11.Checked = npcdata.m_aControllers[i].bActived;
                    checkBox12.Checked = npcdata.m_aControllers[i].bActiveTimeInvalid;
                    checkBox13.Checked = npcdata.m_aControllers[i].bStopTimeInvalid;
                    checkBox11.CheckState = npcdata.m_aControllers[i].bActived ? CheckState.Checked : CheckState.Unchecked;
                    checkBox12.CheckState = npcdata.m_aControllers[i].bActiveTimeInvalid ? CheckState.Checked : CheckState.Unchecked;
                    checkBox13.CheckState = npcdata.m_aControllers[i].bStopTimeInvalid ? CheckState.Checked : CheckState.Unchecked;
                    updateVars = false;
                }
                else
                {
                    updateVars = true;
                    textBox64.Text = "";
                    textBox59.Text = "";
                    textBox58.Text = "";
                    textBox57.Text = "";
                    textBox54.Text = "";
                    textBox61.Text = "";
                    textBox62.Text = "";
                    textBox63.Text = "";
                    textBox65.Text = "";
                    textBox66.Text = "";
                    comboBox3.SelectedItem = null;
                    textBox71.Text = "";
                    textBox70.Text = "";
                    textBox69.Text = "";
                    textBox68.Text = "";
                    textBox67.Text = "";
                    comboBox4.SelectedItem = null;
                    textBox60.Text = "";
                    textBox64.BackColor = Color.White;
                    textBox59.BackColor = Color.White;
                    textBox58.BackColor = Color.White;
                    textBox57.BackColor = Color.White;
                    textBox54.BackColor = Color.White;
                    textBox61.BackColor = Color.White;
                    textBox62.BackColor = Color.White;
                    textBox63.BackColor = Color.White;
                    textBox65.BackColor = Color.White;
                    textBox66.BackColor = Color.White;
                    textBox71.BackColor = Color.White;
                    textBox70.BackColor = Color.White;
                    textBox69.BackColor = Color.White;
                    textBox68.BackColor = Color.White;
                    textBox67.BackColor = Color.White;
                    textBox60.BackColor = Color.White;
                    checkBox11.Checked = false;
                    checkBox12.Checked = false;
                    checkBox13.Checked = false;
                    checkBox11.CheckState = CheckState.Indeterminate;
                    checkBox12.CheckState = CheckState.Indeterminate;
                    checkBox13.CheckState = CheckState.Indeterminate;
                    updateVars = false;
                }
                updateTriggerItems();
            }
        }

        private void dataGridView3_SelectionChanged(object sender, EventArgs e)
        {
            updateTriggerData();
        }

        void updateTriggerItems()
        {
            dataGridView6.Rows.Clear();
            for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
            {
                for (int i1 = 0; i1 < npcdata.Header.iNumAIGen; i1++)
                {
                    if (npcdata.m_aAreas[i1].idCtrl == npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].id)
                    {
                        dataGridView6.Rows.Add(getnpcList1Node1Name(i1));
                        dataGridView6.Rows[dataGridView6.Rows.Count - 1].Tag = "1" + i1.ToString();
                    }
                }
                for (int i1 = 0; i1 < npcdata.Header.iNumResArea; i1++)
                {
                    if (npcdata.m_aResAreas[i1].idCtrl == npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].id)
                    {
                        dataGridView6.Rows.Add(getResListNode1Name(i1));
                        dataGridView6.Rows[dataGridView6.Rows.Count - 1].Tag = "2" + i1.ToString();
                    }
                }
                for (int i1 = 0; i1 < npcdata.Header.iNumDynObj; i1++)
                {
                    if (npcdata.m_aDynObjs[i1].idController == npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].id)
                    {
                        dataGridView6.Rows.Add(getDynObj1Name(i1));
                        dataGridView6.Rows[dataGridView6.Rows.Count - 1].Tag = "3" + i1.ToString();
                    }
                }
            }
        }

        private void textBox64_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].id = value;
                        dataGridView3.Rows[dataGridView3.SelectedRows[i].Index].SetValues(getTriggerName(dataGridView3.SelectedRows[i].Index));
                    }
                    updateTriggerItems();
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox58_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].iControllerID = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox57_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].iWaitTime = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox54_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].iStopTime = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox61_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].ActiveTime.iDay = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox62_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    if (value > 11)
                    {
                        tb.BackColor = Color.Red;
                        return;
                    }
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].ActiveTime.iMouth = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox63_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].ActiveTime.iYear = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox65_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].ActiveTime.iHours = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox66_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].ActiveTime.iMinutes = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars && comboBox3.SelectedItem != null)
            {
                for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                {
                    npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].ActiveTime.iWeek = convertWeekDayBack(comboBox3.SelectedIndex);
                }
            }
        }

        private void textBox71_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].StopTime.iDay = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox70_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    if (value > 11)
                    {
                        tb.BackColor = Color.Red;
                        return;
                    }
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].StopTime.iMouth = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox69_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].StopTime.iYear = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox68_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].StopTime.iHours = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox67_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].StopTime.iMinutes = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars && comboBox4.SelectedItem != null)
            {
                for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                {
                    npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].StopTime.iWeek = convertWeekDayBack(comboBox4.SelectedIndex);
                }
            }
        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                {
                    npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].bActived = checkBox11.Checked;
                }
            }
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                {
                    npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].bActiveTimeInvalid = checkBox12.Checked;
                }
            }
        }

        private void checkBox13_CheckedChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                {
                    npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].bStopTimeInvalid = checkBox13.Checked;
                }
            }
        }

        public uint getUniqieTriggerId()
        {
            uint result = 0;
            foreach (NPCGENFILECTRL trigger in npcdata.m_aControllers)
            {
                if (trigger.id > result) result = trigger.id;
            }
            return result + 1;
        }

        public void createTrigger_Click(object sender, EventArgs e)
        {
            npcdata.m_aControllers.Add(new NPCGENFILECTRL());
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime = new NPCCTRLTIME();
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iDay = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iHours = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iMinutes = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iMouth = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iWeek = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iYear = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime = new NPCCTRLTIME();
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iDay = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iHours = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iMinutes = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iMouth = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iWeek = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iYear = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].bActived = false;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].bActiveTimeInvalid = false;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].bStopTimeInvalid = false;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iActiveTimeRange = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iControllerID = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].id = getUniqieTriggerId();
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iStopTime = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iWaitTime = 0;
            npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].szName = getBytesGBK("Trigger №" + npcdata.m_aControllers.Count.ToString());
            dataGridView3.Rows.Add(getTriggerName(npcdata.Header.iNumNPCCtrl));
            selectRow(dataGridView3, npcdata.Header.iNumNPCCtrl);
            npcdata.Header.iNumNPCCtrl++;
            updateTriggerData();
            tabPage4.Text = getLocalizedString("Triggers") + " [" + dataGridView3.Rows.Count + "]";
            button1_Click(null, null);
        }

        private void removeTrigger_Click(object sender, EventArgs e)
        {
            dataGridView3.Enabled = false;
            foreach (DataGridViewRow row in dataGridView3.SelectedRows)
            {
                npcdata.m_aControllers.RemoveAt(row.Index);
                npcdata.Header.iNumNPCCtrl--;
                dataGridView3.Rows.RemoveAt(row.Index);
                Application.DoEvents();
            }
            for (int j = 0; j < dataGridView3.Rows.Count; j++)
            {
                dataGridView3.Rows[j].SetValues(getTriggerName(j));
            }
            dataGridView3.Enabled = true;
            updateTriggerData();
            button1_Click(null, null);
            tabPage4.Text = getLocalizedString("Triggers") + " [" + dataGridView3.Rows.Count + "]";
        }

        private void copyTrigger_Click(object sender, EventArgs e)
        {
            int start = npcdata.Header.iNumNPCCtrl;
            int i;
            for (int k = dataGridView3.SelectedRows.Count - 1; k >= 0; k--)
            {
                i = dataGridView3.SelectedRows[k].Index;
                npcdata.m_aControllers.Add(new NPCGENFILECTRL());
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime = new NPCCTRLTIME();
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iDay = npcdata.m_aControllers[i].ActiveTime.iDay;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iHours = npcdata.m_aControllers[i].ActiveTime.iHours;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iMinutes = npcdata.m_aControllers[i].ActiveTime.iMinutes;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iMouth = npcdata.m_aControllers[i].ActiveTime.iMouth;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iWeek = npcdata.m_aControllers[i].ActiveTime.iWeek;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].ActiveTime.iYear = npcdata.m_aControllers[i].ActiveTime.iYear;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime = new NPCCTRLTIME();
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iDay = npcdata.m_aControllers[i].StopTime.iDay;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iHours = npcdata.m_aControllers[i].StopTime.iHours;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iMinutes = npcdata.m_aControllers[i].StopTime.iMinutes;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iMouth = npcdata.m_aControllers[i].StopTime.iMouth;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iWeek = npcdata.m_aControllers[i].StopTime.iWeek;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].StopTime.iYear = npcdata.m_aControllers[i].StopTime.iYear;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].bActived = npcdata.m_aControllers[i].bActived;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].bActiveTimeInvalid = npcdata.m_aControllers[i].bActiveTimeInvalid;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].bStopTimeInvalid = npcdata.m_aControllers[i].bStopTimeInvalid;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iActiveTimeRange = npcdata.m_aControllers[i].iActiveTimeRange;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iControllerID = npcdata.m_aControllers[i].iControllerID;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].id = getUniqieTriggerId();
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iStopTime = npcdata.m_aControllers[i].iStopTime;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].iWaitTime = npcdata.m_aControllers[i].iWaitTime;
                npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].szName = new byte[128];
                for (int j = 0; j < 128; j++)
                {
                    npcdata.m_aControllers[npcdata.Header.iNumNPCCtrl].szName[j] = npcdata.m_aControllers[i].szName[j];
                }
                dataGridView3.Rows.Add(getTriggerName(npcdata.Header.iNumNPCCtrl));
                npcdata.Header.iNumNPCCtrl++;
            }
            selectRows(dataGridView3, start, npcdata.Header.iNumNPCCtrl - 1);
            tabPage4.Text = getLocalizedString("Triggers") + " [" + dataGridView3.Rows.Count + "]";
        }

        private void triggerMenu_Opening(object sender, CancelEventArgs e)
        {
            if (npcdataLoaded)
            {
                createTrigger.Enabled = true;
                importTrigger.Enabled = true;
                if (dataGridView3.CurrentCell != null)
                {
                    copyTrigger.Enabled = true;
                    removeTrigger.Enabled = true;
                    exportTrigger.Enabled = true;
                }
                else
                {
                    removeTrigger.Enabled = false;
                    copyTrigger.Enabled = false;
                    exportTrigger.Enabled = false;
                }
            }
            else
            {
                createTrigger.Enabled = false;
                removeTrigger.Enabled = false;
                copyTrigger.Enabled = false;
                importTrigger.Enabled = false;
                exportTrigger.Enabled = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (saveDialog)
            {
                DialogResult dr = MessageBox.Show(getLocalizedString("You have unsaved changes in this npcgen!") + "\n" + getLocalizedString("Do you want to save them?"), "Regular NPC Editor", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                switch (dr)
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        e.Cancel = true;
                        saveDialog = false;
                        saveNPCDataFile(filePath, npcdata.version);
                        break;
                    case System.Windows.Forms.DialogResult.No:
                        saveDialog = false;
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                        e.Cancel = true;
                        return;
                }
            }
            using (FileStream fs = File.Open(appPath + "//config//settings.txt", FileMode.Truncate, FileAccess.Write))
            using (BufferedStream bs = new BufferedStream(fs))
            using (StreamWriter sr = new StreamWriter(bs, Encoding.UTF8))
            {
                sr.WriteLine("language={0}", Settings["language"]);
                sr.WriteLine("clientVersion={0}", Settings["clientVersion"]);
                sr.WriteLine("processName={0}", Settings["processName"]);
                sr.WriteLine("selectedDatabase={0}", Settings["selectedDatabase"]);
            }
            Application.Exit();
        }

        private void languageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;
            foreach (ToolStripMenuItem it in languageToolStripMenuItem.DropDownItems)
            {
                if (it.Text == mi.Text)
                {
                    it.Checked = true;
                    Settings["language"] = it.Text;
                }
                else
                {
                    it.Checked = false;
                }
            }
            updateLanguage();
        }

        private void clientVersionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;
            foreach (ToolStripMenuItem it in clientVersionToolStripMenuItem.DropDownItems)
            {
                if (it.Text == mi.Text)
                {
                    it.Checked = true;
                    Settings["clientVersion"] = it.Text;
                }
                else
                {
                    it.Checked = false;
                }
            }
        }

        private bool CheckRow(string checkText, string searchText, bool ExactMatch, bool CaseSensetive, bool searchRange)
        {
            if (!CaseSensetive)
            {
                checkText = checkText.ToLower();
                searchText = searchText.ToLower();
            }
            return (ExactMatch ? checkText.Equals(searchText) : checkText.Contains(searchText)) && searchRange;
        }

        private bool CheckRow(AREA npcarea, NPCGENFILEAIGEN npc, bool ExactMatch, bool CaseSensetive, bool searchRange)
        {
            if (searchRange)
            {
                searchRange = (Distance(npcarea) <= searchRadius);
            }
            else
            {
                searchRange = true;
            }

            switch (searchType.SelectedIndex)
            {
                case 0:
                    return CheckRow(npc.dwID.ToString(), searchText.Text, ExactMatch, CaseSensetive, searchRange);
                case 1:
                    String name;
                    npcs.TryGetValue(npc.dwID, out name);
                    return name == null ? false : CheckRow(name, searchText.Text, ExactMatch, CaseSensetive, searchRange);
            }
            return false;
        }

        private bool CheckRow(NPCGENFILERESAREA npcarea, NPCGENFILERES npc, bool ExactMatch, bool CaseSensetive, bool searchRange)
        {
            if (searchRange)
            {
                searchRange = (Distance(npcarea) <= searchRadius);
            }
            else
            {
                searchRange = true;
            }

            switch (searchType.SelectedIndex)
            {
                case 0:
                    return CheckRow(npc.idTemplate.ToString(), searchText.Text, ExactMatch, CaseSensetive, searchRange);
                case 1:
                    String name;
                    mines.TryGetValue(npc.idTemplate, out name);
                    return name == null ? false : CheckRow(name, searchText.Text, ExactMatch, CaseSensetive, searchRange);
            }
            return false;
        }

        private bool CheckRow(NPCGENFILECTRL npcarea, bool ExactMatch, bool CaseSensetive, bool searchRange)
        {
            switch (searchType.SelectedIndex)
            {
                case 0:
                    return CheckRow(npcarea.id.ToString(), searchText.Text, ExactMatch, CaseSensetive, true);
                case 1:
                    return CheckRow(getStringGBK(TrimByteArray(npcarea.szName)), searchText.Text, ExactMatch, CaseSensetive, true);
            }
            return false;
        }
        private bool CheckRow(NPCGENFILEDYNOBJ npcarea, bool ExactMatch, bool CaseSensetive, bool searchRange)
        {
            if (searchRange)
            {
                searchRange = (Distance(npcarea) <= searchRadius);
            }
            else
            {
                searchRange = true;
            }

            switch (searchType.SelectedIndex)
            {
                case 0:
                    return CheckRow(npcarea.dwDynObjID.ToString(), searchText.Text, ExactMatch, CaseSensetive, searchRange);
                case 1:
                    String name;
                    dynobjs.TryGetValue(npcarea.dwDynObjID, out name);
                    return name == null ? false : CheckRow(name, searchText.Text, ExactMatch, CaseSensetive, searchRange);
            }
            return false;
        }

        private void Search(DataGridView dg)
        {
            dataGridView4.Rows.Clear();
            if (checkBox17.Checked)
            {
                for (int i = 0; i < npcdata.m_aAreas.Count; i++)
                {
                    if (npcdata.m_aAreas[i].iNumGen > 0)
                    {
                        for (int j = 0; j < npcdata.m_aAreas[i].iNumGen; j++)
                        {
                            if (CheckRow(npcdata.m_aAreas[i], npcdata.m_aAreas[i].m_aNPCGens[j], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                            {
                                dataGridView4.Rows.Add(getnpcList1Node1Name(i));
                                dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "0" + i.ToString();
                            }
                        }
                    }
                }
                for (int i = 0; i < npcdata.m_aResAreas.Count; i++)
                {
                    if (npcdata.m_aResAreas[i].iNumRes > 0)
                    {
                        for (int j = 0; j < npcdata.m_aResAreas[i].iNumRes; j++)
                        {
                            if (CheckRow(npcdata.m_aResAreas[i], npcdata.m_aResAreas[i].m_aRes[j], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                            {
                                dataGridView4.Rows.Add(getResListNode1Name(i));
                                dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "1" + i.ToString();
                            }
                        }
                    }
                }
                for (int i = 0; i < npcdata.m_aDynObjs.Count; i++)
                {
                    if (CheckRow(npcdata.m_aDynObjs[i], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                    {
                        dataGridView4.Rows.Add(getDynObj1Name(i));
                        dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "2" + i.ToString();
                    }
                }
                for (int i = 0; i < npcdata.m_aControllers.Count; i++)
                {
                    if (CheckRow(npcdata.m_aControllers[i], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                    {
                        dataGridView4.Rows.Add(getTriggerName(i));
                        dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "3" + i.ToString();
                    }
                }
                return;
            }
            if (dg == npcList)
            {
                for (int i = 0; i < npcdata.m_aAreas.Count; i++)
                {
                    if (npcdata.m_aAreas[i].iNumGen > 0)
                    {
                        for (int j = 0; j < npcdata.m_aAreas[i].iNumGen; j++)
                        {
                            if (CheckRow(npcdata.m_aAreas[i], npcdata.m_aAreas[i].m_aNPCGens[j], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                            {
                                dataGridView4.Rows.Add(getnpcList1Node1Name(i));
                                dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "0" + i.ToString();
                            }
                        }
                    }
                }
            }
            if (dg == dataGridView1)
            {
                for (int i = 0; i < npcdata.m_aResAreas.Count; i++)
                {
                    if (npcdata.m_aResAreas[i].iNumRes > 0)
                    {
                        for (int j = 0; j < npcdata.m_aResAreas[i].iNumRes; j++)
                        {
                            if (CheckRow(npcdata.m_aResAreas[i], npcdata.m_aResAreas[i].m_aRes[j], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                            {
                                dataGridView4.Rows.Add(getResListNode1Name(i));
                                dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "1" + i.ToString();
                            }
                        }
                    }
                }
            }
            if (dg == dataGridView2)
            {
                for (int i = 0; i < npcdata.m_aDynObjs.Count; i++)
                {
                    if (CheckRow(npcdata.m_aDynObjs[i], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                    {
                        dataGridView4.Rows.Add(getDynObj1Name(i));
                        dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "2" + i.ToString();
                    }
                }
            }
            if (dg == dataGridView3)
            {
                for (int i = 0; i < npcdata.m_aControllers.Count; i++)
                {
                    if (CheckRow(npcdata.m_aControllers[i], checkBox14.Checked, checkBox16.Checked, checkBox15.Checked && checkBox15.Enabled))
                    {
                        dataGridView4.Rows.Add(getTriggerName(i));
                        dataGridView4.Rows[dataGridView4.Rows.Count - 1].Tag = "3" + i.ToString();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                if (searchCategory.SelectedIndex == 0)
                {
                    Search(npcList);
                }
                if (searchCategory.SelectedIndex == 1)
                {
                    Search(dataGridView1);
                }
                if (searchCategory.SelectedIndex == 2 && npcdata.version >= 6)
                {
                    Search(dataGridView2);
                }
                if (searchCategory.SelectedIndex == 3 && npcdata.version >= 7)
                {
                    Search(dataGridView3);
                }
                tabPage5.Text = getLocalizedString("Search") + " [" + dataGridView4.Rows.Count + "]";
            }
        }

        private void dataGridView4_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string dc = dataGridView4.Rows[dataGridView4.CurrentCell.RowIndex].Tag.ToString();
            int i = Int32.Parse(dc.Substring(0, 1));
            int j = Int32.Parse(dc.Substring(1, dc.Length - 1));
            if (i == 0)
            {
                tabControl1.SelectedTab = tabPage1;
                selectRow(npcList, j);
            }
            if (i == 1)
            {
                tabControl1.SelectedTab = tabPage2;
                selectRow(dataGridView1, j);
            }
            if (i == 2 && npcdata.version >= 6)
            {
                tabControl1.SelectedTab = tabPage3;
                selectRow(dataGridView2, j);
            }
            if (i == 3 && npcdata.version >= 7)
            {
                tabControl1.SelectedTab = tabPage4;
                selectRow(dataGridView3, j);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (VersionWindow dialog = new VersionWindow())
            {
                dialog.Text = getLocalizedString("About");
                dialog.ShowDialog();
            }
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[i].Index].iType = comboBox7.SelectedIndex;
                }
            }
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            Settings["processName"] = ((ToolStripTextBox)sender).Text;
        }

        private void npcList1_SelectionChanged(object sender, EventArgs e)
        {
            updatenpcList2Data();
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iLoopType = comboBox8.SelectedIndex;
                }
            }
        }

        private void dataGridView5_SelectionChanged(object sender, EventArgs e)
        {
            updateRes2ListData();
        }

        List<Control> ignoreControlsList = new List<Control>();

        void ApplyEvent(Control ctrl)
        {
            foreach (Control c in ctrl.Controls)
            {
                if (ignoreControlsList.Contains(c)) continue;
                if (c is TextBox)
                {
                    c.TextChanged += new EventHandler(AnyControl_StateChanged);
                }
                if (c is ComboBox)
                {
                    ((ComboBox)c).SelectedIndexChanged += new EventHandler(AnyControl_StateChanged);
                }
                if (c is CheckBox)
                {
                    ((CheckBox)c).CheckedChanged += new EventHandler(AnyControl_StateChanged);
                }
                ApplyEvent(c);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                ApplyEvent(c);
            }
            /*  if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "//key.data"))
              {
                  string check;
                  using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(AppDomain.CurrentDomain.BaseDirectory + "//key.data"))
                  using (MemoryMappedViewStream mms = mmf.CreateViewStream())
                  using (BinaryReader br = new BinaryReader(mms))
                  {
                      check = br.ReadString();
                  }
                  if (LicenseKeyWindow.ComputeHash("G7-" + LicenseKeyWindow.getUniqieId() + "-6T", new MD5CryptoServiceProvider()) != check)
                  {
                      File.Delete(AppDomain.CurrentDomain.BaseDirectory + "//key.data");
                      this.Hide();
                      using (LicenseKeyWindow lk = new LicenseKeyWindow())
                      {
                          if (lk.ShowDialog(this) != DialogResult.OK)
                          {
                              Application.Exit();
                          }
                      }
                      this.Show();
                  }
              }
              else
              {
                  this.Hide();
                  using (LicenseKeyWindow lk = new LicenseKeyWindow())
                  {
                      if (lk.ShowDialog(this) != DialogResult.OK)
                      {
                          Application.Exit();
                      }
                  }
                  this.Show();
              } */
        }

        private void v1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                SaveFileDialog sd = new SaveFileDialog();
                sd.Filter = "npcgen (*.data) | *.data";
                if (sd.ShowDialog() == DialogResult.OK)
                {
                    int ver = Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString());
                    if (ver < npcdata.version)
                    {
                        MessageBox.Show(getLocalizedString("Lowering npcdata version may result some data loss"), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    converterToolStripMenuItem.Enabled = false;
                    saveNPCDataMenuItem.Enabled = false;
                    saveAsNPCDataMenuItem.Enabled = false;
                    saveNPCDataFile(sd.FileName, ver);
                    saveNPCDataMenuItem.Enabled = true;
                    saveAsNPCDataMenuItem.Enabled = true;
                    converterToolStripMenuItem.Enabled = true;
                }
                sd.Dispose();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Process[] procList = Process.GetProcessesByName(Settings["processName"]);
            if (!npcdataLoaded)
            {
                toolStripMenuItem1.Enabled = false;
                toolStripMenuItem1.Text = getLocalizedString("This feature require loading npcgen.data");
            }
            if (procList.Length > 0)
            {
                IntPtr hProcess;
                try
                {
                    hProcess = procList[0].Handle;
                }
                catch (Exception)
                {
                    return;
                }
                byte[] lpBuffer = new byte[4];
                String ver = "";
                foreach (ToolStripMenuItem it in clientVersionToolStripMenuItem.DropDownItems)
                {
                    if (it.Checked)
                    {
                        ver = it.Text;
                        break;
                    }
                }
                Offset offset = offsets[0];
                foreach (Offset off in offsets)
                {
                    if (off.name == ver)
                    {
                        offset = off;
                    }
                }
                int bytesRead = 4;
                int baseOffset = 0;
                for (int i = 0; i < offset.BaseChain.Length; i++)
                {
                    ReadProcessMemory(hProcess.ToInt32(), baseOffset + offset.BaseChain[i], lpBuffer, 4, ref bytesRead);
                    baseOffset = BitConverter.ToInt32(lpBuffer, 0);
                }
                ReadProcessMemory(hProcess.ToInt32(), baseOffset + offset.DirX, lpBuffer, 4, ref bytesRead);
                DirX = BitConverter.ToSingle(lpBuffer, 0);
                ReadProcessMemory(hProcess.ToInt32(), baseOffset + offset.DirY, lpBuffer, 4, ref bytesRead);
                DirY = BitConverter.ToSingle(lpBuffer, 0);
                ReadProcessMemory(hProcess.ToInt32(), baseOffset + offset.DirZ, lpBuffer, 4, ref bytesRead);
                DirZ = BitConverter.ToSingle(lpBuffer, 0);

                ReadProcessMemory(hProcess.ToInt32(), baseOffset + offset.PosX, lpBuffer, 4, ref bytesRead);
                PosX = BitConverter.ToSingle(lpBuffer, 0);
                ReadProcessMemory(hProcess.ToInt32(), baseOffset + offset.PosY, lpBuffer, 4, ref bytesRead);
                PosY = BitConverter.ToSingle(lpBuffer, 0);
                ReadProcessMemory(hProcess.ToInt32(), baseOffset + offset.PosZ, lpBuffer, 4, ref bytesRead);
                PosZ = BitConverter.ToSingle(lpBuffer, 0);
                toolStripMenuItem1.Enabled = true;
                if (PosX == 0 && PosY == 0 && PosZ == 0)
                {
                    toolStripMenuItem1.Enabled = false;
                    checkBox15.Enabled = false;
                    textBox14.Enabled = false;
                    label72.Enabled = false;
                    checkBox15.Text = getLocalizedString("This feature require you character to be log in");
                    toolStripMenuItem1.Text = getLocalizedString("This feature require you character to be log in");
                    return;
                }
                checkBox15.Enabled = true;
                textBox14.Enabled = true;
                label72.Enabled = true;
                switch (searchCategory.SelectedIndex)
                {
                    case 0:
                        checkBox15.Text = getLocalizedString("Find $TAB$ only in selected radius across character").Replace("$TAB$", getLocalizedString("NPCs")); break;
                    case 1:
                        checkBox15.Text = getLocalizedString("Find $TAB$ only in selected radius across character").Replace("$TAB$", getLocalizedString("Resources").ToLower()); break;
                    case 2:
                        checkBox15.Text = getLocalizedString("Find $TAB$ only in selected radius across character").Replace("$TAB$", getLocalizedString("Dynamic objects").ToLower()); break;
                }
                if (tabControl1.SelectedTab == tabPage1)
                {
                    if (npcList.CurrentCell == null)
                    {
                        toolStripMenuItem1.Enabled = false;
                        toolStripMenuItem1.Text = getLocalizedString("This feature require selecting any NPC");
                    }
                    else
                    {
                        toolStripMenuItem1.Text = getLocalizedString("Copy $DATA$ with direction from client to current selected NPC").Replace("$DATA$", "(" + PosX + "; " + PosY + "; " + PosZ + ")");
                    }
                    return;
                }
                if (tabControl1.SelectedTab == tabPage2)
                {
                    if (dataGridView1.CurrentCell == null)
                    {
                        toolStripMenuItem1.Enabled = false;
                        toolStripMenuItem1.Text = getLocalizedString("This feature require selecting any resource");
                    }
                    else
                    {
                        toolStripMenuItem1.Text = getLocalizedString("Copy $DATA$ with direction from client to current selected resource").Replace("$DATA$", "(" + PosX + "; " + PosY + "; " + PosZ + ")");
                    }
                    return;
                }
                if (tabControl1.SelectedTab == tabPage3)
                {
                    if (dataGridView2.CurrentCell == null)
                    {
                        toolStripMenuItem1.Enabled = false;
                        toolStripMenuItem1.Text = getLocalizedString("This feature require selecting any dynamic object");
                    }
                    else
                    {
                        toolStripMenuItem1.Text = getLocalizedString("Copy $DATA$ with direction from client to current selected dynamic object").Replace("$DATA$", "(" + PosX + "; " + PosY + "; " + PosZ + ")");
                    }
                    return;
                }
            }
            else
            {
                toolStripMenuItem1.Enabled = false;
                toolStripMenuItem1.Text = getLocalizedString("This feature require running client");
                checkBox15.Enabled = false;
                checkBox15.Text = getLocalizedString("This feature require running client");
                textBox14.Enabled = false;
                label72.Enabled = false;
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage1)
            {

                textBox2.Text = PosX.ToString();
                textBox3.Text = PosY.ToString();
                textBox4.Text = PosZ.ToString();
                textBox5.Text = DirX.ToString();
                textBox6.Text = DirY.ToString();
                textBox7.Text = DirZ.ToString();
            }
            if (tabControl1.SelectedTab == tabPage2)
            {
                textBox31.Text = PosX.ToString();
                textBox32.Text = PosY.ToString();
                textBox33.Text = PosZ.ToString();
            }
            if (tabControl1.SelectedTab == tabPage3)
            {
                textBox43.Text = PosX.ToString();
                textBox44.Text = PosY.ToString();
                textBox45.Text = PosZ.ToString();
            }
        }

        double searchRadius = 2500.0;

        private void textBox14_TextChanged_1(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            double value;
            tb.BackColor = Color.White;
            if (double.TryParse(tb.Text, out value))
            {
                searchRadius = value * value;
            }
            else
            {
                tb.BackColor = Color.Red;
            }
        }

        public double Distance(AREA obj)
        {
            double x = Convert.ToDouble(obj.vPos[0]);
            double y = Convert.ToDouble(obj.vPos[1]);
            double z = Convert.ToDouble(obj.vPos[2]);
            return Math.Pow((PosX - x), 2) + Math.Pow((PosY - y), 2) + Math.Pow((PosZ - z), 2);
        }

        public double Distance(NPCGENFILEDYNOBJ obj)
        {
            double x = Convert.ToDouble(obj.vPos[0]);
            double y = Convert.ToDouble(obj.vPos[1]);
            double z = Convert.ToDouble(obj.vPos[2]);
            return Math.Pow((PosX - x), 2) + Math.Pow((PosY - y), 2) + Math.Pow((PosZ - z), 2);
        }

        public double Distance(NPCGENFILERESAREA obj)
        {
            double x = Convert.ToDouble(obj.vPos[0]);
            double y = Convert.ToDouble(obj.vPos[1]);
            double z = Convert.ToDouble(obj.vPos[2]);
            return Math.Pow((PosX - x), 2) + Math.Pow((PosY - y), 2) + Math.Pow((PosZ - z), 2);
        }

        private void openElementsdataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setOpenSaveControlStatus(false);
            using (var openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "elements (*.data) | *.data";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath1 = openFileDialog.FileName;
                    openElementsDataFile(filePath1);
                    for (int i = 0; i < npcdata.Header.iNumAIGen; i++)
                    {
                        npcList.Rows[i].SetValues(getnpcList1Node1Name(i));
                        progressBar1.Value++;
                    }
                    if (npcList.CurrentCell != null)
                    {
                        selectRow(npcList, npcList.CurrentCell.RowIndex);
                    }
                    for (int i = 0; i < npcdata.Header.iNumResArea; i++)
                    {
                        dataGridView1.Rows[i].SetValues(getResListNode1Name(i));
                        progressBar1.Value++;
                    }
                    if (dataGridView1.CurrentCell != null)
                    {
                        selectRow(dataGridView1, dataGridView1.CurrentCell.RowIndex);
                    }
                }
            }
            setOpenSaveControlStatus(true);
        }

        public int getElementsTotalCount()
        {
            return npcList.Rows.Count + dataGridView1.Rows.Count;
        }

        void openElementsDataFile(string filePath)
        {
            try
            {
                eLC = new eListCollection(filePath, progressBar1);
                npcs.Clear();
                mines.Clear();
                uint id;
                int id2;
                string s;
                for (int i = 0; i < eLC.Lists[57].elementValues.Length; i++)
                {
                    id = uint.Parse(eLC.GetValue(57, i, 0));
                    if (npcs.TryGetValue(id, out s))
                    {
                        MessageBox.Show(getLocalizedString("Error in elements.data") + ":\n" + getLocalizedString("NPC with id $ID$ and name $NAME$ has same id as NPC with name $NAME2$").Replace("$ID$", id.ToString()).Replace("$NAME$", eLC.GetValue(57, i, 1)).Replace("$NAME2$", s), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        npcs.Add(id, eLC.GetValue(57, i, 1));
                    }
                    Application.DoEvents();
                }
                for (int i = 0; i < eLC.Lists[38].elementValues.Length; i++)
                {
                    id = uint.Parse(eLC.GetValue(38, i, 0));
                    if (npcs.TryGetValue(id, out s))
                    {
                        MessageBox.Show(getLocalizedString("Error in elements.data") + ":\n" + getLocalizedString("NPC with id $ID$ and name $NAME$ has same id as NPC with name $NAME2$").Replace("$ID$", id.ToString()).Replace("$NAME$", eLC.GetValue(38, i, 2)).Replace("$NAME2$", s), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        npcs.Add(id, eLC.GetValue(38, i, 2));
                    }
                    Application.DoEvents();
                }
                int mineEssenceIndex = 79;
                for (int i = 0; i < eLC.Lists.Length; i++)
                {
                    if (eLC.Lists[i].listName.Contains("MINE_ESSENCE"))
                    {
                        mineEssenceIndex = i;
                    }
                }
                for (int i = 0; i < eLC.Lists[mineEssenceIndex].elementValues.Length; i++)
                {
                    id2 = int.Parse(eLC.GetValue(mineEssenceIndex, i, 0));
                    if (mines.TryGetValue(id2, out s))
                    {
                        MessageBox.Show(getLocalizedString("Error in elements.data") + ":\n" + getLocalizedString("Resource with id $ID$ and name $NAME$ has same id as resource with name $NAME2$").Replace("$ID$", id2.ToString()).Replace("$NAME$", eLC.GetValue(79, i, 2)).Replace("$NAME2$", s), "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        mines.Add(id2, eLC.GetValue(mineEssenceIndex, i, 2));
                    }
                    Application.DoEvents();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error loading this elements.data.\n" + exc.Message, "Regular NPC Editor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // TODO: Добавить редактор path.sev

        private void russianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem ti in databaseToolStripMenuItem.DropDownItems)
            {
                ti.Checked = false;
            }
            ((ToolStripMenuItem)sender).Checked = true;
            Settings["selectedDatabase"] = ((ToolStripMenuItem)sender).Tag.ToString();
        }

        private void textBox18_TextChanged_1(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                int value;
                tb.BackColor = Color.White;
                if (int.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iRefresh = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox27_TextChanged_1(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView5.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView5.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[i].Index].dwRefreshTime = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void dynamicobjectsdataEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dynObjsWindow = new DynamicObjectWindow();
            dynObjsWindow.Show();
        }

        private void textBox60_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                uint value;
                tb.BackColor = Color.White;
                if (uint.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView3.SelectedRows.Count; i++)
                    {
                        npcdata.m_aControllers[dataGridView3.SelectedRows[i].Index].iActiveTimeRange = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void dataGridView6_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            string dc = dg.Rows[dg.CurrentCell.RowIndex].Tag.ToString();
            string i = dc.Substring(0, 1);
            int j = Convert.ToInt32(dc.Substring(1, dc.Length - 1));
            switch (i)
            {
                case "1":
                    tabControl1.SelectedTab = tabPage1;
                    selectRow(npcList, j);
                    break;
                case "2":
                    tabControl1.SelectedTab = tabPage2;
                    selectRow(dataGridView1, j);
                    break;
                case "3":
                    tabControl1.SelectedTab = tabPage3;
                    selectRow(dataGridView2, j);
                    break;
            }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                for (int i = 0; i < npcList.SelectedRows.Count; i++)
                {
                    npcdata.m_aAreas[npcList.SelectedRows[i].Index].iGroupType = comboBox5.SelectedIndex;
                }
            }
        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].fOffsetWater = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox22_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].fOffsetTrn = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void textBox33_TextChanged(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0 && !updateVars)
            {
                TextBox tb = (TextBox)sender;
                float value;
                tb.BackColor = Color.White;
                if (float.TryParse(tb.Text, out value))
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].vPos[2] = value;
                    }
                }
                else
                {
                    tb.BackColor = Color.Red;
                }
            }
        }

        private void exportToCoordsdatatxtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                using (ExportWindow ew = new ExportWindow())
                {
                    ew.ShowDialog();
                }
            }
        }

        enum ExportType { NPCGroup, Npc, ResGroup, Res, DynObj, Trigger };

        private void exportNPCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList1.SelectedRows.Count > 0)
            {
                using (SaveFileDialog sf = new SaveFileDialog())
                {
                    sf.Filter = "Export files (*.export) | *.export";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryWriter bw = new BinaryWriter(new FileStream(sf.FileName, FileMode.OpenOrCreate)))
                        {
                            bw.BaseStream.Position = 0;
                            bw.Write(npcdata.version);
                            bw.Write((int)ExportType.Npc);
                            bw.Write(npcList1.SelectedRows.Count);
                            for (int i = 0; i < npcList1.SelectedRows.Count; i++)
                            {
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwID);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwNum);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iRefresh);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwDiedTimes);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwAggressive);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].fOffsetWater);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].fOffsetTrn);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwFaction);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwFacHelper);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].dwFacAccept);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bNeedHelp);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bDefFaction);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bDefFacHelper);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].bDefFacAccept);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iPathID);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iLoopType);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iSpeedFlag);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iDeadTime);
                                if (npcdata.version >= 11)
                                {
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcList1.SelectedRows[i].Index].iRefreshLower);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void importNpc1_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                using (OpenFileDialog of = new OpenFileDialog())
                {
                    of.Filter = "Export files (*.export) | *.export";
                    if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(of.FileName, FileMode.Open)))
                        {
                            try
                            {
                                int version = br.ReadInt32();
                                if (br.ReadInt32() == (int)ExportType.Npc)
                                {
                                    int count = br.ReadInt32();
                                    for (int i = 0; i < count; i++)
                                    {
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Add(new NPCGENFILEAIGEN());
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].dwID = br.ReadUInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].dwNum = br.ReadUInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].iRefresh = br.ReadInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].dwDiedTimes = br.ReadUInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].dwAggressive = br.ReadUInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].fOffsetWater = br.ReadSingle();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].fOffsetTrn = br.ReadSingle();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].dwFaction = br.ReadUInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].dwFacHelper = br.ReadUInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].dwFacAccept = br.ReadUInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].bNeedHelp = br.ReadBoolean();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].bDefFaction = br.ReadBoolean();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].bDefFacHelper = br.ReadBoolean();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].bDefFacAccept = br.ReadBoolean();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].iPathID = br.ReadInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].iLoopType = br.ReadInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].iSpeedFlag = br.ReadInt32();
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].iDeadTime = br.ReadInt32();
                                        if (version >= 11)
                                        {
                                            npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens[npcdata.m_aAreas[npcList.SelectedRows[0].Index].m_aNPCGens.Count - 1].iRefreshLower = br.ReadInt32();
                                        }
                                        npcdata.m_aAreas[npcList.SelectedRows[0].Index].iNumGen++;
                                    }
                                    updatenpcList1Data();
                                    selectRows(npcList1, npcList1.Rows.Count - count, npcList1.Rows.Count - 1);
                                    npcList.Rows[npcList.SelectedRows[0].Index].SetValues(getnpcList1Node1Name(npcList.SelectedRows[0].Index));
                                }
                                else
                                {
                                    MessageBox.Show(this, getLocalizedString("This export file is invalid for this import operation!"), getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show(this, getLocalizedString("This export file is corrupted or invalid!") + "\n" + exc.Message, getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        private void exportNpc_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded && npcList.SelectedRows.Count > 0)
            {
                using (SaveFileDialog sf = new SaveFileDialog())
                {
                    sf.Filter = "Export files (*.export) | *.export";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryWriter bw = new BinaryWriter(new FileStream(sf.FileName, FileMode.OpenOrCreate)))
                        {
                            bw.BaseStream.Position = 0;
                            bw.Write(npcdata.version);
                            bw.Write((int)ExportType.NPCGroup);
                            bw.Write(npcList.SelectedRows.Count);
                            for (int i = 0; i < npcList.SelectedRows.Count; i++)
                            {
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].iType);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].iNumGen);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vPos[0]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vPos[1]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vPos[2]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vDir[0]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vDir[1]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vDir[2]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vExts[0]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vExts[1]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].vExts[2]);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].iNPCType);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].iGroupType);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].bInitGen);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].bAutoRevive);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].bValidOnce);
                                bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].dwGenID);
                                if (npcdata.version >= 7)
                                {
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].idCtrl);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].iLifeTime);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].iMaxNum);
                                }
                                for (int j = 0; j < npcdata.m_aAreas[npcList.SelectedRows[i].Index].iNumGen; j++)
                                {
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].dwID);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].dwNum);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].iRefresh);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].dwDiedTimes);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].dwAggressive);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].fOffsetWater);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].fOffsetTrn);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].dwFaction);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].dwFacHelper);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].dwFacAccept);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].bNeedHelp);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].bDefFaction);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].bDefFacHelper);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].bDefFacAccept);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].iPathID);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].iLoopType);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].iSpeedFlag);
                                    bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].iDeadTime);
                                    if (npcdata.version >= 11)
                                    {
                                        bw.Write(npcdata.m_aAreas[npcList.SelectedRows[i].Index].m_aNPCGens[j].iRefreshLower);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void importNpc_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                using (OpenFileDialog of = new OpenFileDialog())
                {
                    of.Filter = "Export files (*.export) | *.export";
                    if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(of.FileName, FileMode.Open)))
                        {
                            try
                            {
                                int version = br.ReadInt32();
                                if (br.ReadInt32() == (int)ExportType.NPCGroup)
                                {
                                    int count = br.ReadInt32();
                                    for (int i = 0; i < count; i++)
                                    {
                                        npcdata.m_aAreas.Add(new AREA());
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].iType = br.ReadInt32();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].iNumGen = br.ReadInt32();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vPos = new float[3];
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vPos[0] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vPos[1] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vPos[2] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vDir = new float[3];
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vDir[0] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vDir[1] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vDir[2] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vExts = new float[3];
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vExts[0] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vExts[1] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].vExts[2] = br.ReadSingle();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].iNPCType = br.ReadInt32();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].iGroupType = br.ReadInt32();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].bInitGen = br.ReadBoolean();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].bAutoRevive = br.ReadBoolean();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].bValidOnce = br.ReadBoolean();
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].dwGenID = br.ReadUInt32();
                                        if (version >= 7)
                                        {
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].idCtrl = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].iLifeTime = br.ReadInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].iMaxNum = br.ReadInt32();
                                        }
                                        npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens = new List<NPCGENFILEAIGEN>();
                                        for (int j = 0; j < npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].iNumGen; j++)
                                        {
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens.Add(new NPCGENFILEAIGEN());
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].dwID = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].dwNum = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].iRefresh = br.ReadInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].dwDiedTimes = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].dwAggressive = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].fOffsetWater = br.ReadSingle();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].fOffsetTrn = br.ReadSingle();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].dwFaction = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].dwFacHelper = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].dwFacAccept = br.ReadUInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].bNeedHelp = br.ReadBoolean();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].bDefFaction = br.ReadBoolean();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].bDefFacHelper = br.ReadBoolean();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].bDefFacAccept = br.ReadBoolean();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].iPathID = br.ReadInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].iLoopType = br.ReadInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].iSpeedFlag = br.ReadInt32();
                                            npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].iDeadTime = br.ReadInt32();
                                            if (version >= 11)
                                            {
                                                npcdata.m_aAreas[npcdata.m_aAreas.Count - 1].m_aNPCGens[j].iRefreshLower = br.ReadInt32();
                                            }
                                        }
                                        Application.DoEvents();
                                        npcdata.Header.iNumAIGen++;
                                        npcList.Rows.Add(getnpcList1Node1Name(npcdata.m_aAreas.Count - 1));
                                    }
                                    selectRows(npcList, npcdata.m_aAreas.Count - count, npcdata.m_aAreas.Count - 1);
                                }
                                else
                                {
                                    MessageBox.Show(this, getLocalizedString("This export file is invalid for this import operation!"), getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show(this, getLocalizedString("This export file is corrupted or invalid!") + "\n" + exc.Message, getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        private void exportRes_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView1.SelectedRows.Count > 0)
            {
                using (SaveFileDialog sf = new SaveFileDialog())
                {
                    sf.Filter = "Export files (*.export) | *.export";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryWriter bw = new BinaryWriter(new FileStream(sf.FileName, FileMode.OpenOrCreate)))
                        {
                            bw.BaseStream.Position = 0;
                            bw.Write(npcdata.version);
                            bw.Write((int)ExportType.ResGroup);
                            bw.Write(dataGridView1.SelectedRows.Count);
                            for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                            {
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].vPos[0]);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].vPos[1]);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].vPos[2]);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].fExtX);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].fExtZ);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].iNumRes);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].bInitGen);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].bAutoRevive);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].bValidOnce);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].dwGenID);
                                if (npcdata.version >= 6)
                                {
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].dir[0]);
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].dir[1]);
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].rad);
                                }
                                if (npcdata.version >= 7)
                                {
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].idCtrl);
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].iMaxNum);
                                }
                                for (int j = 0; j < npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].iNumRes; j++)
                                {
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].m_aRes[j].iResType);
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].m_aRes[j].idTemplate);
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].m_aRes[j].dwRefreshTime);
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].m_aRes[j].dwNumber);
                                    bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[i].Index].m_aRes[j].fHeiOff);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void importRes_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                using (OpenFileDialog of = new OpenFileDialog())
                {
                    of.Filter = "Export files (*.export) | *.export";
                    if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(of.FileName, FileMode.Open)))
                        {
                            try
                            {
                                int version = br.ReadInt32();
                                if (br.ReadInt32() == (int)ExportType.ResGroup)
                                {
                                    int count = br.ReadInt32();
                                    for (int i = 0; i < count; i++)
                                    {
                                        npcdata.m_aResAreas.Add(new NPCGENFILERESAREA());
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].vPos = new float[3];
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].vPos[0] = br.ReadSingle();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].vPos[1] = br.ReadSingle();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].vPos[2] = br.ReadSingle();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].fExtX = br.ReadSingle();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].fExtZ = br.ReadSingle();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].iNumRes = br.ReadInt32();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].bInitGen = br.ReadBoolean();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].bAutoRevive = br.ReadBoolean();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].bValidOnce = br.ReadBoolean();
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].dwGenID = br.ReadUInt32();
                                        if (version >= 6)
                                        {
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].dir = new byte[2];
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].dir[0] = br.ReadByte();
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].dir[1] = br.ReadByte();
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].rad = br.ReadByte();
                                        }
                                        if (version >= 7)
                                        {
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].idCtrl = br.ReadUInt32();
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].iMaxNum = br.ReadInt32();
                                        }
                                        npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].m_aRes = new List<NPCGENFILERES>();
                                        for (int j = 0; j < npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].iNumRes; j++)
                                        {
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].m_aRes.Add(new NPCGENFILERES());
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].m_aRes[j].iResType = br.ReadInt32();
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].m_aRes[j].idTemplate = br.ReadInt32();
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].m_aRes[j].dwRefreshTime = br.ReadUInt32();
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].m_aRes[j].dwNumber = br.ReadUInt32();
                                            npcdata.m_aResAreas[npcdata.m_aResAreas.Count - 1].m_aRes[j].fHeiOff = br.ReadSingle();
                                        }
                                        Application.DoEvents();
                                        npcdata.Header.iNumResArea++;
                                        dataGridView1.Rows.Add(getResListNode1Name(npcdata.m_aResAreas.Count - 1));
                                    }
                                    selectRows(dataGridView1, npcdata.m_aResAreas.Count - count, npcdata.m_aResAreas.Count - 1);
                                }
                                else
                                {
                                    MessageBox.Show(this, getLocalizedString("This export file is invalid for this import operation!"), getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show(this, getLocalizedString("This export file is corrupted or invalid!") + "\n" + exc.Message, getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        private void exportRes1_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView5.SelectedRows.Count > 0)
            {
                using (SaveFileDialog sf = new SaveFileDialog())
                {
                    sf.Filter = "Export files (*.export) | *.export";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryWriter bw = new BinaryWriter(new FileStream(sf.FileName, FileMode.OpenOrCreate)))
                        {
                            bw.BaseStream.Position = 0;
                            bw.Write(npcdata.version);
                            bw.Write((int)ExportType.Res);
                            bw.Write(dataGridView5.SelectedRows.Count);
                            for (int j = 0; j < dataGridView5.SelectedRows.Count; j++)
                            {
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[j].Index].iResType);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[j].Index].idTemplate);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[j].Index].dwRefreshTime);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[j].Index].dwNumber);
                                bw.Write(npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[dataGridView5.SelectedRows[j].Index].fHeiOff);
                            }
                        }
                    }
                }
            }
        }

        private void importRes1_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                using (OpenFileDialog of = new OpenFileDialog())
                {
                    of.Filter = "Export files (*.export) | *.export";
                    if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(of.FileName, FileMode.Open)))
                        {
                            try
                            {
                                int version = br.ReadInt32();
                                if (br.ReadInt32() == (int)ExportType.Res)
                                {
                                    int count = br.ReadInt32();
                                    for (int j = 0; j < count; j++)
                                    {
                                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes.Add(new NPCGENFILERES());
                                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes.Count - 1].iResType = br.ReadInt32();
                                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes.Count - 1].idTemplate = br.ReadInt32();
                                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes.Count - 1].dwRefreshTime = br.ReadUInt32();
                                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes.Count - 1].dwNumber = br.ReadUInt32();
                                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes[npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].m_aRes.Count - 1].fHeiOff = br.ReadSingle();
                                        npcdata.m_aResAreas[dataGridView1.SelectedRows[0].Index].iNumRes++;
                                    }
                                    updateRes2ListData();
                                    selectRows(dataGridView1, npcdata.m_aResAreas.Count - count, npcdata.m_aResAreas.Count - 1);
                                }
                                else
                                {
                                    MessageBox.Show(this, getLocalizedString("This export file is invalid for this import operation!"), getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show(this, getLocalizedString("This export file is corrupted or invalid!") + "\n" + exc.Message, getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        private void exportDynObj_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView2.SelectedRows.Count > 0)
            {
                using (SaveFileDialog sf = new SaveFileDialog())
                {
                    sf.Filter = "Export files (*.export) | *.export";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryWriter bw = new BinaryWriter(new FileStream(sf.FileName, FileMode.OpenOrCreate)))
                        {
                            bw.BaseStream.Position = 0;
                            bw.Write(npcdata.version);
                            bw.Write((int)ExportType.DynObj);
                            bw.Write(dataGridView2.SelectedRows.Count);
                            for (int j = 0; j < dataGridView2.SelectedRows.Count; j++)
                            {
                                bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].dwDynObjID);
                                bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].vPos[0]);
                                bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].vPos[1]);
                                bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].vPos[2]);
                                bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].dir[0]);
                                bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].dir[1]);
                                bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].rad);
                                if (npcdata.version >= 9)
                                {
                                    bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].idController);
                                }
                                if (npcdata.version >= 10)
                                {
                                    bw.Write(npcdata.m_aDynObjs[dataGridView2.SelectedRows[j].Index].scale);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void importDynObj_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                using (OpenFileDialog of = new OpenFileDialog())
                {
                    of.Filter = "Export files (*.export) | *.export";
                    if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(of.FileName, FileMode.Open)))
                        {
                            try
                            {
                                int version = br.ReadInt32();
                                if (br.ReadInt32() == (int)ExportType.DynObj)
                                {
                                    int count = br.ReadInt32();
                                    for (int j = 0; j < count; j++)
                                    {
                                        npcdata.m_aDynObjs.Add(new NPCGENFILEDYNOBJ());
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].dwDynObjID = br.ReadUInt32();
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].vPos = new float[3];
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].vPos[0] = br.ReadSingle();
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].vPos[1] = br.ReadSingle();
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].vPos[2] = br.ReadSingle();
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].dir = new byte[2];
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].dir[0] = br.ReadByte();
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].dir[1] = br.ReadByte();
                                        npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].rad = br.ReadByte();
                                        if (version >= 9)
                                        {
                                            npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].idController = br.ReadUInt32();
                                        }
                                        if (version >= 10)
                                        {
                                            npcdata.m_aDynObjs[npcdata.m_aDynObjs.Count - 1].scale = br.ReadByte();
                                        }
                                        Application.DoEvents();
                                        dataGridView2.Rows.Add(getDynObj1Name(npcdata.m_aDynObjs.Count - 1));
                                        npcdata.Header.iNumDynObj++;
                                    }
                                    selectRows(dataGridView2, npcdata.m_aDynObjs.Count - count, npcdata.m_aDynObjs.Count - 1);
                                }
                                else
                                {
                                    MessageBox.Show(this, getLocalizedString("This export file is invalid for this import operation!"), getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show(this, getLocalizedString("This export file is corrupted or invalid!") + "\n" + exc.Message, getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        private void exportTrigger_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded && dataGridView3.SelectedRows.Count > 0)
            {
                using (SaveFileDialog sf = new SaveFileDialog())
                {
                    sf.Filter = "Export files (*.export) | *.export";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryWriter bw = new BinaryWriter(new FileStream(sf.FileName, FileMode.OpenOrCreate)))
                        {
                            bw.BaseStream.Position = 0;
                            bw.Write(npcdata.version);
                            bw.Write((int)ExportType.Trigger);
                            bw.Write(dataGridView3.SelectedRows.Count);
                            for (int j = 0; j < dataGridView3.SelectedRows.Count; j++)
                            {
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].id);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].iControllerID);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].szName);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].bActived);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].iWaitTime);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].iStopTime);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].bActiveTimeInvalid);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].bStopTimeInvalid);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].ActiveTime.iYear);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].ActiveTime.iMouth);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].ActiveTime.iWeek);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].ActiveTime.iDay);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].ActiveTime.iHours);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].ActiveTime.iMinutes);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].StopTime.iYear);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].StopTime.iMouth);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].StopTime.iWeek);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].StopTime.iDay);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].StopTime.iHours);
                                bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].StopTime.iMinutes);
                                if (npcdata.version >= 8)
                                {
                                    bw.Write(npcdata.m_aControllers[dataGridView3.SelectedRows[j].Index].iActiveTimeRange);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void importTrigger_Click(object sender, EventArgs e)
        {
            if (npcdataLoaded)
            {
                using (OpenFileDialog of = new OpenFileDialog())
                {
                    of.Filter = "Export files (*.export) | *.export";
                    if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(of.FileName, FileMode.Open)))
                        {
                            try
                            {
                                int version = br.ReadInt32();
                                if (br.ReadInt32() == (int)ExportType.Trigger)
                                {
                                    int count = br.ReadInt32();
                                    for (int j = 0; j < count; j++)
                                    {
                                        npcdata.m_aControllers.Add(new NPCGENFILECTRL());
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].id = br.ReadUInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].iControllerID = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].szName = new byte[128];
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].szName = br.ReadBytes(128);
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].bActived = br.ReadBoolean();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].iWaitTime = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].iStopTime = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].bActiveTimeInvalid = br.ReadBoolean();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].bStopTimeInvalid = br.ReadBoolean();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].ActiveTime = new NPCCTRLTIME();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].ActiveTime.iYear = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].ActiveTime.iMouth = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].ActiveTime.iWeek = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].ActiveTime.iDay = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].ActiveTime.iHours = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].ActiveTime.iMinutes = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].StopTime = new NPCCTRLTIME();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].StopTime.iYear = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].StopTime.iMouth = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].StopTime.iWeek = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].StopTime.iDay = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].StopTime.iHours = br.ReadInt32();
                                        npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].StopTime.iMinutes = br.ReadInt32();
                                        if (version >= 8)
                                        {
                                            npcdata.m_aControllers[npcdata.m_aControllers.Count - 1].iActiveTimeRange = br.ReadUInt32();
                                        }
                                        Application.DoEvents();
                                        dataGridView3.Rows.Add(getTriggerName(npcdata.m_aControllers.Count - 1));
                                        npcdata.Header.iNumNPCCtrl++;
                                    }
                                    selectRows(dataGridView3, npcdata.m_aControllers.Count - count, npcdata.m_aControllers.Count - 1);
                                }
                                else
                                {
                                    MessageBox.Show(this, getLocalizedString("This export file is invalid for this import operation!"), getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception exc)
                            {
                                MessageBox.Show(this, getLocalizedString("This export file is corrupted or invalid!") + "\n" + exc.Message, getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }
    }
}

