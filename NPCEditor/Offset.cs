﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPCEditor
{
    class Offset
    {
        public int[] BaseChain;
        public int PosX;
        public int PosY;
        public int PosZ;
        public int DirX;
        public int DirY;
        public int DirZ;
        public String name;
    }
}
