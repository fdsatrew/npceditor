﻿namespace NPCEditor
{
    partial class DynamicObjectWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DynamicObjectWindow));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dynObjsList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dynObjMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.copyDynObj = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAllUnknownIdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dynObjsList)).BeginInit();
            this.dynObjMenu.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.dynObjsList, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 452F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(753, 452);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dynObjsList
            // 
            this.dynObjsList.AllowUserToAddRows = false;
            this.dynObjsList.AllowUserToDeleteRows = false;
            this.dynObjsList.AllowUserToResizeColumns = false;
            this.dynObjsList.AllowUserToResizeRows = false;
            this.dynObjsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dynObjsList.ColumnHeadersVisible = false;
            this.dynObjsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column1,
            this.Path});
            this.dynObjsList.ContextMenuStrip = this.dynObjMenu;
            this.dynObjsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dynObjsList.Location = new System.Drawing.Point(3, 3);
            this.dynObjsList.MultiSelect = false;
            this.dynObjsList.Name = "dynObjsList";
            this.dynObjsList.RowHeadersVisible = false;
            this.dynObjsList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dynObjsList.ShowCellErrors = false;
            this.dynObjsList.ShowCellToolTips = false;
            this.dynObjsList.ShowEditingIcon = false;
            this.dynObjsList.ShowRowErrors = false;
            this.dynObjsList.Size = new System.Drawing.Size(747, 446);
            this.dynObjsList.TabIndex = 7;
            this.dynObjsList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dynObjsList_CellEndEdit);
            this.dynObjsList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dynObjsList_CellMouseDoubleClick);
            this.dynObjsList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDown);
            this.dynObjsList.SelectionChanged += new System.EventHandler(this.dynObjsList_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 2;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 2;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "Name";
            this.Column1.MinimumWidth = 2;
            this.Column1.Name = "Column1";
            this.Column1.Width = 2;
            // 
            // Path
            // 
            this.Path.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Path.HeaderText = "Path";
            this.Path.Name = "Path";
            // 
            // dynObjMenu
            // 
            this.dynObjMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createDynObj,
            this.removeDynObj,
            this.copyDynObj});
            this.dynObjMenu.Name = "contextMenuStrip2";
            this.dynObjMenu.Size = new System.Drawing.Size(203, 70);
            this.dynObjMenu.Opening += new System.ComponentModel.CancelEventHandler(this.dynObjMenu_Opening);
            // 
            // createDynObj
            // 
            this.createDynObj.Image = global::NPCEditor.Properties.Resources.add;
            this.createDynObj.Name = "createDynObj";
            this.createDynObj.Size = new System.Drawing.Size(202, 22);
            this.createDynObj.Text = "Create dynamic object";
            this.createDynObj.Click += new System.EventHandler(this.createDynObj_Click);
            // 
            // removeDynObj
            // 
            this.removeDynObj.Image = global::NPCEditor.Properties.Resources.remove;
            this.removeDynObj.Name = "removeDynObj";
            this.removeDynObj.Size = new System.Drawing.Size(202, 22);
            this.removeDynObj.Text = "Remove dynamic object";
            this.removeDynObj.Click += new System.EventHandler(this.removeDynObj_Click);
            // 
            // copyDynObj
            // 
            this.copyDynObj.Image = global::NPCEditor.Properties.Resources.clone;
            this.copyDynObj.Name = "copyDynObj";
            this.copyDynObj.Size = new System.Drawing.Size(202, 22);
            this.copyDynObj.Text = "Clone dynamic object";
            this.copyDynObj.Click += new System.EventHandler(this.copyDynObj_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(753, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.open;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.save;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.save;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportAllUnknownIdsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // exportAllUnknownIdsToolStripMenuItem
            // 
            this.exportAllUnknownIdsToolStripMenuItem.Image = global::NPCEditor.Properties.Resources.dynobjs;
            this.exportAllUnknownIdsToolStripMenuItem.Name = "exportAllUnknownIdsToolStripMenuItem";
            this.exportAllUnknownIdsToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.exportAllUnknownIdsToolStripMenuItem.Text = "Export all unknown dynamic objects";
            this.exportAllUnknownIdsToolStripMenuItem.Click += new System.EventHandler(this.exportAllUnknownIdsToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // DynamicObjectWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 476);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "DynamicObjectWindow";
            this.Text = "Dynamic objects editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DynamicObjectWindow_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dynObjsList)).EndInit();
            this.dynObjMenu.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ContextMenuStrip dynObjMenu;
        private System.Windows.Forms.ToolStripMenuItem createDynObj;
        private System.Windows.Forms.ToolStripMenuItem removeDynObj;
        private System.Windows.Forms.ToolStripMenuItem copyDynObj;
        public System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem exportAllUnknownIdsToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Path;
        public System.Windows.Forms.DataGridView dynObjsList;
    }
}