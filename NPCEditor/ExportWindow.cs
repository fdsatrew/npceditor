﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace NPCEditor
{
    public partial class ExportWindow : Form
    {
        string filename;

        public ExportWindow()
        {
            InitializeComponent();
            button1.Text = MainWindow.instance.getLocalizedString("Open coord_data.txt");
            label2.Text = MainWindow.instance.getLocalizedString("Map name");
            checkBox1.Text = MainWindow.instance.getLocalizedString("Export only selected NPCs");
            checkBox2.Text = MainWindow.instance.getLocalizedString("Export all NPCs");
            checkBox4.Text = MainWindow.instance.getLocalizedString("Export only selected resources");
            checkBox3.Text = MainWindow.instance.getLocalizedString("Export all resources");
            button2.Text = MainWindow.instance.getLocalizedString("Export");
            Text = MainWindow.instance.getLocalizedString("Export NPCs to coords_data.txt");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            checkBox2.Checked = !checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            checkBox1.Checked = !checkBox2.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog of = new OpenFileDialog())
            {
                of.Filter = "Text files (*.txt) | *.txt";
                if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    filename = of.FileName;
                    textBox1.Text = filename;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(filename))
            {
                MessageBox.Show(MainWindow.instance.getLocalizedString("Enter path to coords_data.txt"), MainWindow.instance.getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                button2.Enabled = false;
                button1.Enabled = false;
                new Thread(() =>
                {
                    using (StreamWriter sw = new StreamWriter(new FileStream(filename, FileMode.Append), Encoding.GetEncoding("GBK")))
                    {
                        if (checkBox1.Checked)
                        {
                            MainWindow.instance.BeginInvoke((Action)(() =>
                            {
                                progressBar1.Value = 0;
                                progressBar1.Maximum = MainWindow.instance.npcList.SelectedRows.Count;
                            }));
                            for (int i = 0; i < MainWindow.instance.npcList.SelectedRows.Count; i++)
                            {
                                for (int j = 0; j < MainWindow.instance.npcdata.m_aAreas[MainWindow.instance.npcList.SelectedRows[i].Index].iNumGen; j++)
                                {
                                    sw.WriteLine(MainWindow.instance.npcdata.m_aAreas[MainWindow.instance.npcList.SelectedRows[i].Index].m_aNPCGens[j].dwID + "	" + textBox2.Text + "	" + MainWindow.instance.npcdata.m_aAreas[MainWindow.instance.npcList.SelectedRows[i].Index].vPos[0].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aAreas[MainWindow.instance.npcList.SelectedRows[i].Index].vPos[1].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aAreas[MainWindow.instance.npcList.SelectedRows[i].Index].vPos[2].ToString().Replace(",", "."));
                                }
                                MainWindow.instance.BeginInvoke((Action)(() =>
                                {
                                    progressBar1.Value++;
                                }));
                            }
                        }
                        else
                        {
                            MainWindow.instance.BeginInvoke((Action)(() =>
                            {
                                progressBar1.Value = 0;
                                progressBar1.Maximum = MainWindow.instance.npcList.Rows.Count;
                            }));
                            for (int i = 0; i < MainWindow.instance.npcList.Rows.Count; i++)
                            {
                                for (int j = 0; j < MainWindow.instance.npcdata.m_aAreas[i].iNumGen; j++)
                                {
                                    sw.WriteLine(MainWindow.instance.npcdata.m_aAreas[i].m_aNPCGens[j].dwID + "	" + textBox2.Text + "	" + MainWindow.instance.npcdata.m_aAreas[i].vPos[0].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aAreas[i].vPos[1].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aAreas[i].vPos[2].ToString().Replace(",", "."));
                                }
                                MainWindow.instance.BeginInvoke((Action)(() =>
                                {
                                    progressBar1.Value++;
                                }));
                            }
                        }
                        if (checkBox4.Checked)
                        {
                            MainWindow.instance.BeginInvoke((Action)(() =>
                            {
                                progressBar1.Value = 0;
                                progressBar1.Maximum = MainWindow.instance.dataGridView1.SelectedRows.Count;
                            }));
                            for (int i = 0; i < MainWindow.instance.dataGridView1.SelectedRows.Count; i++)
                            {
                                for (int j = 0; j < MainWindow.instance.npcdata.m_aResAreas[MainWindow.instance.dataGridView1.SelectedRows[i].Index].iNumRes; j++)
                                {
                                    sw.WriteLine(MainWindow.instance.npcdata.m_aResAreas[MainWindow.instance.dataGridView1.SelectedRows[i].Index].m_aRes[j].idTemplate + "	" + textBox2.Text + "	" + MainWindow.instance.npcdata.m_aResAreas[MainWindow.instance.dataGridView1.SelectedRows[i].Index].vPos[0].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aResAreas[MainWindow.instance.dataGridView1.SelectedRows[i].Index].vPos[1].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aResAreas[MainWindow.instance.dataGridView1.SelectedRows[i].Index].vPos[2].ToString().Replace(",", "."));
                                }
                                MainWindow.instance.BeginInvoke((Action)(() =>
                                {
                                    progressBar1.Value++;
                                }));
                            }
                        }
                        else
                        {
                            MainWindow.instance.BeginInvoke((Action)(() =>
                            {
                                progressBar1.Value = 0;
                                progressBar1.Maximum = MainWindow.instance.dataGridView1.Rows.Count;
                            }));
                            for (int i = 0; i < MainWindow.instance.dataGridView1.Rows.Count; i++)
                            {
                                for (int j = 0; j < MainWindow.instance.npcdata.m_aResAreas[i].iNumRes; j++)
                                {
                                    sw.WriteLine(MainWindow.instance.npcdata.m_aResAreas[i].m_aRes[j].idTemplate + "	" + textBox2.Text + "	" + MainWindow.instance.npcdata.m_aResAreas[i].vPos[0].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aResAreas[i].vPos[1].ToString().Replace(",", ".") + "	" + MainWindow.instance.npcdata.m_aResAreas[i].vPos[2].ToString().Replace(",", "."));
                                }
                                MainWindow.instance.BeginInvoke((Action)(() =>
                                {
                                    progressBar1.Value++;
                                }));
                            }
                        }
                    }
                    MainWindow.instance.BeginInvoke((Action)(() =>
                    {
                        MessageBox.Show(MainWindow.instance.getLocalizedString("Export finished!"), MainWindow.instance.getLocalizedString("Information"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        button2.Enabled = true;
                        button1.Enabled = true;
                    }));
                }).Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, MainWindow.instance.getLocalizedString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            checkBox3.Checked = !checkBox4.Checked;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            checkBox4.Checked = !checkBox3.Checked;
        }
    }
}
