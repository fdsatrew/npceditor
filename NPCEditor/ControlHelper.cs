﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace NPCEditor
{
    public static class ControlHelper
    {

        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        static ToolTip toolTip1 = new ToolTip();

        public static void setupTooltip()
        {
            // Set up the delays for the ToolTip.
            toolTip1.AutoPopDelay = 32000;
            toolTip1.InitialDelay = 0;
            toolTip1.ReshowDelay = 0;
            toolTip1.IsBalloon = true;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip1.ShowAlways = true;
            toolTip1.UseAnimation = true;
        }

        public static void updateTooltip(Control ctrl, string text)
        {
            toolTip1.SetToolTip(ctrl, text);
        }

        public static void addLabelToTextBox(TextBox textbox, string text)
        {
            var btn = new Label();
            btn.Text = text;
            btn.Font = new Font("Times New Roman", 10.0f);
            btn.Location = new Point(textbox.ClientSize.Width - 10, 0);
            textbox.Controls.Add(btn);
            // Send EM_SETMARGINS to prevent text from disappearing underneath the button
            SendMessage(textbox.Handle, 0xd3, (IntPtr)2, (IntPtr)(btn.ClientSize.Width << 10));
        }

        public static void addButtonToTextBox(TextBox textbox, Image img, string desc, EventHandler callBack)
        {
            var btn = new PictureBox();
            btn.Size = new Size(16, 16);
            btn.Location = new Point(textbox.ClientSize.Width - 16, 0);
            btn.SizeMode = PictureBoxSizeMode.StretchImage;
            btn.Margin = Padding.Empty;
            btn.Image = img;
            btn.Cursor = Cursors.Arrow;
            textbox.Controls.Add(btn);
            toolTip1.SetToolTip(btn, desc);
            btn.Click += callBack;
            // Send EM_SETMARGINS to prevent text from disappearing underneath the button
            SendMessage(textbox.Handle, 0xd3, (IntPtr)2, (IntPtr)(btn.Width << 16));
        }
    }
}
