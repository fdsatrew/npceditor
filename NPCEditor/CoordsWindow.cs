﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NPCEditor
{
    public partial class CoordsWindow : Form
    {
        public CoordsWindow()
        {
            InitializeComponent();
            this.Text = MainWindow.instance.getLocalizedString("Convert coordinates");
            button2.Text = MainWindow.instance.getLocalizedString("Cancel");
        }
    }
}
