﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NPCEditor
{
    public static class Extensions
    {
        public static string GetLocalization(int id)
        {
            return "placeholder";
        }

        public static string SkillDesc(int id)
        {
            return "placeholder";
        }

        public static string SkillName(int id)
        {
            return "placeholder";
        }

        public static string ItemPropsSecondsToString2(uint id)
        {
            return "placeholder";
        }

        public static string ItemPropsSecondsToString(uint id)
        {
            return "placeholder";
        }
        public static string InstanceName(int id)
        {
            return "placeholder";
        }

        public static IEnumerable<uint> GetPowers(uint value)
        {
            uint v = value;
            while (v > 0)
            {
                yield return (v & 0x01);
                v >>= 1;
            }
        }

        public static string DecodingFoodMask(string food_mask)
        {
            return "placeholder";
        }

        public static string DecodingCharacterComboId(string character_combo_id)
        {
            return "placeholder";
        }

        public static string ConvertToClientX(float x)
        {
            double cx = 400 + Math.Truncate(x * 0.1);
            return cx.ToString();
        }
        public static string ConvertToClientY(float y)
        {
            double cy = Math.Truncate(y * 0.1);
            return cy.ToString();
        }
        public static string ConvertToClientZ(float z)
        {
            double cz = 550 + Math.Truncate(z * 0.1);
            return cz.ToString();
        }

        public static float ConvertToServerX(int x)
        {
            double cx = 400 + Math.Truncate(x * 0.1);
            Console.WriteLine((x - 400) * 10);
            return 0f;
        }
        public static float ConvertToServerY(int y)
        {
            double cy = Math.Truncate(y * 0.1);
            return 0f;
        }
        public static float ConvertToServerZ(int z)
        {
            double cz = 550 + Math.Truncate(z * 0.1);
            return 0f;
        }
    }

    public static class NumericExtensions
    {
        public static float toDegrees(this float val)
        {
            return 57.2958f * val;
        }

        public static float ToRadians(this float val)
        {
            return 0.0174532f * val;
        }

        public static byte ToPWRotation(this float val)
        {
            return Convert.ToByte(((val) / 360) * 256);
        }

        public static float ToDegress(this byte val)
        {
            return (val * 360) / 256;
        }
    }
}
