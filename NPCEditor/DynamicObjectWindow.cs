﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NPCEditor
{
    public partial class DynamicObjectWindow : Form
    {
        public bool saveDialog = false;
        public DynamicObjectWindow()
        {
            InitializeComponent();
            updateLanguage();
            if (MainWindow.instance.dynObjsdata != null)
            {
                for (int i = 0; i < MainWindow.instance.dynObjsdata.count; i++)
                {
                    Application.DoEvents();
                    dynObjsList.Rows.Add(MainWindow.instance.dynObjsdata.objectsinfo[i].id, getDynObjName(i), MainWindow.instance.getStringGBK(MainWindow.instance.dynObjsdata.objectsinfo[i].path));
                }
                if (MainWindow.instance.dynObjsdata.count > 0)
                {
                    MainWindow.instance.selectRow(dynObjsList, 0);
                }
            }
        }

        public void updateLanguage()
        {
            if (MainWindow.instance.dynObjsFilePath != null)
            {
                this.Text = MainWindow.instance.getLocalizedString("dynamicobjects.data editor") + (saveDialog ? "* [" : " [") + MainWindow.instance.dynObjsFilePath + "]";
            }
            else
            {
                this.Text = MainWindow.instance.getLocalizedString("dynamicobjects.data editor");
            }
            fileToolStripMenuItem.Text = MainWindow.instance.getLocalizedString("File");
            toolsToolStripMenuItem.Text = MainWindow.instance.getLocalizedString("Tools");
            openToolStripMenuItem.Text = MainWindow.instance.getLocalizedString("Open dynamicobjects.data");
            saveAsToolStripMenuItem.Text = MainWindow.instance.getLocalizedString("Save As");
            saveToolStripMenuItem.Text = MainWindow.instance.getLocalizedString("Save");
            exportAllUnknownIdsToolStripMenuItem.Text = MainWindow.instance.getLocalizedString("Export all unknown dynamic objects");
            createDynObj.Text = MainWindow.instance.getLocalizedString("Create dynamic object");
            removeDynObj.Text = MainWindow.instance.getLocalizedString("Remove dynamic object");
            copyDynObj.Text = MainWindow.instance.getLocalizedString("Clone dynamic object");
        }

        private void dataGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            if (e.Button == MouseButtons.Right)
            {
                // Add this
                dg.CurrentCell = dg.Rows[e.RowIndex].Cells[e.ColumnIndex];
                // Can leave these here - doesn't hurt
                dg.Rows[e.RowIndex].Selected = true;
                dg.Focus();
            }
        }

        String getDynObjName(int i)
        {
            String result;
            if (!MainWindow.instance.dynobjs.TryGetValue(MainWindow.instance.dynObjsdata.objectsinfo[i].id, out result))
            {
                result = "NOT FOUND";
            }
            return result;
        }

        void saveDynamicObjectsData(string filePath)
        {
            using (BinaryWriter bw = new BinaryWriter(File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)))
            {
                bw.Write(MainWindow.instance.dynObjsdata.version);
                bw.Write(MainWindow.instance.dynObjsdata.count);
                for (int i = 0; i < MainWindow.instance.dynObjsdata.count; i++)
                {
                    bw.Write(MainWindow.instance.dynObjsdata.objectsinfo[i].id);
                    bw.Write(MainWindow.instance.dynObjsdata.objectsinfo[i].length);
                    bw.Write(MainWindow.instance.dynObjsdata.objectsinfo[i].path);
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "dynamicobjects (*.data) | *.data";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    MainWindow.instance.dynObjsDataLoaded = false;
                    openOrSaveControl(false);
                    dynObjsList.Rows.Clear();
                    MainWindow.instance.dynObjsFilePath = openFileDialog.FileName;
                    MainWindow.instance.dynObjsdata = new DYNAMICOBJECTSDATA();
                    using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(MainWindow.instance.dynObjsFilePath))
                    using (MemoryMappedViewStream mms = mmf.CreateViewStream())
                    using (BinaryReader br = new BinaryReader(mms))
                    {
                        MainWindow.instance.dynObjsdata.version = br.ReadInt32();
                        if (MainWindow.instance.dynObjsdata.version != 1347242308)
                        {
                            MessageBox.Show(MainWindow.instance.getLocalizedString("This version of dynamicobjects.data is not supported"), MainWindow.instance.getLocalizedString("dynamicobjects.data editor"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                            MainWindow.instance.dynObjsFilePath = null;
                            MainWindow.instance.dynObjsdata = null;
                            openOrSaveControl(true);
                            return;
                        }
                        MainWindow.instance.dynObjsdata.count = br.ReadInt32();
                        MainWindow.instance.dynObjsdata.objectsinfo = new List<DYNAMICOBJECT>();
                        for (int i = 0; i < MainWindow.instance.dynObjsdata.count; i++)
                        {
                            MainWindow.instance.dynObjsdata.objectsinfo.Add(new DYNAMICOBJECT());
                            MainWindow.instance.dynObjsdata.objectsinfo[i].id = br.ReadUInt32();
                            MainWindow.instance.dynObjsdata.objectsinfo[i].length = br.ReadInt32();
                            MainWindow.instance.dynObjsdata.objectsinfo[i].path = br.ReadBytes(MainWindow.instance.dynObjsdata.objectsinfo[i].length);
                            Application.DoEvents();
                            dynObjsList.Rows.Add(MainWindow.instance.dynObjsdata.objectsinfo[i].id, getDynObjName(i), MainWindow.instance.getStringGBK(MainWindow.instance.dynObjsdata.objectsinfo[i].path));
                            dynObjsList.Rows[i].Cells[1].ReadOnly = true;
                        }
                    }
                    openOrSaveControl(true);
                    MainWindow.instance.dynObjsDataLoaded = true;
                    if (MainWindow.instance.dynObjsdata.count > 0)
                    {
                        MainWindow.instance.selectRow(dynObjsList, 0);
                    }
                    this.Text = MainWindow.instance.getLocalizedString("dynamicobjects.data editor") + " [" + MainWindow.instance.dynObjsFilePath + "]";
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            updateLanguage();
        }

        private void exportAllUnknownIdsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MainWindow.instance.dynObjsDataLoaded)
            {
                string s;
                SaveFileDialog sd = new SaveFileDialog();
                sd.Filter = "Text file (*.txt) | *.txt";
                if (sd.ShowDialog() == DialogResult.OK)
                {
                    using (FileStream fs = File.Open(sd.FileName, FileMode.OpenOrCreate, FileAccess.Write))
                    using (BufferedStream bs = new BufferedStream(fs))
                    using (StreamWriter sr = new StreamWriter(bs, Encoding.UTF8))
                    {
                        for (int i = 0; i < MainWindow.instance.dynObjsdata.count; i++)
                        {
                            if (!MainWindow.instance.dynobjs.TryGetValue(MainWindow.instance.dynObjsdata.objectsinfo[i].id, out s))
                            {
                                sr.WriteLine(MainWindow.instance.dynObjsdata.objectsinfo[i].id + "\t" + MainWindow.instance.getStringGBK(MainWindow.instance.dynObjsdata.objectsinfo[i].path));
                            }
                        }
                    }
                }
                sd.Dispose();
            }
        }

        private void dynObjsList_SelectionChanged(object sender, EventArgs e)
        {
            if (dynObjsList.CurrentCell != null)
                dynObjsList.Rows[dynObjsList.CurrentCell.RowIndex].Selected = true;
        }
        public uint getUniqueId()
        {
            uint result = 0;
            foreach (DYNAMICOBJECT dynObj in MainWindow.instance.dynObjsdata.objectsinfo)
            {
                if (dynObj.id > result) result = dynObj.id;
            }
            return result + 1;
        }

        private void createDynObj_Click(object sender, EventArgs e)
        {
            int i = MainWindow.instance.dynObjsdata.count;
            MainWindow.instance.dynObjsdata.objectsinfo.Add(new DYNAMICOBJECT());
            MainWindow.instance.dynObjsdata.objectsinfo[i].id = getUniqueId();
            MainWindow.instance.dynObjsdata.objectsinfo[i].path = MainWindow.instance.getBytesGBK("path");
            MainWindow.instance.dynObjsdata.objectsinfo[i].length = MainWindow.instance.dynObjsdata.objectsinfo[i].path.Length;
            dynObjsList.Rows.Add(MainWindow.instance.dynObjsdata.objectsinfo[i].id, getDynObjName(i), MainWindow.instance.getStringGBK(MainWindow.instance.dynObjsdata.objectsinfo[i].path));
            dynObjsList.Rows[i].Cells[1].ReadOnly = true;
            MainWindow.instance.selectRow(dynObjsList, i);
            MainWindow.instance.dynObjsdata.count++;
            saveDialog = true;
        }

        private void dynObjMenu_Opening(object sender, CancelEventArgs e)
        {
            if (MainWindow.instance.dynObjsDataLoaded)
            {
                createDynObj.Enabled = true;
                if (dynObjsList.CurrentCell != null)
                {
                    copyDynObj.Enabled = true;
                    removeDynObj.Enabled = true;
                }
            }
            else
            {
                createDynObj.Enabled = false;
                copyDynObj.Enabled = false;
                removeDynObj.Enabled = false;
            }
        }

        private void removeDynObj_Click(object sender, EventArgs e)
        {
            int i = dynObjsList.CurrentCell.RowIndex;
            MainWindow.instance.dynObjsdata.objectsinfo.RemoveAt(i);
            dynObjsList.Rows.RemoveAt(i);
            MainWindow.instance.dynObjsdata.count--;
            saveDialog = true;
        }

        private void copyDynObj_Click(object sender, EventArgs e)
        {
            int i = MainWindow.instance.dynObjsdata.count;
            int j = dynObjsList.CurrentCell.RowIndex;
            MainWindow.instance.dynObjsdata.objectsinfo.Add(new DYNAMICOBJECT());
            MainWindow.instance.dynObjsdata.objectsinfo[i].id = getUniqueId();
            MainWindow.instance.dynObjsdata.objectsinfo[i].path = MainWindow.instance.dynObjsdata.objectsinfo[j].path;
            MainWindow.instance.dynObjsdata.objectsinfo[i].length = MainWindow.instance.dynObjsdata.objectsinfo[j].length;
            dynObjsList.Rows.Add(MainWindow.instance.dynObjsdata.objectsinfo[i].id, getDynObjName(i), MainWindow.instance.getStringGBK(MainWindow.instance.dynObjsdata.objectsinfo[i].path));
            dynObjsList.Rows[i].Cells[1].ReadOnly = true;
            MainWindow.instance.selectRow(dynObjsList, i);
            MainWindow.instance.dynObjsdata.count++;
            saveDialog = true;
        }

        void openOrSaveControl(bool enable)
        {
            openToolStripMenuItem.Enabled = enable;
            saveAsToolStripMenuItem.Enabled = enable;
            saveToolStripMenuItem.Enabled = enable;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MainWindow.instance.dynObjsdata != null)
            {
                openOrSaveControl(false);
                saveDynamicObjectsData(MainWindow.instance.dynObjsFilePath);
                openOrSaveControl(true);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MainWindow.instance.dynObjsdata != null)
            {
                SaveFileDialog sd = new SaveFileDialog();
                sd.Filter = "dynamicobjects (*.data) | *.data";
                if (sd.ShowDialog() == DialogResult.OK)
                {
                    openOrSaveControl(false);
                    saveDynamicObjectsData(sd.FileName);
                    openOrSaveControl(true);
                }
                sd.Dispose();
            }
        }

        private void dynObjsList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell tb = dynObjsList.Rows[e.RowIndex].Cells[e.ColumnIndex];
            switch (e.ColumnIndex)
            {
                case 0:
                    uint value;
                    try
                    {
                        value = uint.Parse(tb.Value.ToString());
                        tb.Style.BackColor = Color.White;
                    }
                    catch (Exception)
                    {
                        tb.Style.BackColor = Color.Red;
                        return;
                    }
                    MainWindow.instance.dynObjsdata.objectsinfo[e.RowIndex].id = value;
                    dynObjsList.Rows[e.RowIndex].Cells[1].Value = getDynObjName(e.RowIndex);
                    break;
                case 2:
                    MainWindow.instance.dynObjsdata.objectsinfo[e.RowIndex].path = MainWindow.instance.getBytesGBK(tb.Value.ToString());
                    MainWindow.instance.dynObjsdata.objectsinfo[e.RowIndex].length = MainWindow.instance.dynObjsdata.objectsinfo[e.RowIndex].path.Length;
                    break;
            }
            saveDialog = true;
        }

        private void dynObjsList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dynObjsList.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.White;
        }

        private void DynamicObjectWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (saveDialog)
            {
                e.Cancel = true;
                DialogResult dr = MessageBox.Show(MainWindow.instance.getLocalizedString("You have unsaved changes in this dynamicojbects!") + "\n" + MainWindow.instance.getLocalizedString("Do you want to save them?"), "Regular NPC Editor", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                switch (dr)
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        saveDialog = false;
                        saveDynamicObjectsData(MainWindow.instance.dynObjsFilePath);
                        this.Close();
                        break;
                    case System.Windows.Forms.DialogResult.No:
                        saveDialog = false;
                        this.Close();
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                        return;
                }
            }
        }
    }
}
